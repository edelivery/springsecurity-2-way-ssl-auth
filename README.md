# SpringSecurity 2-way-SSL for eDelivery
SpringSecurity add-on supporting handling 2-way-SSL authentication.

Usernames in all eDelivery projects follow the same common pattern: 

*CN={common-name},O={organisation},C={country}:{serial-number}*

This library supports both X509 and BlueCoat reverse proxy client certificates authentication.
 
Sample SpringSecurity configuration, covering basic auth and both types of 2-way-SSL:

        <http>
            <http-basic/>
            <anonymous granted-authority="ROLE_ANONYMOUS"/>
    
            <custom-filter position="X509_FILTER" ref="x509AuthFilter"/>
            <custom-filter position="PRE_AUTH_FILTER" ref="blueCoatReverseProxyAuthFilter"/>
    
            <intercept-url method="POST" access=" ! isAnonymous()" pattern="/*"/>
    
            <csrf disabled="true"/>
        </http>
    
        <authentication-manager alias="sampleAuthenticationManager">
    
            <authentication-provider ref="preauthAuthProvider"/>
    
            <authentication-provider>
                <password-encoder hash="bcrypt"/>
                <user-service id="sampleUserService">
                    <user name="CN=comon name,O=org,C=BE:0000000000000066" authorities="N/A"/>
                    <user name="test_user_hashed_pass"
                          password="$2a$06$k.Q/6anG4Eq/nNTZ0C1UIuAKxpr6ra5oaMkMSrlESIyA5jKEsUdyS" authorities="N/A"/>
                </user-service>
            </authentication-provider>
    
        </authentication-manager>
    
        <b:bean id="preauthAuthProvider"
                class="org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider">
            <b:property name="preAuthenticatedUserDetailsService">
                <b:bean id="userDetailsServiceWrapper"
                        class="org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper">
                    <b:property name="userDetailsService" ref="sampleUserService"/>
                </b:bean>
            </b:property>
        </b:bean>
    
        <b:bean id="blueCoatReverseProxyAuthFilter" class="eu.europa.ec.edelivery.security.ClientCertAuthenticationFilter">
            <b:property name="authenticationManager" ref="sampleAuthenticationManager"/>
            <b:property name="blueCoatEnabled" value="true"/>
        </b:bean>
    
        <b:bean id="x509AuthFilter" class="eu.europa.ec.edelivery.security.EDeliveryX509AuthenticationFilter">
            <b:property name="authenticationManager" ref="sampleAuthenticationManager"/>
        </b:bean>
    
    </b:beans>
