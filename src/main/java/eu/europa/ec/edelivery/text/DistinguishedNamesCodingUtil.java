/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.edelivery.text;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

import javax.naming.InvalidNameException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import jakarta.xml.bind.DatatypeConverter;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Utility for manipulating Distinguished Names - operations that are not covered by well known libraries.
 * <p>
 * @author Pawel GUTOWSKI
 * @since 1.0
 */
public class DistinguishedNamesCodingUtil {

    /**
     * Subject normalization contains only attributes:
     * CN: CommonName
     * O: Organization
     * C: CountryName
     * CertificateIdentifier contains also the serial number.
     */
    protected static final List<String> MINIMAL_ATTRIBUTES_DN = Arrays.asList("CN", "O", "C");

    /**
     * Issuer normalization contains only most commons attributes:
     * CN: CommonName
     * OU: OrganizationalUnit
     * O: Organization
     * L: Locality
     * ST: StateOrProvinceName
     * C: CountryName
     * Ignore trouble making emailAddress, SerialNumber because different ReverseProxy add this attributes in various way.
     * (As part of CN separated by / or as separate RDN...)
     */
    protected static final List<String> COMMON_ATTRIBUTES_DN = Arrays.asList("CN", "OU", "O", "L", "ST", "C");

    private static final Logger LOG = LoggerFactory.getLogger(DistinguishedNamesCodingUtil.class);

    private static final Pattern HEX_LITERALS_PATTERN = Pattern.compile("(\\\\x[0-9a-fA-F]{2})+");
    private static final String HEX_LITERAL_PREFIX = "\\\\x";

    private static final Pattern DIACRITICS = Pattern.compile("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");

    // emailAddress - serialNumber  https://tools.ietf.org/rfc/rfc5280.txt
    // Implementations of this specification MUST
    //   be prepared to receive the following standard attribute types in
    //   issuer and subject (Section 4.1.2.6) names:
    //
    //      * country,
    //      * organization,
    //      * organizational unit,
    //      * distinguished name qualifier,
    //      * state or province name,
    //      * common name (e.g., "Susan Housley"), and
    //      * serial number.

    // Non-capturing group that matches most common RFC2253 Distinguished name's keys with equal sign, that means:
    // * key name and equal sing (i.e.: CN= )
    // * by key we mean [A-Z] - min 1, max 6 characters
    // * might be followed by white chars
    // * might be preceded by comma and white chars
    // * sample matches:  "CN="    " OU = "    " , O="
    private static final String DN_KEY_EQUAL_REGEX = "(?:(,{0,1}|\\+{0,1})\\s{0,10}(?:[A-Z]{1,6}|emailAddress|EMAILADDRESS|serialNumber|SERIALNUMBER|street|postalCode)\\s{0,10}=)";
    private static final String DN_KEY_EQUAL_REGEX_UNV = "(?:,{0,1}\\s{0,10}(?:[A-Z]{1,6}|emailAddress)\\s{0,10}=)";
    //private static final String DN_KEY_EQUAL_REGEX = "(?:,{0,1}\\s{0,10}(?:[A-Za-z]{1,16})\\s{0,10}=)";
    // Non-greedy matcher for DN RDN values. Matches everything that:
    // * is preceded by key name and equal sign
    // * is followed by key name and equal sign or end of String
    private static final String DN_VALUE_REGEX = format("(?<=%s)(.+?)(?=%s|$)", DN_KEY_EQUAL_REGEX, DN_KEY_EQUAL_REGEX);

    // old reverse proxy added email, serial number and other non common  RDNs as / delimited value to previous RDNs
    // Matches unwanted RDN's garbage postfix at the end, i.e. for this DN:
    // CN=Common name/emailAddress\=a@b.pl,C=PL
    // will match:
    // /emailAddress\=a@b.pl
    private static final String DN_RDN_UNWANTED_POSTFIX_ATTR_REGEX = format("(/[a-zA-Z]+\\\\{0,1}=.+?)(?=%s|$)", DN_KEY_EQUAL_REGEX_UNV);
    //private static final String DN_RDN_UNWANTED_POSTFIX_ATTR_REGEX = format("(/[a-zA-Z]+\\\\{0,1}=.+?)(?=%s|$)", DN_KEY_EQUAL_REGEX);

    private DistinguishedNamesCodingUtil() {
    }

    /**
     * Decodes UTF HEX byte literals,
     * I.e.: "Za\xC5\xBC\xC3\xB3\xC5\x82\xC4\x87 g\xC4\x99\xC5\x9Bl\xC4\x85 ja\xC5\xBA\xC5\x84"
     * is decoded to: "Zażółć gęślą jaźń"
     * <p>
     * Or this containing Scandinavian characters: "Norwegian/Swedish:xC3x86 xC3xA6,xC3x98 xC3xB8"
     * is decoded to: "Norwegian/Swedish:Æ æ,Ø ø"
     *
     * @param mixedStringAndHexInput String to be decoded, with UTF bytes HEX encoded, prefixed with "\x"
     * @return Decoded String
     */
    public static String decodeUtfHexLiterals(String mixedStringAndHexInput) {
        String unescapedResult = mixedStringAndHexInput;
        Matcher m = HEX_LITERALS_PATTERN.matcher(unescapedResult);
        while (m.find()) {
            String hexWithPrefixes = m.group();
            String hexBinarySequence = hexWithPrefixes.replaceAll(HEX_LITERAL_PREFIX, "");
            byte[] bytes = DatatypeConverter.parseHexBinary(hexBinarySequence);
            String utfDecoded = new String(bytes, UTF_8);
            unescapedResult = m.replaceFirst(utfDecoded);
            m = HEX_LITERALS_PATTERN.matcher(unescapedResult);
        }
        return unescapedResult;
    }

    /**
     * Fixes not escaped characters within Distinguished Name.
     * I.e.: Special characters defined in RFC2253 (like comma and some others) break DN parsing if they are not escaped from RDN value.
     *
     * @param notEscapedDN Distinguished Name that needs escaping, i.e.: CN=comma,inside,C=PL
     * @return escaped DN, i.e.: CN=comma\,inside,C=PL
     */
    public static String smartEscapeDistinguishedName(String notEscapedDN) {
        String escapedDN = notEscapedDN;
        Matcher m = Pattern.compile(DN_VALUE_REGEX).matcher(escapedDN);
        int index = 0;
        while (m.find(index)) {
            String rdnEscapedVal = escapeRdnValue(m.group());
            escapedDN = escapedDN.substring(0, m.start()) + rdnEscapedVal + escapedDN.substring(m.end());
            index = m.start() + rdnEscapedVal.length();
            m = Pattern.compile(DN_VALUE_REGEX).matcher(escapedDN);
        }
        if (!escapedDN.equals(notEscapedDN)) {
            LOG.info("Fixed malformed DN that needed escaping:Old DN: [{}] New DN: [{}]", notEscapedDN, escapedDN);
        }
        return escapedDN;
    }

    private static String escapeRdnValue(String rdnValue) {
        try {
            //Prevent from escaping already escaped characters
            rdnValue = (String) Rdn.unescapeValue(rdnValue);
        } catch (IllegalArgumentException e) {
            LOG.info("Provided malformed DN RDN value, error message: [{}] Wrong characters will be escaped: [{}]", e.getMessage(), rdnValue);
        }
        return Rdn.escapeValue(rdnValue);
    }

    /**
     * Some tools (i.e.: BlueCoat) introduce garbage attributes at the end of RDN - they mix in other fields like "emailAddress" or "title"
     * For instance they copy "E=a@b.pl" value at the and of CN: "CN=common name/emailAddress\=a@b.pl"
     * This method removes such garbage attributes
     */

    public static String removeUnwantedRdnPostfixAttr(String dirtyValue) {
        return dirtyValue.replaceAll(DN_RDN_UNWANTED_POSTFIX_ATTR_REGEX, "");
    }

    /**
     * It replaces special charactes by it representant in the alphabet.
     * <p>
     * Ex = "äåãaaáeéiïíoóöőuúüűńñÿ AÁEËÉIÍOÓÖŐUŪÚÜŰŘ" will be replaced byaaaaaaeeiiioooouuuunny AAEEEIIOOOOUUUUUR
     *
     * @author Flavio Santos
     */
    public static String normalizeUnicode(String value) {
        String normalizedValue = Normalizer.normalize(value, Normalizer.Form.NFD);
        return DIACRITICS.matcher(normalizedValue).replaceAll("");
    }

    public static String normalizeChars(String fieldValue) {
        fieldValue = decodeUtfHexLiterals(fieldValue);
        fieldValue = normalizeUnicode(fieldValue);
        return fieldValue;
    }

    /**
     * Normalize dn containing only certain rnd list. Some RNDs can be in name multiple time (ex: OU) that is why
     * multiple rnds are returned in alphabet order.
     *
     * @param distinguishedName
     * @return normalized DN
     */
    public static String normalizeDN(String distinguishedName, List<String> rdnList) {

        // Make a map from type to name
        final Map<String, List<Rdn>> parts = getAllRDNValues(distinguishedName);

        StringBuilder strNormalizedDN = new StringBuilder();
        for (String rdn : rdnList) {
            // skip value if not from targeted list
            if (!parts.containsKey(rdn)) {
                continue;
            }

            List<Rdn> list = parts.get(rdn);
            // sort by alphabet type=value
            Collections.sort(list);
            for (Rdn el : list) {
                if (strNormalizedDN.length() != 0) {
                    strNormalizedDN.append(",");
                }
                // for legacy reasons when certificate id was manually inserted to database - start and end spaces
                // were neglected. For back-compatibility trim values
                strNormalizedDN.append(StringUtils.trim(el.getType()));
                strNormalizedDN.append("=");
                strNormalizedDN.append(Rdn.escapeValue(StringUtils.trim(el.getValue().toString())));
            }

        }
        return strNormalizedDN.toString();
    }

    /**
     * Method returns all RDN values in a map.
     *
     * @param distinguishedName DN string to parse
     * @return  map of RDN values
     */
    public static Map<String, List<Rdn>> getAllRDNValues(String distinguishedName) {
        String normalizedDn = normalizeChars(distinguishedName);
        LdapName ldapName;
        try {
            ldapName = new LdapName(normalizedDn);
        } catch (InvalidNameException e) {
            throw new BadCredentialsException("Received invalid domain name for certificate pre-authentication: " + distinguishedName, e);
        }

        // Make a map from type to name
        final Map<String, List<Rdn>> parts = new HashMap<>();
        for (final Rdn rdn : ldapName.getRdns()) {
            if (rdn.size() > 1) {
                // flatten mutli-value names. ,
                parseMultiValueRDn(rdn.toAttributes(), parts);
            } else {
                if (!parts.containsKey(rdn.getType())) {
                    parts.put(rdn.getType(), new ArrayList<>());
                }
                List<Rdn> lst = parts.get(rdn.getType());
                lst.add(rdn);
                parts.put(rdn.getType(), lst);
            }
        }
        return parts;
    }

    public static void parseMultiValueRDn(Attributes attrs, Map<String, List<Rdn>> parts) {
        NamingEnumeration enr = attrs.getAll();
        try {
            while (enr.hasMore()) {
                Object mvRDn = enr.next();
                if (mvRDn instanceof BasicAttribute) {
                    BasicAttribute ba = (BasicAttribute) mvRDn;
                    if (!parts.containsKey(ba.getID())) {
                        parts.put(ba.getID(), new ArrayList<>());
                    }
                    List<Rdn> lst = parts.get(ba.getID());
                    lst.add(new Rdn(ba.getID(), ba.get()));
                }
            }

        } catch (NamingException ex) {
            LOG.error("Error occurred when parsing multi-value  attributes  [" + attrs + "]!", ex);
        }
    }

    public static String normalizeOrTrim(String domainName, List<String> rdnList) {
        try {
            return normalizeDN(domainName, rdnList);
        } catch (BadCredentialsException be) {
            LOG.error("Error occurred when normalize domain [" + domainName + "]. Return trimmed value!", be);
            return StringUtils.trimToNull(domainName);
        }
    }

    /**
     * Subject has in normalized form only CN, O and C
     *
     * @param domainName domain name to normalize
     * @return normalized domain name
     */
    public static String normalizeDN(String domainName) {
        return normalizeDN(domainName, MINIMAL_ATTRIBUTES_DN);
    }

    public static List<String> getMinimalAttributesDN() {
        return MINIMAL_ATTRIBUTES_DN;
    }

    public static List<String> getCommonAttributesDN() {
        return COMMON_ATTRIBUTES_DN;
    }
}
