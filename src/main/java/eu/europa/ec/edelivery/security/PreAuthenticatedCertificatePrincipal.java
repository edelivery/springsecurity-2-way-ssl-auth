/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil.normalizeDN;

/**
 * PreAuthenticated token and user details. To be used with {@code {@link ClientCertAuthenticationFilter }}
 * or direct 2-way-SSL authentication {@code {@link EDeliveryX509AuthenticationFilter}}.
 *
 * @author Pawel Gutowski
 */
public class PreAuthenticatedCertificatePrincipal implements Principal {

    //BlueCoat legacy for the ClientCert uses this format instead of HEX for serial numbers smaller than 2 bytes
    private static final Pattern BLUE_COAT_DEC_AND_HEX_COMBINED_PATTERN = Pattern.compile(".*\\d+\\s*\\((\\w+)\\).*");

    private String certSerial;
    private String subjectShortDN;
    private String subjectCommonDN;
    private String subjectOriginalDN;
    private String issuerDN;
    private String issuerOriginalDN;
    private Date notAfter;
    private Date notBefore;
    private List<String> clrDistributionPoints = new ArrayList<>();
    private List<String> policyOids = new ArrayList<>();
    private Object credentials;
    private X509Certificate certificate;

    public PreAuthenticatedCertificatePrincipal(String subjectDN, String issuerDN, String certSerialNumber) {
        this(null, subjectDN, issuerDN, certSerialNumber, null, null);
    }

    public PreAuthenticatedCertificatePrincipal(String subjectDN, String issuerDN, BigInteger certSerialNumber) {
        this(subjectDN, issuerDN, certSerialNumber.toString(16));
    }

    public PreAuthenticatedCertificatePrincipal(String subjectDN, String issuerDN, BigInteger certSerialNumber, Date notBefore, Date notAfter) {
        this(null, subjectDN, issuerDN, certSerialNumber.toString(16), notBefore, notAfter);
    }

    public PreAuthenticatedCertificatePrincipal(X509Certificate cert, String subjectDN, String issuerDN, String certSerialNumber, Date notBefore, Date notAfter, List<String> policyOids) {
        // use same MINIMAL_ATTRIBUTES_DN list to normalize subject and issuer. SML uses CertificateId -generate from subject to locate issuer.
        this.certificate = cert;
        this.subjectShortDN = normalizeDN(subjectDN);
        this.subjectCommonDN = normalizeDN(subjectDN, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        this.subjectOriginalDN = subjectDN;
        this.issuerDN = normalizeDN(issuerDN, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        this.issuerOriginalDN = issuerDN;
        this.certSerial = normalizeSerial(certSerialNumber);
        this.notBefore = notBefore;
        this.notAfter = notAfter;
        this.policyOids = policyOids;
        setCredential(cert);
    }

    public PreAuthenticatedCertificatePrincipal(X509Certificate cert, String subjectDN, String issuerDN, String certSerialNumber, Date notBefore, Date notAfter) {
        // use same MINIMAL_ATTRIBUTES_DN list to normalize subject and issuer. SML uses CertificateId -generate from subject to locate issuer.
        this.certificate = cert;
        this.subjectShortDN = normalizeDN(subjectDN);
        this.subjectCommonDN = normalizeDN(subjectDN, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        this.subjectOriginalDN = subjectDN;
        this.issuerDN = normalizeDN(issuerDN, DistinguishedNamesCodingUtil.getCommonAttributesDN());
        this.issuerOriginalDN = issuerDN;
        this.certSerial = normalizeSerial(certSerialNumber);
        this.notBefore = notBefore;
        this.notAfter = notAfter;
        setCredential(cert);
    }

    public PreAuthenticatedCertificatePrincipal(X509Certificate cert, String subjectDN, String issuerDN, BigInteger certSerialNumber, Date notBefore, Date notAfter) {
        this(cert, subjectDN, issuerDN, certSerialNumber.toString(16), notBefore, notAfter);
    }

    @Override
    public String getName() {
        return getName(16);
    }

    public String getName(int padding) {
        // add 0 if the length of the serial number ends with an odd number
        // legacy issue with the Client-Cert header
        String paddedSerialNumber = StringUtils.leftPad(certSerial, padding, "0");
        if (paddedSerialNumber.length() % 2 == 1) {
            paddedSerialNumber = "0" + paddedSerialNumber;
        }
        return subjectShortDN + ":" + paddedSerialNumber;
    }

    public String getSubjectShortDN() {
        return subjectShortDN;
    }

    public String getIssuerDN() {
        return issuerDN;
    }

    public String getIssuerOriginalDN() {
        return issuerOriginalDN;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }

    private String normalizeSerial(String certSerial) {
        Matcher blueCoatCombinedMatcher = BLUE_COAT_DEC_AND_HEX_COMBINED_PATTERN.matcher(certSerial);
        if (blueCoatCombinedMatcher.matches()) {
            certSerial = blueCoatCombinedMatcher.group(1);
        }
        certSerial = certSerial.replace(":", "");
        certSerial = certSerial.replaceFirst("0x", "");
        certSerial = certSerial.toLowerCase();
        // always return to lower case
        return certSerial.toLowerCase();
    }

public Object getCredentials() {
        return credentials;
    }

    public void setCredential(Object credentials) {
        this.credentials = credentials;
    }

    public String getCertSerial() {
        return certSerial;
    }

    public void setCertSerial(String certSerial) {
        this.certSerial = certSerial;
    }

    public void setSubjectShortDN(String subjectShortDN) {
        this.subjectShortDN = subjectShortDN;
    }

    public void setIssuerDN(String issuerDN) {
        this.issuerDN = issuerDN;
    }

    public Date getNotAfter() {
        return notAfter;
    }

    public void setNotAfter(Date notAfter) {
        this.notAfter = notAfter;
    }

    public Date getNotBefore() {
        return notBefore;
    }

    public void setNotBefore(Date notBefore) {
        this.notBefore = notBefore;
    }

    public List<String> getClrDistributionPoints() {
        return clrDistributionPoints;
    }

    public String getSubjectCommonDN() {
        return subjectCommonDN;
    }

    public void setSubjectCommonDN(String subjectCommonDN) {
        this.subjectCommonDN = subjectCommonDN;
    }

    public String getSubjectOriginalDN() {
        return subjectOriginalDN;
    }

    public void setSubjectOriginalDN(String subjectOriginalDN) {
        this.subjectOriginalDN = subjectOriginalDN;
    }

    public List<String> getPolicyOids() {
        return policyOids;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PreAuthenticatedCertificatePrincipal)) return false;
        PreAuthenticatedCertificatePrincipal that = (PreAuthenticatedCertificatePrincipal) o;
        return certSerial.equals(that.certSerial) &&
                subjectShortDN.equals(that.subjectShortDN) &&
                issuerDN.equals(that.issuerDN) &&
                Objects.equals(notAfter, that.notAfter) &&
                Objects.equals(notBefore, that.notBefore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(certSerial, subjectShortDN, issuerDN, notAfter, notBefore);
    }

    @Override
    public String toString() {
        return "PreAuthenticatedCertificatePrincipal{" +
                "certSerial='" + certSerial + '\'' +
                ", subjectShortDN='" + subjectShortDN + '\'' +
                ", issuerDN='" + issuerDN + '\'' +
                '}';
    }
}
