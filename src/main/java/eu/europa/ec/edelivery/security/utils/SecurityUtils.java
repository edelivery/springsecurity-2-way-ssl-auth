package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.exception.SecRuntimeException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Base64;


/**
 * Runtime exception thrown on unexpected internal events as "NoSuchAlgorithmException"
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class SecurityUtils {
    private static final Logger LOG = LoggerFactory.getLogger(SecurityUtils.class);

    protected SecurityUtils() {
    }

    public static class Secret {
        final SecretKey key;
        final AlgorithmParameterSpec ivParameter;

        public Secret(byte[] vector, SecretKey key) {
            this.ivParameter = new GCMParameterSpec(GCM_TAG_LENGTH_BIT, vector);
            this.key = key;
        }

        public Secret(AlgorithmParameterSpec ivParameter, SecretKey key) {
            this.ivParameter = ivParameter;
            this.key = key;
        }

        public byte[] getVector() {
            return ivParameter instanceof GCMParameterSpec ?
                    ((GCMParameterSpec) ivParameter).getIV() : null;
        }

        public AlgorithmParameterSpec getIVParameter() {
            return ivParameter;
        }

        public SecretKey getKey() {
            return key;
        }
    }

    private static final String VALID_USER_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final String VALID_PW_CHARS = VALID_USER_CHARS + "!@#$%^&*()-_=+{}[]|:;<>?,./";

    private static final int DEFAULT_PASSWORD_LENGTH = 16;
    private static final int DEFAULT_USER_LENGTH = 8;

    public static final String ALGORITHM_KEY = "AES";
    public static final String ALGORITHM_ENCRYPTION = "AES/GCM/NoPadding";
    public static final String ALGORITHM_ENCRYPTION_OBSOLETE = "AES/CBC/PKCS5Padding";
    public static final int KEY_SIZE = 256;
    public static final int GCM_TAG_LENGTH_BIT = 128;
    // for te gcm iv size is 12
    public static final int IV_GCM_SIZE = 12;
    // NULL IV is for CBC which  has IV size 16!
    private static final IvParameterSpec NULL_IV = new IvParameterSpec(new byte[16]);

    public static final String DECRYPTED_TOKEN_PREFIX = "{DEC}{";


    public static String getNonEncryptedValue(String value) {
        return value.substring(DECRYPTED_TOKEN_PREFIX.length(), value.lastIndexOf('}'));
    }

    public static String generateAuthenticationToken(boolean devMode) {
        return generateAuthenticationToken(devMode, DEFAULT_PASSWORD_LENGTH);
    }

    public static String generateAuthenticationToken(boolean devMode, int length) {
        String newKeyPassword = RandomStringUtils.random(length, 0, VALID_PW_CHARS.length(),
                false, false,
                VALID_PW_CHARS.toCharArray(), getRandomInstance(devMode));

        LOG.debug("New token generated with mode [{}]", devMode);
        return newKeyPassword;
    }

    public static String generateAuthenticationTokenIdentifier(boolean devMode) {
        return generateAuthenticationTokenIdentifier(devMode, DEFAULT_USER_LENGTH);
    }

    public static String generateAuthenticationTokenIdentifier(boolean devMode, int length) {
        String newKeyPassword = RandomStringUtils.random(length, 0, VALID_USER_CHARS.length(),
                true, false,
                VALID_USER_CHARS.toCharArray(), getRandomInstance(devMode));
        LOG.debug("New token identifier generated with mode [{}]", devMode);
        return newKeyPassword;
    }

    public static Secret generatePrivateSymmetricKey(boolean devMode) {
        // Generates a random key
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(ALGORITHM_KEY);
        } catch (NoSuchAlgorithmException exc) {
            throw new SecRuntimeException("Error occurred while generating secret key for encryption", exc);
        }
        keyGenerator.init(KEY_SIZE);
        SecretKey privateKey = keyGenerator.generateKey();
        SecureRandom rnd = getRandomInstance(devMode);
        // Using setSeed(byte[]) to reseed a Random object
        byte[] seed = rnd.generateSeed(IV_GCM_SIZE);
        rnd.setSeed(seed);

        byte[] buffIV = new byte[IV_GCM_SIZE];
        rnd.nextBytes(buffIV);

        return new Secret(buffIV, privateKey);
    }

    public static void generatePrivateSymmetricKey(File path, boolean devMode) {
        Secret secret = generatePrivateSymmetricKey(devMode);
        try (FileOutputStream out = new FileOutputStream(path)) {
            // first write IV
            out.write('#');
            out.write(secret.getVector());
            out.write('#');
            out.write(secret.getKey().getEncoded());
            out.flush();
        } catch (IOException exc) {
            throw new SecRuntimeException("Error occurred while saving key for encryption", exc);
        }
    }

    public static String encryptWrappedToken(File encKeyFile, String token) {
        return encryptWrappedToken(readSecret(encKeyFile), token);
    }

    public static String encryptWrappedToken(Secret secret, String token) {
        if (!StringUtils.isBlank(token) && token.startsWith(SecurityUtils.DECRYPTED_TOKEN_PREFIX)) {
            String unWrapToken = getNonEncryptedValue(token);
            return SecurityUtils.encrypt(secret, unWrapToken);
        }
        return token;
    }

    public static String encryptURLSafe(Secret secret, String plainToken) {
        return encrypt(secret, plainToken, Base64.getUrlEncoder().withoutPadding());
    }

    public static String encrypt(Secret secret, String plainToken) {
        return encrypt(secret.getKey(), secret.getIVParameter(), plainToken, Base64.getEncoder());
    }

    public static String encrypt(Secret secret, String plainToken, Base64.Encoder encoder) {
        return encrypt(secret.getKey(), secret.getIVParameter(), plainToken, encoder);
    }

    public static String encrypt(SecretKey privateKey, AlgorithmParameterSpec iv, String plainToken, Base64.Encoder encoder) {
        try {
            Cipher cipher = Cipher.getInstance(iv == NULL_IV ? ALGORITHM_ENCRYPTION_OBSOLETE : ALGORITHM_ENCRYPTION);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey, iv);
            byte[] encryptedData = cipher.doFinal(plainToken.getBytes());
            return new String(encoder.encode(encryptedData));
        } catch (Exception exc) {
            throw new SecRuntimeException("Error occurred while encrypting key for encryption", exc);
        }
    }

    public static String encrypt(File path, String plainToken) {
        Secret secret = readSecret(path);
        return encrypt(secret, plainToken, Base64.getEncoder());
    }

    public static Secret readSecret(File path) {
        byte[] buff;
        try {
            buff = Files.readAllBytes(path.toPath());
        } catch (Exception exc) {
            throw new SecRuntimeException("Error occurred while reading encryption key [" + path.getAbsolutePath() + "]!  Root cause: " + ExceptionUtils.getRootCauseMessage(exc), exc);
        }

        AlgorithmParameterSpec iv = getSaltParameter(buff);
        SecretKey privateKey = getSecretKey(buff);
        return new Secret(iv, privateKey);
    }

    public static String decrypt(File keyPath, String encryptedToken) {

        Secret secret = readSecret(keyPath);
        return decrypt(secret, encryptedToken);

    }

    public static String decrypt(Secret secret, String encryptedToken) {
        return decrypt(secret.getKey(), secret.ivParameter, encryptedToken, Base64.getDecoder());
    }

    public static String decryptUrlSafe(Secret secret, String encryptedToken) {
        return decrypt(secret.getKey(), secret.ivParameter, encryptedToken, Base64.getUrlDecoder());
    }

    public static String decrypt(SecretKey privateKey, AlgorithmParameterSpec iv, String encryptedToken, Base64.Decoder decoder) {
        try {
            byte[] decodedEncryptedPassword = decoder.decode(encryptedToken.getBytes());
            // this is for back-compatibility - if key parameter is IV than is CBC else ie GCM
            Cipher cipher = Cipher.getInstance(iv instanceof IvParameterSpec ? ALGORITHM_ENCRYPTION_OBSOLETE : ALGORITHM_ENCRYPTION);
            cipher.init(Cipher.DECRYPT_MODE, privateKey, iv);
            byte[] decrypted = cipher.doFinal(decodedEncryptedPassword);
            return new String(decrypted);
        } catch (BadPaddingException | IllegalBlockSizeException ibse) {
            throw new SecRuntimeException("Either private key or encrypted password might not be correct. Please check both.  Root cause: "
                    + ExceptionUtils.getRootCauseMessage(ibse), ibse);
        } catch (Exception exc) {
            throw new SecRuntimeException("Error occurred while decrypting the password with the key! Root cause: "
                    + ExceptionUtils.getRootCauseMessage(exc), exc);
        }
    }


    public static AlgorithmParameterSpec getSaltParameter(byte[] buff) {
        AlgorithmParameterSpec iv;
        // this is for back compatibility  - older versions were using "CBC" with IV to null
        // the GCM  is a new enforced algorithm  where GCM salt parameter
        if (buff[0] == '#' && buff[IV_GCM_SIZE + 1] == '#') {
            iv = new GCMParameterSpec(GCM_TAG_LENGTH_BIT, Arrays.copyOfRange(buff, 1, IV_GCM_SIZE + 1));
        } else {
            iv = NULL_IV;
        }
        return iv;
    }

    public static SecretKey getSecretKey(byte[] buff) {
        byte[] skey;
        if (buff[0] == '#') {
            // EAS Key value is after salt value following the patter: // #salt#key
            skey = Arrays.copyOfRange(buff, IV_GCM_SIZE + 2, buff.length);
        } else {
            skey = buff;
        }
        return new SecretKeySpec(skey, ALGORITHM_KEY);
    }

    private static SecureRandom getRandomInstance(boolean devMode) {
        try {
            return devMode ? new SecureRandom() : SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException exc) {
            throw new SecRuntimeException("Error occurred while generating strong secret key for encryption", exc);
        }
    }
}
