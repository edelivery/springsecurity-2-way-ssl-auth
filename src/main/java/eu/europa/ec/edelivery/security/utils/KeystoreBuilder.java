package eu.europa.ec.edelivery.security.utils;


import eu.europa.ec.edelivery.exception.SecRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bouncycastle.operator.OperatorCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Class helper for building the keystore/truststore. If the keystore for the given parameters already exists and is accessible,
 * then nothing new is created. If the keystore exists and cannot be accessed, then a backup file is generated from the existing
 * keystore, and the new one is made with the given parameters.
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class KeystoreBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(KeystoreBuilder.class);
    protected final KeystoreData keystoreData;


    public static KeystoreBuilder create() {
        return new KeystoreBuilder();
    }

    private KeystoreBuilder() {
        keystoreData = new KeystoreData();
    }

    public KeystoreBuilder filename(String filename) {
        keystoreData.filename = filename;
        return this;
    }

    public KeystoreBuilder secretToken(String secretToken) {
        keystoreData.secretToken = secretToken;
        return this;
    }

    public KeystoreBuilder keystoreType(String keystoreType) {
        keystoreData.keystoreType = keystoreType;
        return this;
    }

    public KeystoreBuilder subjectChain(String ... subjectChain) {
        keystoreData.subjectChain = subjectChain;
        return this;
    }

    public KeystoreBuilder aliasList(String ... aliasList) {
        keystoreData.aliasList = aliasList;
        return this;
    }

    public KeystoreBuilder folder(File folder) {
        keystoreData.folder = folder;
        return this;
    }

    public KeystoreBuilder testMode(boolean testMode) {
        keystoreData.testMode = testMode;
        return this;
    }

    public void reset() {
        keystoreData.reset();
    }

    public KeystoreData build() {
        keystoreData.init();

        File keystoreFile = new File(keystoreData.folder, keystoreData.filename);
        if (keystoreFile.exists()) {
            // try to open it
            KeyStore newTrustStore = null;
            try {
                newTrustStore = KeystoreUtils.loadKeystore(keystoreFile, keystoreData.secretToken, keystoreData.keystoreType);
            } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
                LOG.error("Can not open keystore: [{}]  with type: [{}]! Error: [{}]", keystoreFile.getAbsolutePath(), keystoreData.keystoreType, ExceptionUtils.getRootCauseMessage(e));
            }
            if (newTrustStore != null) {
                LOG.debug("Keystore [{}] already exists.", keystoreFile.getAbsolutePath());
                keystoreData.resultKeystore = newTrustStore;
                return keystoreData;
            }
            LOG.error("Keystore [{}] already exists and it can not be accessed. Create a backup and try with an empty keystore!", keystoreFile.getAbsolutePath());
            FileUtils.backupFileIfExists(keystoreFile);
        }
        // if truststore does not exist create a new file
        LOG.info("Generate new keystore file [{}].", keystoreFile.getAbsolutePath());
        generateNewKeystore(keystoreFile);

        return keystoreData;
    }

    private void generateNewKeystore(File keystoreFile){

        try {
            keystoreData.resultKeystore = KeystoreUtils.createNewKeystore(keystoreFile, keystoreData.secretToken, keystoreData.keystoreType);
        } catch (IOException | GeneralSecurityException  ex) {
            String msg = "Can not generate new keystore [" + keystoreFile.getAbsolutePath() + "]: " + ex.getMessage();
            throw new SecRuntimeException(msg, ex);
        }

        // check if keystore is empty then generate cert for user
        if (keystoreData.subjectChain == null || keystoreData.subjectChain.length == 0 ) {
            LOG.debug("No new test Certificates with Keys generated for the keystore!");
            return;
        }

        try (FileOutputStream out = new FileOutputStream(keystoreFile)) {
            // generate and add certificates
            X509CertificateUtils.createAndStoreCertificateWithChain(
                    keystoreData.subjectChain,
                    keystoreData.aliasList,
                    keystoreData.resultKeystore, keystoreData.secretToken);
            // store the keystore
            keystoreData.resultKeystore.store(out, keystoreData.secretToken.toCharArray());

        } catch (IOException | GeneralSecurityException | OperatorCreationException  ex) {
            String msg = "Can not generate new test keys/certificates for the keystore [" + keystoreFile.getAbsolutePath() + "]: " + ex.getMessage();
            throw new SecRuntimeException(msg, ex);
        }

    }

    /**
     * Keystore build result class
     */
    public static class KeystoreData {
        String secretToken;
        String filename;
        String keystoreType;
        String[] subjectChain;
        String[] aliasList;
        File folder;
        KeyStore resultKeystore;
        boolean testMode = false;

        protected void reset() {
            secretToken = null;
            filename = null;
            keystoreType = null;
            folder = null;
            testMode = false;
            resultKeystore = null;
            subjectChain = null;
            aliasList = null;
        }

        protected void init() {
            if (StringUtils.isBlank(secretToken)) {
                secretToken = SecurityUtils.generateAuthenticationToken(testMode);
            }
            if (StringUtils.isBlank(keystoreType)) {
                keystoreType = "PKCS12";
            }
            if (StringUtils.isBlank(filename)) {
                filename = "smp-keystore" +
                        ((StringUtils.equalsIgnoreCase("PKCS12", keystoreType)) ? ".p12" : ".jks");
            }
            if (folder == null) {
                folder = Paths.get("").toAbsolutePath().toFile();
            }
        }

        public String getSecretToken() {
            return secretToken;
        }

        public String getFilename() {
            return filename;
        }

        public String getKeystoreType() {
            return keystoreType;
        }

        public File getFolder() {
            return folder;
        }

        public String[] getSubjectChain() {
            return subjectChain;
        }

        public String[] getAliasList() {
            return aliasList;
        }

        public KeyStore getResultKeystore() {
            return resultKeystore;
        }

        public boolean isTestMode() {
            return testMode;
        }
    }

}
