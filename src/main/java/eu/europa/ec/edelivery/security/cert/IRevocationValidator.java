package eu.europa.ec.edelivery.security.cert;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 * The purpose of the interface is to evaluate if certificate revoked. The implementation can
 * use certificate CRL or OCSP to validate the certificate revocation status.
 */
public interface IRevocationValidator {

    /**
     * Method validates if certificate is revoked. In case of revocation or verification error it throws and error.
     *
     * @param cert the X509Certificate for the revocation validation.
     * @throws CertificateException the subclass CertificateRevokedException is thrown if certificate is revoked. In case of error CertificateException is thrown.
     */
    boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer) throws CertificateException;

    /**
     * Method validates if certificate is revoked. In case of revocation or verification error it throws and error.
     *
     * @param cert            -  X509Certificate for the revocation validation.
     * @param verificationURL list of verification addresses
     * @throws CertificateException
     */
    boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer, List<String> verificationURL) throws CertificateException;

    /**
     * Method fetches the CRL and Validates if serial number for the certificate is on the revocation list.
     *
     * @param serial
     * @param verificationURL
     */
    boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, String verificationURL) throws CertificateException;

    /**
     * Method tries to validate serial number with different URLs if it can not validate with the first
     * it trues  with next url. Verification is completed when with the fist successfully verification.
     *
     * @param serial
     * @param verificationURLs
     */
    boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, List<String> verificationURLs) throws CertificateException;

    /**
     * Set http implementation for fetching the revocation data.
     *
     * @param connection
     */
    void setHttpConnection(URLFetcher connection);

    URLFetcher getHttpConnection();

}

