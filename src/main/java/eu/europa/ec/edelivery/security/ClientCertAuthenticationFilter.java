/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import eu.europa.ec.edelivery.exception.ClientCertParseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.util.HtmlUtils;

import jakarta.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil.*;
import static java.util.Locale.US;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

/**
 * The purpose of this class is ONLY to extract the necessary information on the principal from the
 * incoming request HTTP "Client-Cert" header. External authentication systems (as RP: Blue Coat, F5,..)
 * must provide this information.
 * It is assumed that the external system is responsible for the accuracy of the data and preventing
 * the submission of forged values.
 *
 * <p>
 * This {@code {@link ClientCertAuthenticationFilter }} is a pre-authenticated filter which obtains the user's certificate from a request header.
 * <p>
 * As with most pre-authenticated scenarios, it is essential that the external
 * authentication system is set up correctly as this filter does no authentication
 * whatsoever. All the protection is assumed to be provided externally and if this filter
 * is included inappropriately in a configuration, it would be possible to assume the
 * identity of a user merely by setting the header value.
 * <p>
 * If the header is missing from the request, {@code getPreAuthenticatedPrincipal} returns null value,
 * so other authentication mechanisms could still authenticate request.
 * <p>
 * By default {@code {@link ClientCertAuthenticationFilter }} is turned off, which means "Client-Cert" header is skipped.
 * It can be activated on by setting {@code setBlueCoatEnabled()} to true.
 * <p>
 * <b>IMPIRTANT:</b> Never activate this filter ON in environments that do not stay behind well configured reverse-proxy.
 * In such case attacker can provide whatever header value and authenticate as any chosen user.
 *
 * @author Pawel Gutowski
 */
public class ClientCertAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter implements AuthenticationDetailsSource<HttpServletRequest, PreAuthenticatedCertificatePrincipal> {

    private static final String[] HEADER_ATTR_SUBJECT = {"subject"};
    private static final String[] HEADER_ATTR_SERIAL = {"serial", "sno"};
    private static final String[] HEADER_ATTR_VALID_FROM = {"validFrom"};
    private static final String[] HEADER_ATTR_VALID_TO = {"validTo"};
    private static final String[] HEADER_ATTR_ISSUER = {"issuer"};
    private static final String[] HEADER_POLICY_OIDS = {"policy_oids"};

    private static final String HEADER_NAME = "Client-Cert";

    // filter can be used in multithreaded environment -
    private final ThreadLocal<DateFormat> dateFormatThreadSafe = new ThreadLocal<DateFormat>() {

        @Override
        public DateFormat get() {
            return super.get();
        }

        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("MMM d hh:mm:ss yyyy zzz", US);
        }

        @Override
        public void remove() {
            super.remove();
        }

        @Override
        public void set(DateFormat value) {
            super.set(value);
        }

    };

    private boolean clientCertAuthenticationEnabled = false;

    public ClientCertAuthenticationFilter() {
        super.setAuthenticationDetailsSource(this);
    }

    @Override
    protected PreAuthenticatedCertificatePrincipal getPreAuthenticatedPrincipal(HttpServletRequest request) {

        String certHeader = request.getHeader(HEADER_NAME);
        if (!clientCertAuthenticationEnabled || certHeader == null) {
            if (!clientCertAuthenticationEnabled) {
                logger.debug("ClientCert authentication is disabled!");
            }
            return null;
        }
        logger.debug("Initializing ClientCert authentication with header: " + certHeader);
        return buildPrincipalFromHeader(certHeader);
    }

    /**
     * Not used, see implementation of {@code {@link RequestHeaderAuthenticationFilter}}
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "N/A";
    }

    @Override
    public PreAuthenticatedCertificatePrincipal buildDetails(HttpServletRequest request) {
        PreAuthenticatedCertificatePrincipal principal = getPreAuthenticatedPrincipal(request);
        logger.info("Extracted user details from 'Client-Cert' header: " + principal);
        return principal;
    }

    public PreAuthenticatedCertificatePrincipal buildPrincipalFromHeader(String certHeader) {
        logger.info("Build principal from 'Client-Cert' header: " + certHeader);
        try {

            Date validFrom = getDateField(certHeader, HEADER_ATTR_VALID_FROM);
            Date validTo = getDateField(certHeader, HEADER_ATTR_VALID_TO);
            String subjectDN = getDnField(certHeader, HEADER_ATTR_SUBJECT);
            String issuerDN = getDnField(certHeader, HEADER_ATTR_ISSUER);
            String serial = getField(certHeader, HEADER_ATTR_SERIAL);
            String policyOids = getField(certHeader, HEADER_POLICY_OIDS, false);

            return new PreAuthenticatedCertificatePrincipal(null,
                    subjectDN,
                    issuerDN,
                    serial,
                    validFrom,
                    validTo,
                    StringUtils.isBlank(policyOids) ? Collections.emptyList() : Arrays.asList(policyOids.split(",")));

        } catch (Exception e) {
            String message = "Malformed 'Client-Cert' authentication header. This might be a bug or environment configuration: " + certHeader;
            logger.error(message, e);
            throw new ClientCertParseException(message, e);
        }
    }

    private String getDnField(String header, String[] fieldAlternativeNames) throws UnsupportedEncodingException {
        String fieldValue = getField(header, fieldAlternativeNames);
        fieldValue = decodeUtfHexLiterals(fieldValue);
        fieldValue = smartEscapeDistinguishedName(fieldValue);
        fieldValue = removeUnwantedRdnPostfixAttr(fieldValue);

        return fieldValue;
    }

    private String getField(String header, String[] fieldAlternativeNames) throws UnsupportedEncodingException {
        return getField(header, fieldAlternativeNames, true);
    }

    private String getField(String header, String[] fieldAlternativeNames, boolean mandatory) throws UnsupportedEncodingException {
        for (String fieldName : fieldAlternativeNames) {
            // Header consists of "key=value" map separated by "&"
            // Pattern matches value preceded by "key=" and followed by separator "&" or end-of-line "$"
            Pattern fieldValuePattern = Pattern.compile(fieldName + "=(?<value>[^&]+)", CASE_INSENSITIVE);
            Matcher matcher = fieldValuePattern.matcher(header);
            if (matcher.find()) {
                String fieldValue = matcher.group("value");
                fieldValue = URLDecoder.decode(fieldValue, StandardCharsets.UTF_8.name());
                fieldValue = HtmlUtils.htmlUnescape(fieldValue);

                return fieldValue;
            }
        }
        if (mandatory) {
            throw new BadCredentialsException("ClientCert authentication header does not contain mandatory field: " + Arrays.toString(fieldAlternativeNames) + ": " + header);
        }
        return null;
    }

    private Date getDateField(String header, String[] fieldName) throws UnsupportedEncodingException {
        String dateStr = getField(header, fieldName);
        try {
            return dateFormatThreadSafe.get().parse(dateStr);
        } catch (ParseException e) {
            throw new BadCredentialsException("Invalid date format provided in ClientCert header: " + Arrays.toString(fieldName) + "=" + dateStr);
        }
    }

    public void setClientCertAuthenticationEnabled(boolean isEnabled) {
        if (isEnabled) {
            logger.warn("Client cert authentication is enabled! Use this authentication ONLY when server is set behind reverse-proxy with secured handling of HTTP headers!");
        }
        this.clientCertAuthenticationEnabled = isEnabled;
    }

}
