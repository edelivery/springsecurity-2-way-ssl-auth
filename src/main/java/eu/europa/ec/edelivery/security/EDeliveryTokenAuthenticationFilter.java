package eu.europa.ec.edelivery.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

public class EDeliveryTokenAuthenticationFilter extends RequestHeaderAuthenticationFilter implements AuthenticationDetailsSource<HttpServletRequest, PreAuthenticatedTokenPrincipal> {
    @Deprecated
    public static final String S_HEADER_ADMIN_TOKEN = "Admin-Pwd";
    public static final String S_HEADER_MONITOR_TOKEN = "Monitor-Token";
    private List<String> principalRequestHeaders = Arrays.asList(S_HEADER_ADMIN_TOKEN, S_HEADER_MONITOR_TOKEN);

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        String principal = null;
        String header = null;
        for (String headerValue : principalRequestHeaders) {
            principal = request.getHeader(headerValue);
            if (!StringUtils.isBlank(principal)) {
                header = headerValue;
                break;
            }
        }
        if (principal != null) {
            return new PreAuthenticatedTokenPrincipal(header, principal);
        }
        return null;
    }

    @Override
    public PreAuthenticatedTokenPrincipal buildDetails(HttpServletRequest httpServletRequest) {
        return (PreAuthenticatedTokenPrincipal) getPreAuthenticatedPrincipal(httpServletRequest);
    }
}
