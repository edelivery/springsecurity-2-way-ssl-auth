package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.exception.ClientCertParseException;
import eu.europa.ec.edelivery.security.PreAuthenticatedCertificatePrincipal;
import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.ldap.Rdn;
import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 * X509Certificate utils as extracting CRL distribution points, creating demo certificated and keys..
 */

public class X509CertificateUtils {
    private static final Logger LOG = LoggerFactory.getLogger(X509CertificateUtils.class);

    // Certificate manipulation util with static stateless methods!
    private X509CertificateUtils() {
    }

    /**
     * Extracts all CRL distribution point URLs from the
     * "CRL Distribution Point" extension in a X.509 certificate. If CRL
     * distribution point extension is unavailable, returns an empty list.
     */
    public static List<String> getCrlDistributionPoints(X509Certificate cert) throws CertificateParsingException {

        ASN1Primitive crlObjectArray = extractExtension(cert, Extension.cRLDistributionPoints.getId());
        if (crlObjectArray == null) {
            return Collections.emptyList();
        }

        CRLDistPoint distPoint = CRLDistPoint.getInstance(crlObjectArray);
        List<String> crlUrls = new ArrayList<>();
        for (DistributionPoint dp : distPoint.getDistributionPoints()) {
            DistributionPointName dpn = dp.getDistributionPoint();
            // Look for URIs in fullName
            if (dpn == null || dpn.getType() != DistributionPointName.FULL_NAME) {
                LOG.debug("Skip DistributionPointName [{}]! Searching only for URIs in fullName!", dpn);
                continue;
            }
            GeneralName[] genNames = GeneralNames.getInstance(dpn.getName()).getNames();

            // Look for an URI
            for (GeneralName genName : genNames) {
                if (genName.getTagNo() != GeneralName.uniformResourceIdentifier) {
                    LOG.debug("Skip DistributionPointLocation [{}]! It is not a uniformResourceIdentifier!", genName);
                    continue;
                }
                String urlValue = extractURLAsString(genName);
                if (StringUtils.isNotBlank(urlValue)) {
                    crlUrls.add(urlValue);
                }
            }

        }
        return crlUrls;
    }

    /**
     * Authority Information Access (AIA) is a non-critical extension in an X509 Certificate. This contains the
     * URL of the OCSP endpoint if one is available.
     *
     * @param cert is the certificate
     * @return a lit of URLs in AIA extension of the certificate which will hopefully contain an OCSP endpoint.
     * @throws CertificateParsingException
     */
    public static List<String> getOCSPLocations(X509Certificate cert) throws CertificateParsingException {

        //The extension value for Authority information access Points
        ASN1Primitive ocspObjectArray = extractExtension(cert, Extension.authorityInfoAccess.getId());
        if (ocspObjectArray == null) {
            return Collections.emptyList();
        }
        AuthorityInformationAccess authorityInformationAccess = AuthorityInformationAccess.getInstance(ocspObjectArray);

        List<String> ocspUrlList = new ArrayList<>();
        AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
        for (AccessDescription accessDescription : accessDescriptions) {
            if (!StringUtils.equals(OCSPObjectIdentifiers.id_pkix_ocsp.getId(), accessDescription.getAccessMethod().getId())) {
                LOG.debug("Skip AuthorityInformationAccess with id [{}]! Searching only for certificate OCSP!", accessDescription.getAccessMethod().getId());
                continue;
            }
            String urlValue = extractURLAsString(accessDescription.getAccessLocation());
            if (StringUtils.isNotBlank(urlValue)) {
                ocspUrlList.add(urlValue);
            }
        }
        return ocspUrlList;
    }

    /**
     * Method validates if the object  is uniformResourceIdentifier and extracts the URL else returns null
     *
     * @param generalName extension object
     * @return url or null if the object is not uniformResourceIdentifier
     */
    public static String extractURLAsString(GeneralName generalName) {

        if (generalName == null || generalName.getTagNo() != GeneralName.uniformResourceIdentifier) {
            LOG.debug("General name [{}] is not uniformResourceIdentifier!", generalName);
            return null;
        }
        ASN1IA5String ia5String = ASN1IA5String.getInstance(generalName.getName());
        return ia5String.getString();
    }

    /**
     * Method extracts nested extension asASN1Primitive Object for given extension
     *
     * @param certificate the certificate
     * @param extensionId the extension id
     * @return return the ASN1Primitive or null if the extension for the extension does not exist.
     * @throws CertificateParsingException if extension exists but failed to read/parse.
     */
    public static ASN1Primitive extractExtension(X509Certificate certificate, String extensionId) throws CertificateParsingException {
        //The extension value for Authority information access Points
        byte[] aiaExt = certificate.getExtensionValue(extensionId);
        if (aiaExt == null) {
            LOG.debug("Certificate [{}] doesn't have the extension id [{}]!", certificate.getSubjectDN(), extensionId);
            return null;
        }

        ASN1InputStream oAsnInStream = new ASN1InputStream(
                new ByteArrayInputStream(aiaExt));
        ASN1Primitive derObject;
        try {
            derObject = oAsnInStream.readObject();
        } catch (IOException e) {
            throw new CertificateParsingException("Error occurred whole while extracting extension [" + extensionId + "]!", e);
        }
        DEROctetString dosObject = (DEROctetString) derObject;
        byte[] ocspExtOctets = dosObject.getOctets();
        ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(ocspExtOctets));
        ASN1Primitive derObj2;
        try {
            derObj2 = oAsnInStream2.readObject();
        } catch (IOException e) {
            throw new CertificateParsingException("Error occurred whole while extracting extension [" + extensionId + "]!", e);
        }
        return derObj2;
    }

    public static X509Certificate getX509Certificate(byte[] certBytes) throws CertificateException {
        InputStream is = new ByteArrayInputStream(certBytes);
        return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(is);
    }

    /**
     * Method retrieves https. If https does not exist it return http distribution list.
     * (LDAP is not allowed (FW OPEN) in targeted network)
     * X509CertificateUtilsTest
     *
     * @param urlList
     * @return
     */
    public static String extractHttpCrlDistributionPoint(List<String> urlList) {
        String httpsUrl = null;
        String httpUrl = null;
        for (String url : urlList) {
            String newUrl = url.trim();
            if (newUrl.toLowerCase().startsWith("https://")) {
                httpsUrl = newUrl;
            } else if (newUrl.toLowerCase().startsWith("http://")) {
                httpUrl = newUrl;

            }
        }
        return httpsUrl == null ? httpUrl : httpsUrl;
    }

    public static String getCrlDistributionUrl(X509Certificate cert) {
        if (cert == null) {
            return null;
        }
        List<String> list;
        try {
            list = getCrlDistributionPoints(cert);
        } catch (CertificateParsingException e) {
            throw new ClientCertParseException("Error occurred while parsing the Certificate!. Error: ["+ ExceptionUtils.getRootCauseMessage(e)+"]", e);
        }
        return list.isEmpty() ? null : extractHttpCrlDistributionPoint(list);
    }

    public static PreAuthenticatedCertificatePrincipal extractPrincipalFromCertificate(X509Certificate cert) {

        String subject = cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
        String issuer = cert.getIssuerX500Principal().getName(X500Principal.RFC2253);
        BigInteger serial = cert.getSerialNumber();

        return new PreAuthenticatedCertificatePrincipal(cert, subject, issuer, serial, cert.getNotBefore(), cert.getNotAfter());
    }

    public static boolean hasDistinguishedNameAllRequiredValues(String distinguishedName, String[] requiredValues) {

        // Make a map from type to name
        final Map<String, List<Rdn>> parts = DistinguishedNamesCodingUtil.getAllRDNValues(distinguishedName);
        List<String> keySet = parts.keySet().stream().map(String::toLowerCase).collect(Collectors.toList());

        for (final String code : requiredValues) {
            if (!keySet.contains(code.toLowerCase())) {
                return false;
            }
        }
        return true;
    }

    public static X509Certificate getX509Certificate(String publicKey) throws CertificateException {
        // if certificate has "begin certificate" - then is PEM encoded
        if (publicKey.contains("BEGIN CERTIFICATE")) {
            return getX509Certificate(publicKey.getBytes());
        } else {
            byte[] buff;
            // try do decode
            try {
                buff = Base64.getDecoder().decode(publicKey.getBytes());
            } catch (java.lang.IllegalArgumentException ex) {
                buff = publicKey.getBytes();
            }
            return getX509Certificate(buff);
        }
    }

    /**
     * Generate certificate with given parameters
     *
     * @param serialNumber        -certificate serial numner
     * @param subjectPublicKey    - certificate public key
     * @param subjectDn           - subject distinguish name
     * @param notBefore           - certificate valid from date-time
     * @param notAfter            - certificate valid to date-time
     * @param issuerDn            - issuer certificate. If null a self-signed certificate is generated
     * @param certSignerKey       - private key for singing the certificate. I case of self-signed certificate it must be Certificate private key
     * @param caFlag              - set basic constraint '2.5.29.19' (isCA) set to true and set max path length
     * @param pathLength          - path length in case of caFlag is set to true
     * @param keyUsage            - key usage
     * @param crlURls             - list of CRL urls
     * @param ocspUrls            -
     * @param certificatePolicies -  add certificate policy extensions with the policu OIDs
     * @return
     * @throws IOException
     * @throws IllegalStateException
     * @throws CertificateException
     * @throws OperatorCreationException
     */
    public static X509Certificate generateCertificate(BigInteger serialNumber, PublicKey subjectPublicKey, String subjectDn,
                                                      OffsetDateTime notBefore, OffsetDateTime notAfter,
                                                      String issuerDn,
                                                      PrivateKey certSignerKey,
                                                      boolean caFlag, int pathLength, KeyUsage keyUsage,
                                                      List<String> crlURls,
                                                      List<String> ocspUrls,
                                                      List<String> certificatePolicies)
            throws IOException, IllegalStateException, CertificateException, OperatorCreationException {


        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
                new X500Name(StringUtils.isBlank(issuerDn) ? subjectDn : issuerDn),
                serialNumber == null ? BigInteger.TEN : serialNumber,
                Date.from(notBefore.toInstant()), Date.from(notAfter.toInstant()),
                new X500Name(subjectDn),
                SubjectPublicKeyInfo.getInstance(subjectPublicKey.getEncoded()));

        // set basic basicConstraint  for all except the last leaf certificate
        if (caFlag) {
            if (-1 == pathLength) {
                certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
            } else {
                certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(pathLength));
            }
        }

        if (null != keyUsage) {
            certBuilder.addExtension(Extension.keyUsage, true, keyUsage);
        }

        // add certificate policies
        if (certificatePolicies != null) {
            addExtensionCertificatePolicies(certBuilder, certificatePolicies);
        }
        // add crl urls to leaf certificate
        if (crlURls != null && !crlURls.isEmpty()) {
            addExtensionCRLUrls(certBuilder, crlURls);
        }
        // add OCSP urls to leaf certificate
        if (ocspUrls != null && !ocspUrls.isEmpty()) {
            addExtensionOCSPUrls(certBuilder, ocspUrls);
        }

        String signAlgName = getJCESigningAlgorithmForPrivateKey(certSignerKey);
        ContentSigner sigGen = new JcaContentSignerBuilder(signAlgName)
                .build(certSignerKey);

        return new JcaX509CertificateConverter().getCertificate(certBuilder.build(sigGen));

    }

    public static void addExtensionCertificatePolicies(X509v3CertificateBuilder certBuilder, List<String> certificatePolicies) throws CertIOException {
        if (certificatePolicies.isEmpty()) {
            return;
        }
        List<PolicyInformation> policyInformationList = certificatePolicies.stream().map(certificatePolicy -> {
            ASN1ObjectIdentifier policyObjectIdentifier = new ASN1ObjectIdentifier(certificatePolicy);
            return new PolicyInformation(policyObjectIdentifier);
        }).collect(Collectors.toList());

        certBuilder.addExtension(Extension.certificatePolicies, false,
                new DERSequence(policyInformationList.toArray(new PolicyInformation[]{})));
    }

    public static void addExtensionCRLUrls(X509v3CertificateBuilder certBuilder, List<String> distributionList) throws CertIOException {
        if (distributionList.isEmpty()) {
            return;
        }
        List<DistributionPoint> distributionPoints = distributionList.stream().map(url -> {
            DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                    new GeneralName(GeneralName.uniformResourceIdentifier, url)));

            return new DistributionPoint(distPointOne, null, null);
        }).collect(Collectors.toList());
        certBuilder.addExtension(Extension.cRLDistributionPoints, false, new CRLDistPoint(distributionPoints.toArray(new DistributionPoint[]{})));
    }

    public static void addExtensionOCSPUrls(X509v3CertificateBuilder certBuilder, List<String> ocspUrls) throws CertIOException {
        if (ocspUrls.isEmpty()) {
            return;
        }
        List<AccessDescription> accessDescription = ocspUrls.stream().map(url ->
                new AccessDescription(OCSPObjectIdentifiers.id_pkix_ocsp, new GeneralName(GeneralName.uniformResourceIdentifier, url))
        ).collect(Collectors.toList());

        certBuilder.addExtension(Extension.authorityInfoAccess, false,
                new AuthorityInformationAccess(accessDescription.toArray(new AccessDescription[]{})));
    }

    public static X509Certificate createAndStoreSelfSignedCertificate(String subject, String alias, KeyStore keystore, String secToken) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, OperatorCreationException, IOException, KeyStoreException  {
        return createAndStoreSelfSignedCertificate(null, subject, alias, keystore, secToken);
    }

    public static X509Certificate createAndStoreSelfSignedCertificate(BigInteger serial, String subject, String alias, KeyStore keystore, String secToken) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, OperatorCreationException, IOException, KeyStoreException {

        // generate keys
        KeyPair key = generateKeyPair(CertificateKeyType.RSA_2048);
        KeyUsage usage = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.nonRepudiation);

        X509Certificate cert = generateCertificate(serial, key.getPublic(), subject,
                OffsetDateTime.now().minusDays(1),
                OffsetDateTime.now().plusYears(5),
                null, key.getPrivate(),
                false, -1,
                usage, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList());

        keystore.setKeyEntry(alias, key.getPrivate(), secToken.toCharArray(), new X509Certificate[]{cert});
        return cert;
    }

    /**
     * Create certificate chain and store them to the keystore.
     * The method is intended for generating test certificates chain for init/demo configurations of artefacts (smp/sml).
     *
     * @param subjects array of the certificate subject in reverse order from ROOT CA to  leaf certificate
     * @param aliases  list of aliases corresponds the order of the subjects. if the certificate for alias already exists in the keystore
     *                 the keyststore certificate is used instead of creating new certificate until the first issuer certificate is not found. Leave
     *                 empty/null alias to enforce certificate creating!
     * @param keystore keystore to store the created certificates and keys
     * @param secToken security token to protect the added key entry to keystore
     * @return return list of aliases which corresponds the order of the subjects
     * @throws Exception
     */
    public static List<String> createAndStoreCertificateWithChain(String[] subjects,
                                                                  String[] aliases,
                                                                  KeyStore keystore,
                                                                  String secToken) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, OperatorCreationException, NoSuchProviderException, InvalidAlgorithmParameterException, IOException {
        return createAndStoreCertificateWithChain(null, subjects, aliases, keystore, secToken);
    }

    /**
     * Create certificate chain and store them to the keystore.
     * The method is intended for generating test certificates chain for init/demo configurations of artefacts (smp/sml).
     *
     * @param leafSerial serial number for the leaf certificate!
     * @param subjects   array of the certificate subject in reverse order from ROOT CA to  leaf certificate
     * @param aliases    list of aliases corresponds the order of the subjects. if the certificate for alias already exists in the keystore
     *                   the keyststore certificate is used instead of creating new certificate until the first issuer certificate is not found. Leave
     *                   empty/null alias to enforce certificate creating!
     * @param keystore   keystore to store the created certificates and keys
     * @param secToken   security token to protect the added key entry to keystore
     * @return return list of aliases which corresponds the order of the subjects
     * @throws Exception
     */
    public static List<String> createAndStoreCertificateWithChain(BigInteger leafSerial, String[] subjects,
                                                                  String[] aliases,
                                                                  KeyStore keystore,
                                                                  String secToken) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, OperatorCreationException, NoSuchProviderException, InvalidAlgorithmParameterException, IOException {
        OffsetDateTime validFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime validTo = OffsetDateTime.now().plusYears(5);
        return createAndStoreCertificateWithChain(leafSerial, subjects, aliases, validFrom, validTo, keystore, secToken);

    }

    /**
     * Create certificate chain and store them to the keystore.
     * The method is intended for generating test certificates chain for init/demo configurations of artefacts (smp/sml).
     *
     * @param leafSerial serial number for the leaf certificate!
     * @param subjects   array of the certificate subject in reverse order from ROOT CA to  leaf certificate
     * @param aliases    list of aliases corresponds the order of the subjects. if the certificate for alias already exists in the keystore
     *                   the keyststore certificate is used instead of creating new certificate until the first issuer certificate is not found. Leave
     *                   empty/null alias to enforce certificate creating!
     * @param validFrom  OffsetDateTime from when the certificate is valid
     * @param validTo    OffsetDateTime to when the certificate is valid
     * @param keystore   keystore to store the created certificates and keys
     * @param secToken   security token to protect the added key entry to keystore
     * @return return list of aliases which corresponds the order of the subjects
     * @throws Exception
     */
    public static List<String> createAndStoreCertificateWithChain(BigInteger leafSerial, String[] subjects,
                                                                  String[] aliases,
                                                                  OffsetDateTime validFrom,
                                                                  OffsetDateTime validTo,
                                                                  KeyStore keystore,
                                                                  String secToken) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, OperatorCreationException, NoSuchProviderException, InvalidAlgorithmParameterException, IOException {
        BigInteger[] serialNumbers = new BigInteger[subjects.length];

        serialNumbers[serialNumbers.length - 1] = leafSerial;
        return createAndStoreCertificateWithChain2(serialNumbers, subjects, aliases, validFrom, validTo, null, null, null,
                null, keystore, secToken);
    }

    /**
     * Method generate the certificate chain  and stores it to the given keystore.
     * Certificate data in arrays must be given in order from leaf to CA. If certificate+key is found in provided keystore
     * then certificat+key is taken from keystore.
     * <p>
     * Note leaf certificate has key ussage
     * KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.nonRepudiation
     * others have KeyUsage.keyCertSign
     *
     * @param serialNumbers           - serial numbers from leaf to CA. If Serial numbers  are not given then it is randomly
     * @param subjects                - subject DN names, from Leaf to CA
     * @param aliases                 - aliases. If aliases are not given or if there are null values in provided table - na alias is generated based on DN
     * @param validFrom               - valid from date,
     * @param validTo                 - valid from date
     * @param certificateKeyTypes     - List of certificate key types from leaf to CA.
     * @param certificatePoliciesOids - certificate Policies ID list
     * @param leafCRLs                - certificate CLR values
     * @param leafOcspUrls            - certificate OCSP addresses values
     * @param keystore                - keystore
     * @param secToken                keystore and key passwords.
     * @return List of aliases for generated certificates..
     * @throws Exception
     */
    public static List<String> createAndStoreCertificateWithChain2(BigInteger[] serialNumbers,
                                                                   String[] subjects,
                                                                   String[] aliases,
                                                                   OffsetDateTime validFrom,
                                                                   OffsetDateTime validTo,
                                                                   CertificateKeyType[] certificateKeyTypes,
                                                                   List<List<String>> certificatePoliciesOids,
                                                                   List<String> leafCRLs,
                                                                   List<String> leafOcspUrls,
                                                                   KeyStore keystore,
                                                                   String secToken) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CertificateException, OperatorCreationException, IOException {


        // generate keys
        KeyUsage leafKeyUsage = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.nonRepudiation);
        X509Certificate[] certs = new X509Certificate[subjects.length];

        PrivateKey issuerKey = null;
        String issuerCertDn = null;

        List<String> actualAliases = new ArrayList<>();
        // flag that no certificates were created from the subject list.
        boolean chainCertCreated = false;

        for (int i = 0; i < subjects.length; i++) {
            int iRevertedIndex = subjects.length - 1 - i;
            String alias = aliases != null && aliases.length > i ? aliases[i] : null;


            // the first certificate is issue in a chain!
            boolean isIssuer = subjects.length > 0 && i != subjects.length - 1;
            X509Certificate cert;
            PrivateKey privateKey;

            // get key and certificate from keystore or create a new one
            if (!chainCertCreated && StringUtils.isNotBlank(alias) && keystore.containsAlias(alias)) {
                cert = (X509Certificate) keystore.getCertificate(alias);
                certs[iRevertedIndex] = cert;
                privateKey = (PrivateKey) keystore.getKey(alias, secToken.toCharArray());
                issuerCertDn = cert.getSubjectX500Principal().getName();
            } else {
                String subject = subjects[i];
                CertificateKeyType crtKeyType = certificateKeyTypes == null
                        || certificateKeyTypes.length <= i ? null : certificateKeyTypes[i];
                // if crtKeyType is null, then set default key type as RSA_2048
                crtKeyType = crtKeyType == null ? CertificateKeyType.RSA_2048 : crtKeyType;
                List<String> certificatePolicies = certificatePoliciesOids != null && certificatePoliciesOids.size() > i ? certificatePoliciesOids.get(i) : Collections.emptyList();

                KeyPair key = generateKeyPair(crtKeyType);
                privateKey = key.getPrivate();

                BigInteger serialNumber = serialNumbers == null || serialNumbers.length <= i ? null : serialNumbers[i];
                cert = generateCertificate(serialNumber,
                        key.getPublic(),
                        subject,
                        validFrom,
                        validTo,
                        issuerCertDn,
                        issuerKey == null ? privateKey : issuerKey,
                        isIssuer,
                        subjects.length,
                        isIssuer ? null : leafKeyUsage,
                        isIssuer ? Collections.emptyList() : leafCRLs,
                        isIssuer ? Collections.emptyList() : leafOcspUrls,
                        certificatePolicies);
                // add key to keystore
                certs[iRevertedIndex] = cert;
                alias = KeystoreUtils.addKeyEntry(keystore, privateKey,
                        Arrays.copyOfRange(certs, iRevertedIndex, subjects.length),
                        secToken, alias);
                chainCertCreated = true;
                issuerCertDn = subject;
            }

            actualAliases.add(alias);
            issuerKey = privateKey;

        }
        return actualAliases;
    }

    /**
     * Method returns generated key for the CertificateGenerateKeyType
     *
     * @param certificateKeyType
     * @return
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     */
    public static KeyPair generateKeyPair(CertificateKeyType certificateKeyType) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {

        KeyPairGenerator generator = StringUtils.isNotBlank(certificateKeyType.getJceProvider()) ?
                KeyPairGenerator.getInstance(certificateKeyType.getAlgorithmName(), certificateKeyType.getJceProvider()) :
                KeyPairGenerator.getInstance(certificateKeyType.getAlgorithmName());
        generator.initialize(certificateKeyType.getAlgorithmParameterSpec());
        return generator.generateKeyPair();
    }

    /**
     * Get 'default' certificate signing algorithm for issuer key
     *
     * @param key - Issuer Key to sign certificate
     * @return signing algorithm
     */
    public static String getJCESigningAlgorithmForPrivateKey(PrivateKey key) {
        if (key == null || StringUtils.isBlank(key.getAlgorithm())){
            LOG.warn("Can define signing algorithm for null key/key.algorithm");
            return null;
        }
        switch (key.getAlgorithm()) {
            case "Ed448":
                return "Ed448";
            case "Ed25519":
                return "Ed25519";
            case "ECDSA":
                return "SHA256withECDSA";
            case "RSA":
                return "SHA256WITHRSA";
            default:
                LOG.debug("Key algorithm [{}], is not supported.", key.getAlgorithm());
                return null;
        }
    }

    /**
     * Extracts all Certificate Policy identifiers the "Certificate policy" extension of X.509.
     * If the certificate policy extension is unavailable, returns an empty list.
     *
     * @param cert a X509 certificate
     * @return the list of CRL urls of certificate policy identifiers
     */
    public static List<String> getCertificatePolicyIdentifiers(X509Certificate cert) throws CertificateException {

        byte[] certPolicyExt = cert.getExtensionValue(org.bouncycastle.asn1.x509.Extension.certificatePolicies.getId());
        if (certPolicyExt == null) {
            return new ArrayList<>();
        }

        CertificatePolicies policies;
        try {
            policies = CertificatePolicies.getInstance(JcaX509ExtensionUtils.parseExtensionValue(certPolicyExt));
        } catch (IOException e) {
            throw new CertificateException("Error occurred while reading certificate policy object!", e);
        }

        return Arrays.stream(policies.getPolicyInformation())
                .map(PolicyInformation::getPolicyIdentifier)
                .map(ASN1ObjectIdentifier::getId)
                .map(StringUtils::trim)
                .collect(Collectors.toList());
    }

    /**
     * The method is a basic Certificate validator and it validates: if certificate is valid,
     * if the certificate subject matches the regular expression configured.
     * But it does not do PKIX validation with truststore.
     *
     * @param x509Certificate - certificate to validate
     * @param subjectRegExp - regular expression for validating the certificate subject, if null then the validation is skipped
     * @param allowedCertificatePolicies - list of allowed certificate policies, if null/empty then the validation is skipped
     * @throws CertificateException - if the certificate is not valid or the subject does not match the regular expression
     */
    public static void basicCertificateValidation(X509Certificate x509Certificate, Pattern subjectRegExp, List<String> allowedCertificatePolicies) throws CertificateException {
        // validate if certificate is valid
        x509Certificate.checkValidity();
        if (subjectRegExp != null) {
            String subjectName = x509Certificate.getSubjectX500Principal().getName();
            Matcher matcher = subjectRegExp.matcher(subjectName);
            if (!matcher.matches()) {
                throw new CertificateException("Certificate subject does not match the regular expression configured!");
            } else {
                LOG.debug("Certificate subject [{}] matches the regular expression configured!", subjectName);
            }
        }
        if (allowedCertificatePolicies != null && !allowedCertificatePolicies.isEmpty()) {
            List<String> certPolicyList = X509CertificateUtils.getCertificatePolicyIdentifiers(x509Certificate);
            if (certPolicyList.isEmpty()) {
                throw new CertificateException("Certificate does not have any certificate policy!");
            }
            Optional<String> result = certPolicyList.stream().filter(allowedCertificatePolicies::contains).findFirst();
            if (!result.isPresent()) {
                throw new CertificateException("Certificate does not have allowed certificate policy!");
            }
        }
    }
}
