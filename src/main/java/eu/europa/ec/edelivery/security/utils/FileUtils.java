package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.exception.SecRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static java.lang.String.format;

/**
 * The purpose of the class are static/stateless methods file tools such as:
 * <ul>
 *      <li>test and backup if exists</li>
 * </ul>
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class FileUtils {
    private static final Logger LOG = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
    }

    /**
     * Method tests if the given file exists, and in case of existence, it creates a backup.
     * @param testFile to test and backup.
     */
    public static void backupFileIfExists(File testFile) {
        if (!testFile.exists()) {
            LOG.info("File: [{}] does not exists!", testFile.getAbsolutePath());
            return;
        }
        try {
            int i = 1;
            String fileFormat = testFile.getAbsolutePath() + ".%03d";
            File backupFileTarget = new File(format(fileFormat, i++));

            while (backupFileTarget.exists()) {
                backupFileTarget = new File(format(fileFormat, i++));
            }
            LOG.info("Backup file: [{}] to [{}]!", testFile.getAbsolutePath(),  backupFileTarget.getAbsolutePath());
            Files.move(testFile.toPath(), backupFileTarget.toPath(), StandardCopyOption.ATOMIC_MOVE);
        } catch (IOException ex) {
            String msg = "Error occurred while creating the backup of the file [" + testFile.getAbsolutePath() + "]: " + ex.getMessage();
            throw new SecRuntimeException(msg, ex);
        }
    }
}
