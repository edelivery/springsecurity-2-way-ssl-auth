package eu.europa.ec.edelivery.security.cert.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.StringUtils.*;

/**
 * Certificate revocation validation strategy types
 *
 *  <ul>
 *      <li>OCSP_CRL - OCSP first, CRL second if OCSP fails</li>
 *      <li>CRL_OCSP - CRL first, OCSP second if CRL fails</li>
 *      <li>OCSP_ONLY - OCSP only</li>
 *      <li>CRL_ONLY - CRL only</li>
 *      <li>NO_VALIDATION - no validation</li>
 *  </ul>
 *
 *  @author Joze RIHTARSIC
 *  @since 1.14
 */
public enum CertificateRVStrategyEnum {
    OCSP_CRL,
    CRL_OCSP,
    OCSP_ONLY,
    CRL_ONLY,
    NO_VALIDATION;

    private static final Logger LOG = LoggerFactory.getLogger(CertificateRVStrategyEnum.class);

    public static CertificateRVStrategyEnum fromCode(String name) {
        if (isBlank(name)) {
            LOG.debug("Use default OCSP_CRL value for blank code.");
            return OCSP_CRL;
        }
        try {
            return Enum.valueOf(CertificateRVStrategyEnum.class, upperCase(trim(name)));
        } catch (IllegalArgumentException e) {
            LOG.warn("Unknown CertificateRVStrategyEnum value: [{}]. Use default value OCSP_CRL", name);
            return OCSP_CRL;
        }
    }
}
