/**
 * (C) Copyright 2018 - European Commission | CEF eDelivery
 * <p>
 * Licensed under the EUPL, Version 1.2 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * \BDMSL\bdmsl-parent-pom\LICENSE-EUPL-v1.2.pdf or https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package eu.europa.ec.edelivery.security.cert;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 *
 * The purpose of the interface is URL fetcher implementation. URL can support multiple protocols as HTTP, HTTPS,
 * LDAP, etc.
 *
 */
public interface URLFetcher {

    InputStream fetchURL(String url) throws IOException;

    InputStream fetchURL(String url, Map<String, String> headers, InputStream stream) throws IOException;
}
