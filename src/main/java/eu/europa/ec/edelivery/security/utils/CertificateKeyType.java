package eu.europa.ec.edelivery.security.utils;

import org.bouncycastle.jcajce.spec.EdDSAParameterSpec;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;


/**
 * Supported keu algorithms for the certificate generation.
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public enum CertificateKeyType {
    Ed448(EdDSAParameterSpec.Ed448, new EdDSAParameterSpec(EdDSAParameterSpec.Ed448), BouncyCastleProvider.PROVIDER_NAME),
    Ed25519(EdDSAParameterSpec.Ed25519, new EdDSAParameterSpec(EdDSAParameterSpec.Ed25519), BouncyCastleProvider.PROVIDER_NAME),
    ECDSA_NIST_P256V1("ECDSA", ECNamedCurveTable.getParameterSpec("prime256v1"), null),
    RSA_1024("RSA", new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4), null),
    RSA_2048("RSA", new RSAKeyGenParameterSpec(2048, RSAKeyGenParameterSpec.F4), null);
    String algorithmName;
    String jceProvider;
    AlgorithmParameterSpec algorithmParameterSpec;


    CertificateKeyType(String algorithmName, AlgorithmParameterSpec algorithmParameterSpec, String jceProvider) {
        this.algorithmName = algorithmName;
        this.jceProvider = jceProvider;
        this.algorithmParameterSpec = algorithmParameterSpec;
    }

    public String getAlgorithmName() {
        return algorithmName;
    }

    public String getJceProvider() {
        return jceProvider;
    }

    public AlgorithmParameterSpec getAlgorithmParameterSpec() {
        return algorithmParameterSpec;
    }
}