package eu.europa.ec.edelivery.security.cert;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.wss4j.common.crypto.Merlin;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.x500.X500Principal;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * The purpose of the class is to evaluate certificate validity and whether the certificate should be trusted.
 * The trust verification is entirely based on the the Merlin wss4j implementation and is heavily inspired by
 * DomibusCertificateValidator.java
 *
 * @author Joze Rihtarsic
 * @since 1.9
 */
public class CertificateValidator extends Merlin {

    private static final Logger LOG = LoggerFactory.getLogger(CertificateValidator.class);

    Pattern subjectRegularExpressionPattern;
    List<IRevocationValidator> revocationValidatorList = new ArrayList<>();
    List<String> allowedCertificatePolicyOIDs;

    public CertificateValidator(IRevocationValidator crlVerifierService, KeyStore trustStore, String subjectRegularExpression) {
        this(crlVerifierService == null ? Collections.emptyList() : Collections.singletonList(crlVerifierService),
                trustStore, subjectRegularExpression, Collections.emptyList());// initialize merlin
    }

    public CertificateValidator(List<IRevocationValidator> revocationValidatorList, KeyStore trustStore, String subjectRegularExpression, List<String> allowedCertificatePolicyOIDs) {
        // initialize merlin
        if (!org.apache.xml.security.Init.isInitialized()) {
            org.apache.xml.security.Init.init();
        }
        this.revocationValidatorList.addAll(revocationValidatorList);
        if (!StringUtils.isEmpty(subjectRegularExpression)) {
            this.subjectRegularExpressionPattern = Pattern.compile(subjectRegularExpression);
        } else {
            this.subjectRegularExpressionPattern = Pattern.compile(".*");
        }
        this.allowedCertificatePolicyOIDs = allowedCertificatePolicyOIDs;
        // Set truststore to Merlin
        setTrustStore(trustStore);
    }

    /**
     * Method is used for certificate validation by the date validity, trust and revocation list. If certificate is not
     * valid, trusted or is revoked then the {@link CertificateException} it thrown.
     *
     * @param certificate
     * @throws CertificateException
     */

    public void validateCertificate(X509Certificate certificate) throws CertificateException {
        String subjectName = getSubjectDN(certificate);
        LOG.debug("Certificate validator for certificate: [{}]", subjectName);
        if (!isCertificateValid(certificate)) {
            throw new CertificateException("Certificate [" + subjectName + "] is not valid!");
        }

        // is certificate  trusted
        try {
            // user merlin trust implementation to check if we trust the certificate
            verifyTrust(certificate);
        } catch (WSSecurityException ex) {
            throw new CertificateException("Certificate [" + subjectName + "] is not trusted!", ex);
        }

        X509Certificate issuerCert = findIssuerCertificate(certificate);
        validateRevocationStatus(certificate, issuerCert);

        // verify the chain CRL
        if (!verifyCertificateChain(certificate)) {
            throw new CertificateException("Lookup certificate validator failed for " + subjectName + ". The certificate chain is not valid");
        }

        LOG.debug("The Certificate is valid and trusted: [{}]", subjectName);
    }

    /**
     * Method verifies if certificate is trusted. Input for verifications are
     * truststore and subject regular expression. Method does not verify CRL for the certificate.
     *
     * @param certificate
     * @throws WSSecurityException
     */
    protected void verifyTrust(X509Certificate certificate) throws WSSecurityException {
        super.verifyTrust(new X509Certificate[]{certificate},
                false, Collections.singleton(subjectRegularExpressionPattern));
    }

    /**
     * If certificate is valid true else it returns false or it trows exception if is revoked or if revocation
     * service fails to check the revocation.
     * The revocation is validated only if verification service is provided to the class.
     *
     * @param cert
     * @return true/false
     * @throws CertificateException
     */
    public boolean isCertificateValid(X509Certificate cert) {
        boolean isValid = checkValidity(cert);
        if (!isValid) {
            LOG.warn("Certificate is not valid:[{}] ", cert);
            return false;
        }
        return true;
    }

    public X509Certificate findIssuerCertificate(X509Certificate certificate) {


        X509Certificate foundIssuingCert = null;
        if (this.keystore != null) {

            foundIssuingCert = this.findIssuerCertificate(certificate, this.keystore);

        }

        if (foundIssuingCert == null && this.truststore != null) {
            foundIssuingCert = this.findIssuerCertificate(certificate, this.truststore);
        }
        return foundIssuingCert;
    }

    public X509Certificate findIssuerCertificate(X509Certificate certificate, KeyStore store) {

        X500Principal issuerRDN = certificate.getIssuerX500Principal();
        Object issuerName = this.createBCX509Name(issuerRDN.getName());
        try {

            Enumeration<String> e = store.aliases();
            while (e.hasMoreElements()) {
                String alias = e.nextElement();
                Certificate[] certs = store.getCertificateChain(alias);
                if (certs == null || certs.length == 0) {
                    Certificate cert = store.getCertificate(alias);
                    if (cert != null) {
                        certs = new Certificate[]{cert};
                    }
                }

                if (certs != null && certs.length > 0 && certs[0] instanceof X509Certificate) {
                    X509Certificate cert = (X509Certificate) certs[0];
                    X500Principal foundRDN = cert.getSubjectX500Principal();
                    Object certName = this.createBCX509Name(foundRDN.getName());
                    if (issuerName.equals(certName)) {
                        LOG.debug("Subject certificate match found using keystore alias {}", alias);
                        return cert;
                    }
                }
            }
        } catch (KeyStoreException e) {
            LOG.error("Error occurred while reading the keystore for " + getSubjectDN(certificate), e);
        }
        return null;
    }


    /**
     * Method validates the revocation status of the certificate. If the certificate is revoked then the
     * {@link CertificateRevokedException} is thrown.
     *
     * @param cert
     * @param issuer
     * @throws CertificateException
     */

    public void validateRevocationStatus(X509Certificate cert, X509Certificate issuer) throws CertificateException {
        if (revocationValidatorList == null || revocationValidatorList.isEmpty()) {
            LOG.debug("Not revocation validator service is registered (CRL or OCSP). Skip revocation validation");
            return;
        }


        for (IRevocationValidator revocationValidator : revocationValidatorList) {
            try {
                revocationValidator.isCertificateRevoked(cert, issuer);
                return;
            } catch (CertificateRevokedException exc) {
                throw exc;
            } catch (CertificateException exc) {
                LOG.error("Error occurred while revocation status verification!", exc);
            }
        }
        throw new CertificateException("Can not verify the revocation Status.");
    }

    protected boolean checkValidity(X509Certificate cert) {
        boolean result = false;
        try {
            cert.checkValidity();
            result = true;
        } catch (Exception e) {
            LOG.warn("Certificate is not valid [" + cert + "].", e);
        }

        return result;
    }

    /**
     * Methods validates the certificate chain. For that to happen the certificate itself or parent/direct issuer
     * certificate must be present in truststore
     *
     * @param cert
     * @throws CertificateException
     */
    public String getTrustAnchorAliasFromTrustStore(X509Certificate cert) throws CertificateException {
        String subjectDN = getSubjectDN(cert);
        LOG.debug("Verify certificate chain for certificate: [{}]", subjectDN);
        String alias;
        try {
            alias = getTrustStore().getCertificateAlias(cert);
            if (StringUtils.isEmpty(alias)) {
                LOG.debug("Certificate with subject [{}] was not found in truststore. Search for the Issuer alias.", subjectDN);
                alias = getDirectParentAliasFromTruststore(cert);
            }
        } catch (KeyStoreException e) {
            throw new CertificateException("Error occurred while reading the truststore for " + getSubjectDN(cert), e);
        }
        LOG.debug("Got trust anchor alias [{}] for certificate: [{}].", alias, subjectDN);

        return alias;
    }

    // Separated out to allow subclasses to override it
    @Override
    protected PKIXParameters createPKIXParameters(
            Set<TrustAnchor> trustAnchors, boolean enableRevocation
    ) throws InvalidAlgorithmParameterException {
        LOG.info("createPKIXParameters");
        PKIXParameters param = super.createPKIXParameters(trustAnchors, enableRevocation);
        if (!allowedCertificatePolicyOIDs.isEmpty()) {
            LOG.debug("Set allowed certificate policies [{}]", allowedCertificatePolicyOIDs);
            param.setInitialPolicies(new HashSet<>(allowedCertificatePolicyOIDs));
            param.setExplicitPolicyRequired(true);
        }
        return param;
    }

    /**
     * Method returns truststore alias for the certificate
     *
     * @param cert
     * @throws CertificateException
     */
    public String getCertificateAlias(X509Certificate cert) throws CertificateException {
        try {
            return getTrustStore().getCertificateAlias(cert);
        } catch (KeyStoreException e) {
            throw new CertificateException("Error occurred while reading the truststore for " + getSubjectDN(cert), e);
        }
    }

    /**
     * Methods validates the certificate chain. For that to happen the certificate itself or parent/direct issuer
     * certificate must be present in truststore
     *
     * @param cert
     * @throws CertificateException
     */
    protected boolean verifyCertificateChain(X509Certificate cert) throws CertificateException {
        String subjectDN = getSubjectDN(cert);
        LOG.debug("Verify certificate chain for certificate: [{}]", subjectDN);
        String alias = getTrustAnchorAliasFromTrustStore(cert);
        LOG.debug("Verify certificate chain for certificate: [{}] starting with alias [{}].", subjectDN, alias);
        return isCertificateChainValid(truststore, alias);
    }

    public boolean isCertificateChainValid(KeyStore trustStore, String alias) throws CertificateException {
        X509Certificate[] certificateChain;
        try {
            certificateChain = getCertificateChain(trustStore, alias);
        } catch (KeyStoreException e) {
            throw new CertificateException("Error getting the certificate chain from the truststore for [" + alias + "]", e);
        }
        if (certificateChain == null || certificateChain.length == 0 || certificateChain[0] == null) {
            throw new CertificateException("Could not find alias in the truststore [" + alias + "]");
        }

        for (X509Certificate certificate : certificateChain) {
            boolean certificateValid = isCertificateValid(certificate);
            if (!certificateValid) {
                return false;
            }
        }
        return true;
    }

    protected X509Certificate[] getCertificateChain(KeyStore trustStore, String alias) throws KeyStoreException {
        final java.security.cert.Certificate[] certificateChain = trustStore.getCertificateChain(alias);

        if (certificateChain == null) {
            X509Certificate certificate = (X509Certificate) trustStore.getCertificate(alias);
            return new X509Certificate[]{certificate};
        }
        return Arrays.copyOf(certificateChain, certificateChain.length, X509Certificate[].class);

    }

    /**
     * Method returns alias for parent certificate
     *
     * @param cert
     * @return String - alias for parent certificate
     * @throws CertificateException
     * @throws KeyStoreException
     */
    public String getDirectParentAliasFromTruststore(X509Certificate cert) throws CertificateException, KeyStoreException {

        // search certificate issuer
        String issuerString = cert.getIssuerX500Principal().getName();
        Object subjectRDN = this.createBCX509Name(issuerString);

        Enumeration<String> aliasesEnum = getTrustStore().aliases();
        while (aliasesEnum.hasMoreElements()) {
            String alias = aliasesEnum.nextElement();
            Certificate candidate = getTrustStore().getCertificate(alias);
            if (!(candidate instanceof X509Certificate)) {
                continue;
            }

            X509Certificate issuer = (X509Certificate) candidate;
            if (verifyIssuer(cert, subjectRDN, issuer)) {
                LOG.debug("Subject certificate match found using keystore alias [{}]", alias);
                return alias;
            }
        }
        LOG.debug("Certificate with subject [{}] not found in truststore", issuerString);
        return null;
    }

    /**
     * Method verifies if  issuer has the same subject as certificate issuer and also if
     * certificate is signed by issuer.
     *
     * @param certificate
     * @param subjectRDN
     * @param issuer
     * @throws CertificateException
     */
    protected boolean verifyIssuer(X509Certificate certificate, Object subjectRDN, X509Certificate issuer) throws CertificateException {

        String certificateName = getSubjectDN(certificate);
        String issuerName = getIssuerDN(certificate);

        X500Principal foundRDN = issuer.getSubjectX500Principal();
        Object issuerBCX509Name = this.createBCX509Name(foundRDN.getName());
        if (!subjectRDN.equals(issuerBCX509Name)) {
            LOG.trace("Certificate issuer [{}] does not not match [{}].", subjectRDN, issuerBCX509Name);
            return false;
        }

        if (!isSignedBy(certificate, issuer)) {
            LOG.trace("Certificate [{}] is not signed by [{}].", certificateName, issuerName);
            return false;
        }

        LOG.debug("Certificate [{}] has matching issuer [{}].", certificateName, issuerName);
        return true;
    }

    public Pattern getSubjectRegularExpressionPattern() {
        return subjectRegularExpressionPattern;
    }

    public void setSubjectRegularExpressionPattern(Pattern subjectRegularExpressionPattern) {
        this.subjectRegularExpressionPattern = subjectRegularExpressionPattern;
    }

    public List<String> getAllowedCertificatePolicyOIDs() {
        return allowedCertificatePolicyOIDs;
    }

    public void setAllowedCertificatePolicyOIDs(List<String> allowedCertificatePolicyOids) {
        this.allowedCertificatePolicyOIDs = allowedCertificatePolicyOids;
    }

    public List<IRevocationValidator> getVerifierServices() {
        return this.revocationValidatorList;
    }

    public void clearRevocationVerifierServices() {
        if (this.revocationValidatorList != null) {
            this.revocationValidatorList.clear();
        }
    }

    public void addRevocationVerifierService(IRevocationValidator revocationVerifier) {
        if (revocationValidatorList.isEmpty()) {
            revocationValidatorList = new ArrayList<>();
        }
        this.revocationValidatorList.add(revocationVerifier);
    }

    protected String getSubjectDN(X509Certificate cert) {
        if (cert != null && cert.getSubjectDN() != null) {
            return cert.getSubjectDN().getName();
        }
        return null;
    }

    protected String getIssuerDN(X509Certificate cert) {
        if (cert != null && cert.getIssuerDN() != null) {
            return cert.getIssuerDN().getName();
        }
        return null;
    }

    /**
     * Method checks if certificate "signed" is signed by certificate 'signer'
     *
     * @param signed
     * @param signer
     * @return
     * @throws CertificateException
     */
    protected boolean isSignedBy(X509Certificate signed, X509Certificate signer) throws CertificateException {
        String certificateName = signed.getSubjectDN().getName();
        String signerName = signer.getSubjectDN().getName();
        try {
            signed.verify(signer.getPublicKey());
            return true;
        } catch (InvalidKeyException | SignatureException exc) {
            LOG.debug("Certificate [{}] is not signed by expected issuer [{}]", certificateName, signerName);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            // do not throw error and to try also other certificates
            LOG.warn("Unable to verify certificate signature [{}] for issuer certificate [{}] with reason: [{}].",
                    certificateName, signerName, ExceptionUtils.getRootCauseMessage(e));
        }
        return false;
    }
}
