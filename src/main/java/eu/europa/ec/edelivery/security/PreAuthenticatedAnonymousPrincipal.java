package eu.europa.ec.edelivery.security;

import java.security.Principal;

public class PreAuthenticatedAnonymousPrincipal implements Principal {
    @Override
    public String getName() {
        return "Anonymous";
    }

    @Override
    public String toString() {
        return "PreAuthenticatedAnonymousPrincipal";
    }
}
