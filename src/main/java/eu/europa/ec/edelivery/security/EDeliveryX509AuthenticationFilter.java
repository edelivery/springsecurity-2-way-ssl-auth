/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;
import org.springframework.security.web.authentication.preauth.x509.X509PrincipalExtractor;

import javax.security.auth.x500.X500Principal;
import jakarta.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * The purpose of this class is to extend X509AuthenticationFilter and allow also to extract X509Certufucate
 * information on the principal from the incoming request HTTP "SSLClientCert" header.
 * External authentication systems (as RP: Blue Coat, F5,..) must provide this information.
 * It is assumed that the external system is responsible for the accuracy of the data and preventing
 * the submission of forged values.
 * <p>
 * <p>
 * <p>
 * SpringSecurity's {@code {@link X509AuthenticationFilter}} adapted to eDelivery's certificates subjects representation.
 * <p>
 * jakarta.servlet.request.X509Certificate
 *
 * @author Pawel Gutowski
 */
public class EDeliveryX509AuthenticationFilter extends X509AuthenticationFilter implements AuthenticationDetailsSource<HttpServletRequest, PreAuthenticatedCertificatePrincipal> {

    // Header set by application server
    public static final String HTTP_ATTRIBUTE_JAVAX_CERTIFICATE = "jakarta.servlet.request.X509Certificate";
    // header set by Reverse Proxy
    public static final String HTTP_HEADER_RP_CERTIFICATE = "SSLClientCert";

    List<String> certHeaderKeyList;

    private boolean httpHeaderAuthenticationEnabled = false;

    public EDeliveryX509AuthenticationFilter() {
        this(HTTP_HEADER_RP_CERTIFICATE);
    }

    public EDeliveryX509AuthenticationFilter(String... headerNames) {
        certHeaderKeyList = Arrays.asList(headerNames);

        super.setAuthenticationDetailsSource(this);
        setPrincipalExtractor(new Extractor());
    }

    private static class Extractor implements X509PrincipalExtractor {
        @Override
        public Object extractPrincipal(X509Certificate cert) {
            String subject = cert.getSubjectX500Principal().getName(X500Principal.RFC2253);
            String issuer = cert.getIssuerX500Principal().getName(X500Principal.RFC2253);
            BigInteger serial = cert.getSerialNumber();
            return new PreAuthenticatedCertificatePrincipal(cert, subject, issuer, serial, cert.getNotBefore(), cert.getNotAfter());
        }
    }

    @Override
    public PreAuthenticatedCertificatePrincipal buildDetails(HttpServletRequest request) {
        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) getPreAuthenticatedPrincipal(request);
        logger.info("Extracted user details from X509 certificate: " + principal);
        return principal;
    }

    /**
     * Method searches for the
     *
     * @param request
     * @return
     */
    public X509Certificate[] getCertificateArray(HttpServletRequest request) {
        // iterate over other optional parameters with certificate
        Optional<X509Certificate> certObject = certHeaderKeyList.stream()
                .map(certHeader -> getCertificateFromHeader(certHeader, request))
                .filter(Objects::nonNull)
                .findFirst();
        return certObject.map(x509Certificate -> new X509Certificate[]{x509Certificate}).orElse(null);
    }

    public X509Certificate getCertificateFromHeader(String name, HttpServletRequest request) {
        logger.debug("Get X509 certificate from header [" + name + "]");
        CertificateFactory cf;
        String headerValue = null;
        try {
            headerValue = request.getHeader(name);
            if (StringUtils.isBlank(headerValue)) {
                logger.error("Can not parse X509 certificate from header [" + name + "]: [" + headerValue + "] with error: [Header value is empty!].");
                return null;
            }
            ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getDecoder().decode(headerValue));
            cf = CertificateFactory.getInstance("X.509");
            return (X509Certificate) cf.generateCertificate(bais);
        } catch (CertificateException e) {
            logger.error("Can not parse X509 certificate from header [" + name + "]: [" + headerValue + "] with error: [" + ExceptionUtils.getRootCauseMessage(e) + "].");
        } catch (RuntimeException ex) {
            logger.error("Can not decode base64  header [" + name + "]: [" + headerValue + "] with error: [" + ExceptionUtils.getRootCauseMessage(ex) + "].");
        }
        return null;
    }

    public boolean isHttpHeaderAuthenticationEnabled() {
        return httpHeaderAuthenticationEnabled;
    }

    /**
     * Method enable/disable option for http header certificate authentication. This should be enabled with precoision and only
     * when application is setup behind RP with secure handling of http headers
     *
     * @param httpHeaderAuthenticationEnabled true if enabled
     */
    public void setHttpHeaderAuthenticationEnabled(boolean httpHeaderAuthenticationEnabled) {
        if (httpHeaderAuthenticationEnabled) {
            logger.warn("HTTP header authentication is enabled! Use this authentication ONLY when server is set behind reverse-proxy with secure handling of HTTP headers!");
        }
        this.httpHeaderAuthenticationEnabled = httpHeaderAuthenticationEnabled;
    }

@Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        updateCertificateHeader(request);
        return super.getPreAuthenticatedPrincipal(request);
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        updateCertificateHeader(request);
        return super.getPreAuthenticatedCredentials(request);
    }

    protected void updateCertificateHeader(HttpServletRequest request) {
        // check if HTTP_HEADER_JAVAX_CERTIFICATE is null.
        logger.info("Build details from the request. Use http header [" + httpHeaderAuthenticationEnabled + "]");
        if (Objects.isNull(request.getAttribute(HTTP_ATTRIBUTE_JAVAX_CERTIFICATE))
                && httpHeaderAuthenticationEnabled) {
            logger.debug("Get authentication certificate from HTTP headers!");
            // iterate over other optional parameters with certificate
            // always set HTTP_HEADER_JAVAX_CERTIFICATE header because it is processed by X509AuthenticationFilter
            X509Certificate[] array = getCertificateArray(request);
            if (array != null) {
                logger.debug("Got authentication certificate from HTTP headers!");
                request.setAttribute(HTTP_ATTRIBUTE_JAVAX_CERTIFICATE, array);
            }
        }
    }

}
