package eu.europa.ec.edelivery.security.cert;

import javax.security.auth.x500.X500Principal;
import java.security.cert.CRLReason;

/**
 * @author Joze Rihtarsic
 * @since 1.12
 *
 * The Certificate verification constants
 */
public class CertificateConstants {

    public static final Long NULL_LONG = Long.valueOf(-1);
    public static final X500Principal NULL_ISSUER = new X500Principal("");
    public static final CRLReason NULL_CRL_REASON = CRLReason.UNSPECIFIED;

    protected CertificateConstants() {
    }

}
