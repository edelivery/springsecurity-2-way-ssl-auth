package eu.europa.ec.edelivery.security.cert;

import java.util.List;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 * The Proxy data callback. The proxy data is used for the fetching data over HttpURLFetcher
 */
public interface ProxyDataCallback {
    String getUsername();

    String getSecurityToken();

    String getHttpProxyHost();

    Integer getHttpProxyPort();

    String getHttpNoProxyHosts();

    boolean isProxyEnabled();

    List<String> getAllowedURLProtocols();
}
