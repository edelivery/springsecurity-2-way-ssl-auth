package eu.europa.ec.edelivery.security;

import java.security.Principal;
import java.util.Objects;

public class PreAuthenticatedTokenPrincipal implements Principal {

    private String token;
    private String header;

    public PreAuthenticatedTokenPrincipal(String header, String token) {
        this.token = token;
        this.header = header;
    }

    @Override
    public String getName() {
        return token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PreAuthenticatedTokenPrincipal)) return false;
        PreAuthenticatedTokenPrincipal that = (PreAuthenticatedTokenPrincipal) o;
        return token.equals(that.token) &&
                header.equals(that.header);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, header);
    }

    @Override
    public String toString() {
        return "PreAuthenticatedTokenPrincipal{" +
                "header='" + header + '\'' + '}';
    }
}
