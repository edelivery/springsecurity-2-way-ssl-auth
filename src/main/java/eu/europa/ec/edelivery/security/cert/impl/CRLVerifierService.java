package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.exception.CertificateValidationTechnicalException;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.cert.*;
import java.util.*;

import static eu.europa.ec.edelivery.exception.CertificateValidationTechnicalException.ErrorCode.URLConnectionError;
import static eu.europa.ec.edelivery.security.cert.CertificateConstants.*;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 * The CRL Implementation of the IRevocationValidator
 */
public class CRLVerifierService extends AbstractRevocationVerifierService {


    private static final Logger LOG = LoggerFactory.getLogger(CRLVerifierService.class);

    Map<String, X509CRL> crlCacheMap = new HashMap<>();
    Map<String, Long> crlCacheNextRefreshMap = new HashMap<>();
    public static final long ON_FAIL_REFRESH_CRL_INTERVAL_IN_MILISECONDS = 1000L * 60 * 60;

    public CRLVerifierService(URLFetcher httpConnection) {
        super(httpConnection);
    }

    @Override
    List<String> getAllValidationEndpoints(X509Certificate cert) throws CertificateParsingException {
        return X509CertificateUtils.getCrlDistributionPoints(cert);
    }

    public boolean isSerialNumberRevoked(String serial, X509Certificate issuer, String crlDistributionPointURL) throws CertificateException {
        // remove
        String cleanSerial = serial.trim().replaceAll("\\s", "");
        BigInteger biSerial = new BigInteger(cleanSerial, 16);
        return isSerialNumberRevoked(biSerial, issuer, crlDistributionPointURL);
    }

    public boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, String crlDistributionPointURL) throws CertificateException {
        LOG.info("Download CRL [{}].", crlDistributionPointURL);
        return isSerialNumberRevoked(serial, issuer, Collections.singletonList(crlDistributionPointURL));
    }

    @Override
    public boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, List<String> crlDistPoints) throws CertificateException {

        if (crlDistPoints.isEmpty()) {
            LOG.warn("Can not validate certificate with serial number: [{}]. Empty CRL Lists.", serial);
            return false;
        }

        X509CRL crl = null;
        for (String url : crlDistPoints) {
            crl = getCRLByURL(url, true);
            if (crl != null) {
                break;
            }
        }

        if (crl != null) {
            return validateSerialNumberOnCRL(crl, serial);
        } else {
            String message = "Could not get CRL from non of the CLR distribution points: ["
                    + String.join(",", crlDistPoints) + "]!";
            if (isGracefulValidation()) {
                LOG.warn(message);
                return false;
            }
            throw new CertificateValidationTechnicalException(URLConnectionError, message);
        }
    }

    /**
     * Method fetch and parse the CRL.
     *
     * @param crlURL    - CRL address
     * @param quietFail - in case of fetch or parse error - throw exception if parameter is false. Else return null;
     * @return X509CRL or null if parameter quietFail is set to true
     * @throws CertificateException - in case of fetch or parse error - throw exception if parameter quietFail is false. Else return null;
     */
    public X509CRL getCRLByURL(String crlURL, boolean quietFail) throws CertificateException {
        X509CRL x509CRL = null;
        if (StringUtils.isBlank(crlURL)) {
            return x509CRL;
        }
        Date currentDate = Calendar.getInstance().getTime();
        String url = crlURL.trim();
        x509CRL = getCachedCRLByURL(url, currentDate);
        if (x509CRL == null && !isClrCached(url)) {
            // if CRL is null try to get one
            x509CRL = downloadCRL(url, quietFail);
            if (x509CRL == null) {
                LOG.debug("Got  NULL CRL for url [{}]!", crlURL);
            }
            // calculate next update in milliseconds...
            // cache also failed validation for the next hour.
            Long nextRefresh = x509CRL != null && x509CRL.getNextUpdate() != null ? x509CRL.getNextUpdate().getTime()
                    : currentDate.getTime() + ON_FAIL_REFRESH_CRL_INTERVAL_IN_MILISECONDS;

            // set /replace data
            LOG.debug("Set CRL from url [{}] to cache!", crlURL);
            crlCacheMap.put(crlURL, x509CRL);
            crlCacheNextRefreshMap.put(crlURL, nextRefresh);
        } else {
            LOG.debug("Got CRL for url [{}] from cache!", crlURL);
        }
        return x509CRL;
    }

    /**
     * Try to fetch CRL from given URL. In case of fetch or parse
     * error, method throw CertificateException if parameter quietFail is set to true.
     * Else it logs error and return null. This functionality is made for gracefully validation or when
     * validation is done using multiple URLs.
     *
     * @param crlURL    - Location of the revocation list
     * @param quietFail - true throws an error in case of fetch or parse error, else it return null
     * @return X509CRL or null if failOnError is set to false
     * @throws CertificateException: See the attribute quietFail
     */
    public X509CRL downloadCRL(String crlURL, boolean quietFail) throws CertificateException {
        LOG.debug("Fetch CRL from url [{}]", crlURL);
        X509CRL crl = null;
        CertificateException exception = null;
        try (InputStream crlStream = getHttpConnection().fetchURL(crlURL)) {
            if (crlStream != null) {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                crl = (X509CRL) cf.generateCRL(crlStream);
            }
        } catch (IOException e) {
            exception = new CertificateException("Can not download CRL [" + crlURL + "].", e);
        } catch (CertificateException e) {
            exception = new CertificateException("CRL validation error for: [" + crlURL + "]. X.509 CertificateFactory is  not supported! Check JCE configuration!", e);
        } catch (CRLException e) {
            exception = new CertificateException("Parse CRL exception : [" + crlURL + " ].", e);
        }
        // if exception occurred
        if (exception != null) {
            if (!quietFail) {
                throw exception;
            } else {
                LOG.warn("{} Root cause: [{}].", exception.getMessage(), ExceptionUtils.getRootCauseMessage(exception));
            }
        }
        return crl;
    }

    /**
     * Validate if the URL is cached
     *
     * @param url - url
     * @return return true of URL is cached else return false.
     */
    protected boolean isClrCached(String url) {
        Long nextRefresh = crlCacheNextRefreshMap.getOrDefault(url, NULL_LONG);
        return nextRefresh > Calendar.getInstance().getTimeInMillis();
    }

    protected X509CRL getCachedCRLByURL(String crlURL, Date currentDate) {
        X509CRL x509CRL = null;
        if (StringUtils.isBlank(crlURL)) {
            LOG.debug("Blank CRL! Return null");
            return x509CRL;
        }
        String url = crlURL.trim();
        if (crlCacheMap.containsKey(url)) {
            X509CRL crlTmp = crlCacheMap.get(url);
            Long nextRefresh = crlCacheNextRefreshMap.getOrDefault(url, NULL_LONG);
            if (nextRefresh > currentDate.getTime()) {
                x509CRL = crlTmp;
            } else {
                crlCacheNextRefreshMap.remove(url);
                crlCacheMap.remove(url);
            }
        }
        return x509CRL;
    }

    /**
     * This is less secure serial number verification. Method only validates if serial number is on the list, but it
     * does not verify the list signature. Use this method only if you do not have X509Certificate!.
     *
     * @param x509CRL - CRL list
     * @param bi      - Serial number
     * @throws CertificateRevokedException - if serial number is on the list
     */
    protected boolean validateSerialNumberOnCRL(X509CRL x509CRL, BigInteger bi) throws CertificateRevokedException {
        X509CRLEntry entry = x509CRL.getRevokedCertificate(bi);
        if (entry != null) {
            throw buildRevocationError(entry);
        }
        return true;
    }

    protected CertificateRevokedException buildRevocationError(X509CRLEntry entry) {
        Map<String, Extension> map = new HashMap<>();
        return new CertificateRevokedException(entry.getRevocationDate(),
                entry.getRevocationReason() == null ? NULL_CRL_REASON : entry.getRevocationReason(),
                entry.getCertificateIssuer() == null ? NULL_ISSUER : entry.getCertificateIssuer(), map);
    }

}
