package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.exception.CertificateValidationTechnicalException;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.cert.CertException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.x500.X500Principal;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.CertificateParsingException;
import java.security.cert.CertificateRevokedException;
import java.security.cert.X509Certificate;
import java.util.*;

import static eu.europa.ec.edelivery.security.cert.CertificateConstants.NULL_CRL_REASON;
import static eu.europa.ec.edelivery.security.cert.CertificateConstants.NULL_ISSUER;

/**
 * @author Joze Rihtarsic
 * @since 1.12
 * <p>
 * The OCSP Implementation of the IRevocationValidator
 */
public class OCSPVerifierService extends AbstractRevocationVerifierService {

    private static final Logger LOG = LoggerFactory.getLogger(OCSPVerifierService.class);

    public OCSPVerifierService(URLFetcher httpConnection) {
        super(httpConnection);
    }

    @Override
    List<String> getAllValidationEndpoints(X509Certificate cert) throws CertificateParsingException {
        return X509CertificateUtils.getOCSPLocations(cert);
    }

    @Override
    public boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, List<String> verificationURLs) throws CertificateException {

        if (issuer == null) {
            LOG.debug("Missing issuer certificate for serial number [{}]. Skip the OCSP validation", serial);
            throw new CertificateValidationTechnicalException(CertificateValidationTechnicalException.ErrorCode.BadRequest, "Missing issuer certificate for serial number [" + serial + "].");
        }

        String issuerSubjectDN = issuer.getSubjectDN().getName();
        if (verificationURLs.isEmpty()) {
            LOG.debug("Empty OCSP url list for revocation validation of the certificate with serial [{}] and issuer [{}]. Skip the validation", serial, issuerSubjectDN);
            return false;
        }

        // generate the OCSP request
        OCSPReq request = generateOCSPRequest(issuer, serial);
        // list over the verification urls..
        for (String serviceUrl : verificationURLs) {
            List<SingleResp> responses = validateOCSP(serviceUrl, request, issuer);
            if (responses != null && !responses.isEmpty()) {
                SingleResp resp = responses.get(0);
                if (resp.getCertStatus() == CertificateStatus.GOOD) {
                    LOG.debug("Certificate with serial [{}]  and issuer [{}] is not revoked!", serial, issuerSubjectDN);
                    return true;
                } else if (resp.getCertStatus() instanceof RevokedStatus) {
                    LOG.debug("Certificate with serial [{}]  and issuer [{}] is REVOKED on [{}]!", serial, issuerSubjectDN, ((RevokedStatus) resp.getCertStatus()).getRevocationTime());
                    throw buildRevocationError((RevokedStatus) resp.getCertStatus(), issuer.getSubjectX500Principal());
                } else if (resp.getCertStatus() instanceof UnknownStatus) {
                    LOG.warn("Got UNKNOWN status when validation OCSP with serial [{}] and issuer [{}]!", serial, issuerSubjectDN);

                }
            }

        }
        // all conditions are met for the OCSP validation. But can not get the
        if (!isGracefulValidation()) {
            throw new CertificateException("Can't get Revocation Status from OCSP.");
        }
        LOG.warn("Could not validate OCSP validation. Graceful skip of the OCSP validation for the certificate with serial [{}] and issuer [{}]!", serial, issuerSubjectDN);
        return false;
    }

    /**
     * Validate OCSP request using the  given URL
     *
     * @param serviceUrl - OCSP endpoint
     * @param request    - OCSP request
     * @param issuer     - verify the signature
     * @return - list of OCSP responses
     */
    public List<SingleResp> validateOCSP(String serviceUrl, OCSPReq request, X509Certificate issuer) {
        List<SingleResp> responses = Collections.emptyList();
        // retrieve ocsp response
        OCSPResp ocspResponse;
        try {
            ocspResponse = getOCSPResponse(serviceUrl, request);
        } catch (CertificateException e) {
            LOG.warn("Failed to retrieve OCSP response on URL [{}] Error: [{}]!", serviceUrl, ExceptionUtils.getRootCauseMessage(e));
            return responses;
        }

        if (OCSPResp.SUCCESSFUL != ocspResponse.getStatus()) {
            // did not get response from server try with next URL
            LOG.warn("OCSP verification on URL [{}] did not succeed: [{}]!", serviceUrl, ocspResponse.getStatus());
            return responses;
        }


        // parse and validate ocsp response
        try {

            BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
            // get OCSP signing certificate
            X509CertificateHolder signingCertHolder = basicResponse != null
                    && basicResponse.getCerts() != null
                    && basicResponse.getCerts().length > 0 ? basicResponse.getCerts()[0] : null;

            if (signingCertHolder == null) {
                LOG.warn("OCSP verification on URL [{}] failed with root cause error: [Can not verify signature - signing certificate is missing]!", serviceUrl);
                return responses;
            }
            JcaContentVerifierProviderBuilder jcacvpb = new JcaContentVerifierProviderBuilder();
            ContentVerifierProvider cvp = jcacvpb.build(issuer);
            if (!signingCertHolder.isSignatureValid(cvp)) {
                LOG.warn("OCSP verification on URL [{}] failed with root cause error: [OCSP Signing certificate is not trusted ]!", serviceUrl);
                return responses;
            }

            // validate cvpOcspResponse
            ContentVerifierProvider cvpOcspResponse = jcacvpb.build(signingCertHolder);
            if (basicResponse.isSignatureValid(cvpOcspResponse)) {
                responses = Arrays.asList(basicResponse.getResponses());
            } else {
                LOG.warn("OCSP verification on URL [{}] failed with root cause error: [Invalid signature]!", serviceUrl);
            }
        } catch (CertException | OCSPException | OperatorCreationException | CertificateException e) {
            LOG.warn("OCSP signature verification failed on URL [{}]  with root cause error: [{}]!", serviceUrl, ExceptionUtils.getRootCauseMessage(e));

        }
        return responses;
    }

    /**
     * Gets an ASN.1 encoded OCSP response (as defined in RFC 2560) from the given service URL. Currently supports
     * only HTTP.
     *
     * @param serviceUrl URL of the OCSP endpoint.
     * @param request    an OCSP request object.
     * @return OCSP response encoded in ASN.1 structure.
     * @throws CertificateException if the response cannot be retrieved.
     */
    protected OCSPResp getOCSPResponse(String serviceUrl,
                                       OCSPReq request) throws CertificateException {
        LOG.debug("Getting OCSP response from url: [{}]", serviceUrl);
        try {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/ocsp-request");
            headers.put("Accept", "application/ocsp-response");
            InputStream result = getHttpConnection().fetchURL(serviceUrl, headers, new ByteArrayInputStream(request.getEncoded()));
            return new OCSPResp(result);

        } catch (IOException e) {
            throw new CertificateException("Cannot get ocspResponse from url: " + serviceUrl, e);
        }
    }

    /**
     * This method generates an OCSP Request to be sent to an OCSP endpoint.
     *
     * @param issuerCert   is the Certificate of the Issuer of the peer certificate we are interested in.
     * @param serialNumber of the peer certificate.
     * @return generated OCSP request.
     * @throws CertificateException if the request cannot be generated.
     */
    private OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
            throws CertificateException {
        try {
            if (issuerCert == null) {
                throw new CertificateException("Cannot generate OCSP Request with null issuer certificate");
            }
            LOG.info("Generating OCSP request for certificate with serial number [{}] and issuer [{}]", serialNumber, issuerCert.getSubjectDN().getName());
            //  CertID structure is used to uniquely identify certificates that are the subject of
            // an OCSP request or response and has an ASN.1 definition. CertID structure is defined
            // in RFC 2560
            DigestCalculator digestCalculator = new JcaDigestCalculatorProviderBuilder().build().get(CertificateID.HASH_SHA1);
            CertificateID id = new CertificateID(digestCalculator, new JcaX509CertificateHolder(issuerCert), serialNumber);

            // basic request generation with nonce
            OCSPReqBuilder builder = new OCSPReqBuilder();
            builder.addRequest(id);

            // create details for nonce extension. The nonce extension is used to bind
            // a request to a response to prevent replay attacks. As the name implies,
            // the nonce value is something that the client should only use once within a reasonably
            // small period.
            BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
            Extension[] extensions = new Extension[]{
                    new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, false,
                            new DEROctetString(nonce.toByteArray()))};
            builder.setRequestExtensions(new Extensions(extensions));

            return builder.build();
        } catch (OCSPException | OperatorCreationException e) {
            throw new CertificateException("Cannot generate OCSP Request with the " +
                    "given certificate", e);
        }
    }

    protected CertificateRevokedException buildRevocationError(RevokedStatus status, X500Principal issuer) {
        Map<String, java.security.cert.Extension> map = new HashMap<>();
        return new CertificateRevokedException(status.getRevocationTime(),
                NULL_CRL_REASON,
                issuer == null ? NULL_ISSUER : issuer,
                map);
    }

}
