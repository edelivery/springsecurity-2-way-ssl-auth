package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.security.cert.IRevocationValidator;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

/**
 * @author Joze Rihtarsic
 * @since 1.12
 * <p>
 * The OCSP Implementation of the IRevocationValidator
 */
abstract class AbstractRevocationVerifierService implements IRevocationValidator {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractRevocationVerifierService.class);

    private boolean gracefulValidation = true;
    private final List<String> allowedURLSchemas = new ArrayList<>(asList("http://", "https://"));


    private URLFetcher httpConnection;

    public AbstractRevocationVerifierService(URLFetcher httpConnection) {
        this.httpConnection = httpConnection;
    }

    @Override
    public boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer) throws CertificateException {
        List<String> crlDistPoints = getValidationEndpoints(cert);
        if (crlDistPoints.isEmpty()) {
            return false;
        }
        return isCertificateRevoked(cert, issuer, crlDistPoints);
    }


    @Override
    public boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer, List<String> verificationURL) throws CertificateException {
        return isSerialNumberRevoked(cert.getSerialNumber(), issuer, verificationURL);
    }

    @Override
    public boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, String verificationURL) throws CertificateException {
        LOG.info("Download OCSP response [{}].", verificationURL);
        return isSerialNumberRevoked(serial, issuer, Collections.singletonList(verificationURL));
    }


    /**
     * Abstract method to retrieve all validation endpoints from certificate extension.
     *
     * @param cert - certificate to extract validation endpoints
     * @return list of validation endpoints
     */
    abstract List<String> getAllValidationEndpoints(X509Certificate cert) throws CertificateException;

    /**
     * return all validation endpoints from certificate extension which uses allowed URL schemas.
     *
     * @param cert - certificate to extract validation endpoints
     * @return list of validation endpoints
     */
    public List<String> getValidationEndpoints(X509Certificate cert) throws CertificateException {
        List<String> validationEndpoints = getAllValidationEndpoints(cert);
        if (allowedURLSchemas.isEmpty()) {
            LOG.debug("No allowed URL schemas defined. Use all schemas.");
            return validationEndpoints;
        }
        List<String> lowerCaseSchemas = allowedURLSchemas.stream().map(String::toLowerCase).collect(Collectors.toList());

        return validationEndpoints.stream()
                .map(String::toLowerCase)
                .filter(url -> lowerCaseSchemas.stream().anyMatch(url::startsWith))
                .collect(Collectors.toList());
    }


    public boolean isGracefulValidation() {
        return gracefulValidation;
    }

    public void setGracefulValidation(boolean gracefulValidation) {
        this.gracefulValidation = gracefulValidation;
    }

    public void setAllowedURLSchemas(List<String> allowedURLSchemas) {
        this.allowedURLSchemas.clear();
        this.allowedURLSchemas.addAll(allowedURLSchemas);
    }

    public List<String> getAllowedURLSchemas() {
        return allowedURLSchemas;
    }

    @Override
    public URLFetcher getHttpConnection() {
        return httpConnection;
    }

    /**
     * Set URL fetcher implementation.
     *
     * @param connection - URL fetcher implementation
     */
    @Override
    public void setHttpConnection(URLFetcher connection) {
        this.httpConnection = connection;
    }


}
