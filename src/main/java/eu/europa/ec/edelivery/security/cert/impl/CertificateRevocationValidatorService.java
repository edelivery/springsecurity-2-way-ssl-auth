package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.exception.CertificateValidationTechnicalException;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Service validates Certificate revocation status against CRLs and/or OCSP.
 * Default strategy is OCSP_CRL and gracefulValidation is false.
 * <p>
 *  @author Joze RIHTARSIC
 *  @since 1.14
 */
public class CertificateRevocationValidatorService extends AbstractRevocationVerifierService {
    private static final Logger LOG = LoggerFactory.getLogger(CertificateRevocationValidatorService.class);

    protected CertificateRVStrategyEnum validationStrategy = CertificateRVStrategyEnum.OCSP_CRL;

    private final OCSPVerifierService ocsplVerifierService;
    private final CRLVerifierService crlVerifierService;


    public CertificateRevocationValidatorService(URLFetcher httpConnection) {
        super(httpConnection);
        ocsplVerifierService = new OCSPVerifierService(httpConnection);
        crlVerifierService = new CRLVerifierService(httpConnection);
    }

    public  void setValidationParameters(CertificateRVStrategyEnum strategyEnum, boolean gracefulValidation, List<String> allowedUrlSchemas){
        setValidationStrategy(strategyEnum);
        setGracefulValidation(gracefulValidation);
        this.ocsplVerifierService.setGracefulValidation(gracefulValidation);
        this.crlVerifierService.setGracefulValidation(gracefulValidation);
        setAllowedURLSchemas(allowedUrlSchemas);
        this.ocsplVerifierService.setAllowedURLSchemas(allowedUrlSchemas);
        this.crlVerifierService.setAllowedURLSchemas(allowedUrlSchemas);

    }

    public CertificateRVStrategyEnum getValidationStrategy() {
        return validationStrategy;
    }

    public void setValidationStrategy(CertificateRVStrategyEnum validationStrategy) {
        this.validationStrategy = validationStrategy;
    }

    @Override
    public void setHttpConnection(URLFetcher connection) {
        super.setHttpConnection(connection);
        ocsplVerifierService.setHttpConnection(connection);
        crlVerifierService.setHttpConnection(connection);
    }

    @Override
    public boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer) throws CertificateException {
        boolean validated = false;
        switch (validationStrategy) {
            case OCSP_CRL:
                try {
                    ocsplVerifierService.setGracefulValidation(false);
                    validated = ocsplVerifierService.isCertificateRevoked(cert, issuer);
                } catch (CertificateValidationTechnicalException e) {
                    validated = false;
                    LOG.warn("OCSP validation failed! Try CRL validation!", e);
                }
                if (!validated) {
                    crlVerifierService.setGracefulValidation(isGracefulValidation());
                    validated = crlVerifierService.isCertificateRevoked(cert, issuer);
                }

                break;
            case CRL_OCSP:
                try {
                    crlVerifierService.setGracefulValidation(false);
                    validated = crlVerifierService.isCertificateRevoked(cert, issuer);
                } catch (CertificateValidationTechnicalException e) {
                    LOG.warn("CRL validation failed! Try OCSP validation!", e);
                    validated = false;
                }
                if (!validated) {
                    ocsplVerifierService.setGracefulValidation(isGracefulValidation());
                    validated = ocsplVerifierService.isCertificateRevoked(cert, issuer);
                }
                break;
            case OCSP_ONLY:
                ocsplVerifierService.setGracefulValidation(isGracefulValidation());
                validated = ocsplVerifierService.isCertificateRevoked(cert, issuer);
                break;
            case CRL_ONLY:
                crlVerifierService.setGracefulValidation(isGracefulValidation());
                validated =  crlVerifierService.isCertificateRevoked(cert, issuer);
                break;
            case NO_VALIDATION:
                LOG.debug("Certificate revocation validation is disabled!");
                break;

        }
        return validated;
    }

    @Override
    public boolean isCertificateRevoked(X509Certificate cert, X509Certificate issuer, List<String> verificationURL) throws CertificateException {
        if (verificationURL == null || verificationURL.isEmpty()) {
            // if no verification URL is given, then use verification URL extracted from certificate
            isCertificateRevoked(cert, issuer);
        }
        // user default validation strategy which call isSerialNumberRevoked
        return super.isCertificateRevoked(cert, issuer, verificationURL);
    }

    @Override
    public boolean  isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, List<String> verificationURLs) throws CertificateException {
        return isSerialNumberRevoked(serial, issuer, verificationURLs, verificationURLs);
    }
    public boolean isSerialNumberRevoked(BigInteger serial, X509Certificate issuer, List<String> verificationCrlURLs, List<String> verificationOcspURLs) throws CertificateException {

        if ((verificationCrlURLs == null || verificationCrlURLs.isEmpty()) &&
                (verificationOcspURLs == null || verificationOcspURLs.isEmpty())) {
            LOG.warn("No Certificate revocation validation URLs given to validate certificate revocation status! Skip validation!");
            return false;
        }
        boolean validated = false;
        switch (validationStrategy) {
            case OCSP_CRL:
                try {
                    ocsplVerifierService.setGracefulValidation(false);
                    validated = ocsplVerifierService.isSerialNumberRevoked(serial, issuer, verificationOcspURLs);
                } catch (CertificateValidationTechnicalException e) {
                    LOG.warn("OCSP validation failed! Try CRL validation!", e);
                    validated = false;
                }
                if (!validated){
                    crlVerifierService.setGracefulValidation(isGracefulValidation());
                    validated = crlVerifierService.isSerialNumberRevoked(serial, issuer, verificationCrlURLs);
                }
                break;
            case CRL_OCSP:
                try {
                    crlVerifierService.setGracefulValidation(false);
                    validated = crlVerifierService.isSerialNumberRevoked(serial, issuer, verificationCrlURLs);
                } catch (CertificateValidationTechnicalException e) {
                    LOG.warn("CRL validation failed! Try OCSP validation!", e);
                    validated = false;
                }
                if (!validated) {
                    ocsplVerifierService.setGracefulValidation(isGracefulValidation());
                    validated =  ocsplVerifierService.isSerialNumberRevoked(serial, issuer, verificationOcspURLs);
                }
                break;
            case OCSP_ONLY:
                ocsplVerifierService.setGracefulValidation(isGracefulValidation());
                validated = ocsplVerifierService.isSerialNumberRevoked(serial, issuer, verificationOcspURLs);
                break;
            case CRL_ONLY:
                crlVerifierService.setGracefulValidation(isGracefulValidation());
                validated =  crlVerifierService.isSerialNumberRevoked(serial, issuer, verificationCrlURLs);
                break;
            case NO_VALIDATION:
                LOG.debug("Certificate revocation validation is disabled!");
                break;
        }
        return validated;
    }

    @Override
    List<String> getAllValidationEndpoints(X509Certificate cert) throws CertificateException {
        switch (validationStrategy) {
            case OCSP_ONLY:
              return ocsplVerifierService.getAllValidationEndpoints(cert);
            case CRL_ONLY:
              return crlVerifierService.getAllValidationEndpoints(cert);
            default:
                throw new CertificateValidationTechnicalException(CertificateValidationTechnicalException.ErrorCode.ExtensionNotFound,
                        "Unsupported method for validation strategy: " + validationStrategy);

        }
    }
}
