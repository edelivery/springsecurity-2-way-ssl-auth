package eu.europa.ec.edelivery.security.utils;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * X509CertificateBuilder  - Builder class for creating X509Certificate for given input data
 * The Utils is mainly used for startup demo and testing purposes.
 *
 */
public class X509CertificateBuilder {

    private static final org.slf4j.Logger LOG =
            org.slf4j.LoggerFactory.getLogger(X509CertificateBuilder.class);

    BigInteger serialNumber = BigInteger.ONE;
    PublicKey subjectPublicKey;
    String subjectDn;
    OffsetDateTime notBefore;
    OffsetDateTime notAfter;
    String issuerDn;
    PrivateKey certSignerKey;
    boolean caFlag;
    int pathLength;
    KeyUsage keyUsage;
    List<String> crlURls;
    List<String> ocspUrls;
    List<String> certificatePolicyOids;


    // builder pattern for building X509Certificate
    private X509CertificateBuilder() {
    }

    public static X509CertificateBuilder create() {
        return new X509CertificateBuilder();
    }

    public X509CertificateBuilder serialNumber(BigInteger serialNumber) {
        this.serialNumber = serialNumber;
        return this;
    }

    public X509CertificateBuilder subjectPublicKey(PublicKey subjectPublicKey) {
        this.subjectPublicKey = subjectPublicKey;
        return this;
    }

    public X509CertificateBuilder subjectDn(String subjectDn) {
        this.subjectDn = subjectDn;
        return this;
    }

    public X509CertificateBuilder notBefore(OffsetDateTime notBefore) {
        this.notBefore = notBefore;
        return this;
    }

    public X509CertificateBuilder notAfter(OffsetDateTime notAfter) {
        this.notAfter = notAfter;
        return this;
    }

    public X509CertificateBuilder issuerDn(String issuerDn) {
        this.issuerDn = issuerDn;
        return this;
    }

    public X509CertificateBuilder certSignerKey(PrivateKey certSignerKey) {
        this.certSignerKey = certSignerKey;
        return this;
    }

    public X509CertificateBuilder caFlag(boolean caFlag) {
        this.caFlag = caFlag;
        return this;
    }

    public X509CertificateBuilder pathLength(int pathLength) {
        this.pathLength = pathLength;
        return this;
    }

    public X509CertificateBuilder keyUsage(KeyUsage keyUsage) {
        this.keyUsage = keyUsage;
        return this;
    }

    public X509CertificateBuilder crlURls(List<String> crlURls) {
        this.crlURls = crlURls;
        return this;
    }

    public X509CertificateBuilder ocspUrls(List<String> ocspUrls) {
        this.ocspUrls = ocspUrls;
        return this;
    }

    public X509CertificateBuilder certificatePolicyOids(List<String> certificatePolicyOids) {
        this.certificatePolicyOids = certificatePolicyOids;
        return this;
    }

    public X509Certificate build() {
        validateData();
        try {
            return generateCertificate(serialNumber, subjectPublicKey, subjectDn, notBefore, notAfter, issuerDn, certSignerKey, caFlag, pathLength, keyUsage, crlURls, ocspUrls, certificatePolicyOids);
        } catch (IOException | OperatorCreationException | CertificateException e) {
            throw new IllegalArgumentException(e);
        }
    }

    protected void validateData(){

        if (subjectDn == null) {
            throw new IllegalArgumentException("subject Dn is  mandatory");
        }
        if (subjectPublicKey == null) {
            throw new IllegalArgumentException("subject public key is  mandatory");
        }
        if (certSignerKey == null){
            throw new IllegalArgumentException("certificate signer key is  mandatory");
        }
    }



    public static X509Certificate generateCertificate(BigInteger serialNumber, PublicKey subjectPublicKey, String subjectDn,
                                                      OffsetDateTime notBefore, OffsetDateTime notAfter,
                                                      String issuerDn,
                                                      PrivateKey certSignerKey,
                                                      boolean caFlag, int pathLength, KeyUsage keyUsage,
                                                      List<String> crlURls,
                                                      List<String> ocspUrls,
                                                      List<String> certificatePolicies)
            throws IOException, IllegalStateException, CertificateException, OperatorCreationException {


        X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
                new X500Name(StringUtils.isBlank(issuerDn) ? subjectDn : issuerDn),
                serialNumber == null ? BigInteger.TEN : serialNumber,
                Date.from(notBefore!=null?notBefore.toInstant():OffsetDateTime.now().minusDays(1l).toInstant()),
                Date.from(notAfter!=null?notAfter.toInstant():OffsetDateTime.now().plusYears(1l).toInstant()),
                new X500Name(subjectDn),
                SubjectPublicKeyInfo.getInstance(subjectPublicKey.getEncoded()));

        // set basic basicConstraint  for all except the last leaf certificate
        if (caFlag) {
            if (-1 == pathLength) {
                certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(true));
            } else {
                certBuilder.addExtension(Extension.basicConstraints, true, new BasicConstraints(pathLength));
            }
        }

        if (null != keyUsage) {
            certBuilder.addExtension(Extension.keyUsage, true, keyUsage);
        }

        // add certificate policies
        if (certificatePolicies != null) {
            addExtensionCertificatePolicies(certBuilder, certificatePolicies);
        }
        // add crl urls to leaf certificate
        if (crlURls != null && !crlURls.isEmpty()) {
            addExtensionCRLUrls(certBuilder, crlURls);
        }
        // add OCSP urls to leaf certificate
        if (ocspUrls != null && !ocspUrls.isEmpty()) {
            addExtensionOCSPUrls(certBuilder, ocspUrls);
        }

        String signAlgName = getJCESigningAlgorithmForPrivateKey(certSignerKey);
        ContentSigner sigGen = new JcaContentSignerBuilder(signAlgName)
                .build(certSignerKey);

        return new JcaX509CertificateConverter().getCertificate(certBuilder.build(sigGen));

    }

    public static void addExtensionCertificatePolicies(X509v3CertificateBuilder certBuilder, List<String> certificatePolicies) throws CertIOException {
        X509CertificateUtils.addExtensionCertificatePolicies(certBuilder, certificatePolicies);
    }

    public static void addExtensionCRLUrls(X509v3CertificateBuilder certBuilder, List<String> distributionList) throws CertIOException {
        X509CertificateUtils.addExtensionCRLUrls(certBuilder, distributionList);
    }

    public static void addExtensionOCSPUrls(X509v3CertificateBuilder certBuilder, List<String> ocspUrls) throws CertIOException {
        X509CertificateUtils.addExtensionOCSPUrls(certBuilder, ocspUrls);
    }

    /**
     * Get 'default' certificate signing algorithm for issuer key.  In case the
     * algorithm is  eddsa ed25519 signing value is assumed to be Ed25519.
     *
     * @param key - Issuer Key to sign certificate
     * @return signing algorithm
     */
    public static String getJCESigningAlgorithmForPrivateKey(PrivateKey key) {
        if (key == null || StringUtils.isBlank(key.getAlgorithm())){
            LOG.warn("Can define signing algorithm for null key/key.algorithm");
            return null;
        }

        int l = key.getEncoded().length;
        System.out.println("Key algorithm: " +key.getAlgorithm()+  " size: "  + l);
        switch (StringUtils.lowerCase(key.getAlgorithm())) {
            case "ed448":
                return "ed448";
            case "ed25519":
                return "Ed25519";
            case "ec":
                return "SHA256withECDSA";
            case "rsa":
                return "SHA256WITHRSA";
            case "eddsa":
                return key.getEncoded().length < 50 ? "Ed25519" : "Ed448";
            default:
                LOG.debug("Key algorithm [{}], is not supported.", key.getAlgorithm());
                return null;
        }
    }
}
