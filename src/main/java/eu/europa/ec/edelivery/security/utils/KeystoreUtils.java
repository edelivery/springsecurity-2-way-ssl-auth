package eu.europa.ec.edelivery.security.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Collections.list;

/**
 * The purpose of the class are the keystore tools implemented as a static/ stateless methods.
 *
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 */
public class KeystoreUtils {
    private static final Logger LOG = LoggerFactory.getLogger(KeystoreUtils.class);
    private static final String KEYSTORE_TYPE_PKCS12 = "PKCS12";
    private static final String KEYSTORE_TYPE_JKS = "JKS";
    private static final String ALIAS_TEMPLATE = "%s_%03d";

    private static final List<String> SUPPORTED_KEYSTORE_LIST = Arrays.asList(KEYSTORE_TYPE_JKS, KEYSTORE_TYPE_PKCS12);

    // TODO change this to private when new SMP 5.0 will be published do dev branch
    public KeystoreUtils() {
    }

    public static KeyStore loadKeystore(File keystoreFile, String token, String keystoreType) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        KeyStore keyStore = KeyStore.getInstance(keystoreType);
        try (InputStream fis = Files.newInputStream(keystoreFile.toPath())) {
            LOG.debug("Open keystore [{}], with keystore type [{}]!", keystoreFile.getAbsolutePath(), keystoreType);
            keyStore.load(fis, token.toCharArray());
        }
        return keyStore;
    }

    public static KeyStore loadKeystore(File keystoreFile, String token) {
        for (String keystoreType : SUPPORTED_KEYSTORE_LIST) {
            try {
                return loadKeystore(keystoreFile, token, keystoreType);
            } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
                LOG.debug("Can not open keystore: [{}]  with type: [{}]! Error: [{}]", keystoreFile.getAbsolutePath(), keystoreType, ExceptionUtils.getRootCauseMessage(e));
            }
        }
        return null;
    }

    public static void validatePrivateKeyInKeystore(KeyStore keystore, String keystorePassword, String alias) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        if (!keystore.containsAlias(alias)) {
            throw new KeyStoreException("Alias  [" + alias + "] not exists in keystore!");
        }
        keystore.getKey(alias, keystorePassword.toCharArray());
    }

    public static KeyStore createNewKeystore(File keystoreFile, String keystoreToken) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        return createNewKeystore(keystoreFile, keystoreToken, KEYSTORE_TYPE_PKCS12);
    }

    public static KeyStore createNewKeystore(File keystoreFile, String keystoreToken, String keystoreType) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        KeyStore newKeystore;
        try (FileOutputStream out = new FileOutputStream(keystoreFile)) {
            newKeystore = KeyStore.getInstance(keystoreType);
            // initialize keystore
            newKeystore.load(null, keystoreToken.toCharArray());
            newKeystore.store(out, keystoreToken.toCharArray());
        }
        return newKeystore;
    }

    public static String addCertificate(KeyStore keyStore, X509Certificate certificate, String alias) throws KeyStoreException {

        String aliasPrivate = StringUtils.isBlank(alias) ? createAliasFromCert(certificate) : normalizeAlias(alias);

        if (keyStore.containsAlias(aliasPrivate)) {
            int i = 1;
            while (keyStore.containsAlias(format(ALIAS_TEMPLATE, aliasPrivate, i))) {
                i++;
            }
            aliasPrivate = format(ALIAS_TEMPLATE, aliasPrivate, i);
        }

        keyStore.setCertificateEntry(aliasPrivate, certificate);
        return aliasPrivate;
    }

    public static String addKeyEntry(KeyStore keyStore, PrivateKey key, X509Certificate[] chain, String keySecurityToken, String alias) throws KeyStoreException {
        String aliasPrivate = StringUtils.isBlank(alias) ? createAliasFromCert(chain[0]) : normalizeAlias(alias);
        if (keyStore.containsAlias(aliasPrivate)) {
            int i = 1;
            while (keyStore.containsAlias(format(ALIAS_TEMPLATE, aliasPrivate, i))) {
                i++;
            }
            aliasPrivate = format(ALIAS_TEMPLATE, aliasPrivate, i);
        }

        keyStore.setKeyEntry(aliasPrivate, key, keySecurityToken.toCharArray(), chain);
        return aliasPrivate;
    }

    public static String getAliasForCertificate(KeyStore keyStore, X509Certificate certificate) throws KeyStoreException {
        List<String> allAliases = getAllAliases(keyStore);
        for (String alias : allAliases) {
            Certificate entry = keyStore.getCertificate(alias);
            if (entry instanceof X509Certificate && entry.equals(certificate)) {
                return alias;
            }
        }
        return null;
    }

    public static String normalizeAlias(String alias) {
        return StringUtils.isBlank(alias) ? null : alias.trim().toLowerCase();
    }

    /**
     * Insert keys/certificates from sourceKeystore to target Keystore. If certificate with alias already exists alias is
     * appended with _%03d as example
     * _001
     *
     * @param targetKeystore the keystore to merge the source keystore data
     * @param targetPassword the password to modify the target keystore
     * @param sourceKeystore the keystore with data to import to target keystore
     * @param sourcePassword the password to read the data from the source keystore
     * @return list of aliases of the newly added certificates
     * @throws KeyStoreException         can not readwrite the keystore
     * @throws UnrecoverableKeyException can not enter to the keystore
     * @throws NoSuchAlgorithmException  keystore algorithms are not supported
     */
    public static List<String> mergeKeystore(KeyStore targetKeystore, String targetPassword, KeyStore sourceKeystore, String sourcePassword) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        // Get all aliases in the old keystore
        Enumeration<String> enumeration = sourceKeystore.aliases();
        List<String> listAliases = new ArrayList<>();
        while (enumeration.hasMoreElements()) {
            // Determine the current alias
            String alias = enumeration.nextElement();
            String importAlias = getNewImportAlias(targetKeystore, alias);
            // import  Key & Certificates or certificate
            if (sourceKeystore.isKeyEntry(alias)) {
                Key key = sourceKeystore.getKey(alias, sourcePassword.toCharArray());
                Certificate[] certsChain = sourceKeystore.getCertificateChain(alias);
                targetKeystore.setKeyEntry(importAlias, key, targetPassword.toCharArray(), certsChain);
            } else {
                Certificate cert = sourceKeystore.getCertificate(alias);
                targetKeystore.setCertificateEntry(importAlias, cert);
            }
            listAliases.add(importAlias);
        }
        return listAliases;
    }

    private static String getNewImportAlias(KeyStore target, String alias) throws KeyStoreException {
        String newAlias = alias;
        int i = 1;
        while (target.containsAlias(newAlias)) {
            newAlias = alias + String.format("_%03d", i++);
        }
        return newAlias;
    }

    /**
     * Method generate alias from Certificate CN value. IF CN value does not exist then it generated random UUID.
     *
     * @param x509cert the certificate for generating the alias
     * @return generated alias
     */
    public static String createAliasFromCert(X509Certificate x509cert) {
        String dn = x509cert.getSubjectX500Principal().getName();
        String alias = null;
        try {

            LdapName ldapDN = new LdapName(dn);
            for (Rdn rdn : ldapDN.getRdns()) {
                Rdn cn = rdn.size() > 1 ? getCNValueFromMultiValue(rdn.toAttributes().getAll()) : null;
                if (cn != null) {
                    alias = StringUtils.trim(cn.getValue().toString());
                } else if (Objects.equals("CN", rdn.getType())) {
                    alias = StringUtils.trim(rdn.getValue().toString());
                }
                if (StringUtils.isNotBlank(alias)) {
                    break;
                }
            }

        } catch (NamingException e) {
            LOG.error("Can not parse certificate subject: [{}]", dn);
        }

        return formatNewAlias(alias);
    }

    private static Rdn getCNValueFromMultiValue(NamingEnumeration<? extends Attribute> enr) throws NamingException {

        while (enr.hasMore()) {
            Object mvRDn = enr.next();
            if (mvRDn instanceof BasicAttribute) {
                BasicAttribute ba = (BasicAttribute) mvRDn;
                if (Objects.equals("CN", ba.getID())) {
                    return new Rdn(ba.getID(), ba.get());
                }
            }
        }
        return null;
    }

    /**
     * Method replaces line spaces with _,  alias to lowercase and substring to 32 chars. if alias is blank then
     * it generates random UUID as alias.
     *
     * @param alias - format the alias
     * @return formatted alias
     */
    protected static String formatNewAlias(String alias) {

        String privateAlias = StringUtils.isBlank(alias) ? UUID.randomUUID().toString() : alias.toLowerCase().trim();
        // replace spaces
        privateAlias = privateAlias.replaceAll("\\s+", "_");
        if (privateAlias.length() > 32) {
            privateAlias = privateAlias.substring(0, 32);
        }
        return privateAlias;
    }

    /**
     * Method deleted the certificate for alias and returns delete certificate. If alias is empty or certificate does not exist then returns null
     * and nothing is deleted. Method does not store the change!
     *
     * @param keyStore
     * @param alias
     * @return X509Certificate
     * @throws KeyStoreException
     */
    public static X509Certificate deleteCertificate(KeyStore keyStore, String alias) throws KeyStoreException {
        X509Certificate certificate = getCertificate(keyStore, alias);
        if (certificate != null) {
            keyStore.deleteEntry(alias);
        }
        return certificate;
    }

    /**
     * Method returns Certificate for alias if exits. If alias is empty or certificate does not exists
     * it returns null.
     *
     * @param keyStore - open and initialized keystore!
     * @param alias    - alias for the certificate
     * @return X509Certificate
     * @throws KeyStoreException
     */
    public static X509Certificate getCertificate(KeyStore keyStore, String alias) throws KeyStoreException {
        return StringUtils.isEmpty(alias) || !keyStore.containsAlias(alias) ? null :
                (X509Certificate) keyStore.getCertificate(alias);
    }

    public static List<String> getAllAliases(KeyStore keyStore) throws KeyStoreException {
        List<String> aliases = new ArrayList<>();
        Enumeration<String> enumAliases = keyStore.aliases();
        while (enumAliases.hasMoreElements()) {
            aliases.add(enumAliases.nextElement());
        }
        return aliases;
    }

    /**
     * Returns the set of aliases of each entry having certificates present in the second keystore that are also present in the first one.
     *
     * @param keystore keystore file where to look for duplicates
     * @param other keystore file from where to load certificates that are to be checked
     * @return the set of duplicate certificates
     * @throws KeyStoreException when not able to read keystore aliases
     */
    public static Set<String> findDuplicateCertificates(KeyStore keystore, KeyStore other) throws KeyStoreException {
        if (keystore == null || other == null) {
            LOG.debug("No duplicate certificates because at least one keystore is null");
            return Collections.emptySet();
        }

        return list(other.aliases())
                .stream()
                .filter(alias -> containsCertificate(keystore, other, alias))
                .peek(alias -> LOG.debug("Found entry with duplicate certificate [{}]", alias))
                .collect(Collectors.toSet());
    }

    /**
     * Returns whether the target keystore contains the certificate present in the source keystore under an entry having
     * a specific alias.
     *
     * <p>The certificate may be found in the target keystore under a key-pair entry or under a certificate-only one; in
     * both cases, {@code true} is returned.</p>
     *
     * <p>If any of the provided keystore files are {@code null}, if the provided alias is blank or if the certificate
     * is not found in the initial source keystore under the provided alias, {@code false} is returned.</p>
     *
     *
     * @param target the keystore where we look for the presence of the certificate
     * @param source the initial keystore where we read the certificate from
     * @param alias the alias of the entry in the initial keystore where the certificate is located
     * @return returns {@code true} if the certificate is present in the target keystore; otherwise, returns, {@code false}.
     */
    public static boolean containsCertificate(KeyStore target, KeyStore source, String alias) {
        if (source == null || target == null) {
            LOG.debug("No certificate to find because at least one keystore is null");
            return false;
        }

        if (StringUtils.isBlank(alias)) {
            LOG.debug("No certificate to find because alias is blank [{}]", alias);
            return false;
        }

        try {
            Certificate certificate = source.getCertificate(alias);
            return target.getCertificateAlias(certificate) != null;
        } catch (KeyStoreException e) {
            LOG.error("An error occurred while loading the entry [{}] from the source keystore", alias, e);
            return false;
        }
    }
}
