package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import eu.europa.ec.edelivery.security.cert.URLFetcher;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.classic.methods.HttpUriRequestBase;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.io.entity.ByteArrayEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.apache.hc.core5.http.HttpHeaders.CONTENT_TYPE;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 * <p>
 * The purpose of the class is HTTP/HTTPS URL fetcher implementation.
 */
public class HttpURLFetcher implements URLFetcher {

    private static final Logger LOG = LoggerFactory.getLogger(HttpURLFetcher.class);
    public static final int DEF_PROXY_PORT = 80;

    ProxyDataCallback proxyDataCallback;

    public HttpURLFetcher(ProxyDataCallback proxyDataCallback) {
        this.proxyDataCallback = proxyDataCallback;
    }

    /**
     * Method fetch the URL. Method uses proxy settings if the proxy configuration is given by the ProxyDataCallback.
     * It verifies the allowed URL protocols. If the AllowedURLProtocols from the callback method is empty por null
     * then all URL protocols are allowed.
     *
     * @param url -fetch data from the URL
     * @return return input stream
     * @throws IOException
     */
    public InputStream fetchURL(final String url) throws IOException {
        return fetchURL(url, null, null);
    }

    @Override
    public InputStream fetchURL(String url, Map<String, String> headers, InputStream stream) throws IOException {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        try {
            String crlURLPrivate = StringUtils.trim(url);
            List<String> allowedProtocols = proxyDataCallback != null && proxyDataCallback.getAllowedURLProtocols() != null ?
                    proxyDataCallback.getAllowedURLProtocols() : Collections.emptyList();
            // if list is empty all protocols are allowed!
            boolean allowedProtocol = allowedProtocols.isEmpty() ||
                    allowedProtocols.stream().anyMatch(proto -> StringUtils.startsWithIgnoreCase(crlURLPrivate, proto));

            InputStream inputStream = null;
            if (allowedProtocol) {
                URL targetUrl = new URL(url);
                if (useProxy() && !doesTargetMatchNonProxy(targetUrl.getHost(), proxyDataCallback.getHttpNoProxyHosts())
                ) {
                    LOG.debug("Using proxy for accessing th URL: [{}] with stream [{}].", url, stream);
                    String securityToken = proxyDataCallback.getSecurityToken();
                    Integer proxyPort = proxyDataCallback.getHttpProxyPort();
                    inputStream = downloadURLViaProxy(url, headers, stream, proxyDataCallback.getHttpProxyHost(),
                            proxyPort != null ? proxyPort : DEF_PROXY_PORT,
                            proxyDataCallback.getUsername(), securityToken);
                } else {
                    LOG.debug("Using direct access for accessing th URL: [{}] with stream [{}].", url, stream);
                    inputStream = downloadURLDirect(url, headers, stream);
                }
            }
            return inputStream;
        } catch (Exception exc) {
            throw new IOException("Error occurred while fetching CRL [" + url + "]. Error:" + ExceptionUtils.getRootCauseMessage(exc), exc);
        }
    }

    public InputStream downloadURLViaProxy(String url, Map<String, String> headers, InputStream stream,
                                           String proxyHost, Integer proxyPort, String proxyUser,
                                           String proxyPassword) throws IOException {

        BasicCredentialsProvider credentialsProvider = null;
        if (isValidParameter(proxyUser, proxyPassword)) {
            credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(new AuthScope(proxyHost, proxyPort),
                    new UsernamePasswordCredentials(proxyUser, proxyPassword.toCharArray()));
        }
        try (CloseableHttpClient httpclient = credentialsProvider == null ? HttpClients.custom().build() :
                HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build()) {

            RequestConfig config = RequestConfig.custom()
                    .setProxy(new HttpHost(proxyHost, proxyPort))
                    .build();
            HttpUriRequestBase httpRequest = buildHttpRequest(url, headers, stream);

            httpRequest.setConfig(config);
            // log username
            String logUserName = credentialsProvider == null ? "None" : proxyUser;
            LOG.debug("Executing request '{}' via proxy '{}' with user: '{}'.", url, proxyHost,
                    logUserName);

            return execute(httpclient, httpRequest);
        }
    }

    public InputStream downloadURLDirect(String url, Map<String, String> headers, InputStream stream) throws IOException {
        HttpUriRequestBase httpRequest = buildHttpRequest(url, headers, stream);
        return execute(HttpClients.createDefault(), httpRequest);
    }

    public HttpUriRequestBase buildHttpRequest(String url, Map<String, String> headers, InputStream stream) throws IOException {
        HttpUriRequestBase httpRequest = stream == null ? new HttpGet(url) : new HttpPost(url);

        if (stream != null) {
            ContentType contentType = headers != null && headers.containsKey(CONTENT_TYPE) ?
                    ContentType.create(headers.remove(CONTENT_TYPE)) :
                    ContentType.APPLICATION_OCTET_STREAM;
            LOG.debug("Submit messsage with content type: [{}].", contentType);
            ByteArrayEntity entity = new ByteArrayEntity(StreamUtils.copyToByteArray(stream), contentType);
            httpRequest.setEntity(entity);
        }
        if (headers != null && !headers.isEmpty()) {
            headers.forEach(httpRequest::setHeader);
        }
        return httpRequest;
    }

    protected InputStream execute(CloseableHttpClient httpclient, HttpUriRequestBase httpRequest) throws IOException {
        try (CloseableHttpResponse response = httpclient.execute(httpRequest)) {
            // read data from socket input stream to internal buffer.
            return new ByteArrayInputStream(StreamUtils.copyToByteArray(response.getEntity().getContent()));
        }
    }

    protected boolean useProxy() {
        if (proxyDataCallback != null && !proxyDataCallback.isProxyEnabled()) {
            LOG.debug("Proxy not required. The property useProxy is not configured");
            return false;
        }
        return true;
    }

    protected boolean isValidParameter(String... parameters) {
        if (parameters == null || parameters.length == 0) {
            return false;
        }

        for (String parameter : parameters) {
            if (StringUtils.isEmpty(parameter)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method validates if host match non proxy list
     *
     * @param uriHost          target host
     * @param nonProxyHostList non proxy ist
     * @return true if host match nonProxy list else return false.
     */
    public boolean doesTargetMatchNonProxy(String uriHost, String nonProxyHostList) {
        String[] nonProxyHosts = StringUtils.isBlank(nonProxyHostList) ? null : nonProxyHostList.split("\\|");

        int nphLength = nonProxyHosts != null ? nonProxyHosts.length : 0;
        if (nonProxyHosts == null || nphLength < 1) {
            LOG.debug("host: [{}]: DEFAULT (0 non proxy host)", uriHost);
            return false;
        }

        for (String nonProxyHost : nonProxyHosts) {
            String mathcRegExp = (nonProxyHost.startsWith("*") ? "." : "") + nonProxyHost;
            if (uriHost.matches(mathcRegExp)) {
                LOG.debug(" host: [{}] matches nonProxyHost  [{}] : NO PROXY", uriHost, mathcRegExp);
                return true;
            }
        }
        LOG.debug(" host: [{}]: DEFAULT  (no match of [{}] non proxy host)", uriHost, nonProxyHosts);
        return false;
    }

}
