package eu.europa.ec.edelivery.exception;

/**
 * Runtime exception thrown on unexpected internal events as "NoSuchAlgorithmException"
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class SecRuntimeException extends RuntimeException  {
    public SecRuntimeException(String message) {
        super(message);
    }

    public SecRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
