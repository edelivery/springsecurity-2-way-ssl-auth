package eu.europa.ec.edelivery.exception;

import java.security.cert.CertificateException;

/**
 * Exception thrown when a technical error occurs during the validation of a certificate.
 *
 * @author Joze RIHTARSIC
 * @since 1.14
 */
public class CertificateValidationTechnicalException extends CertificateException {
    public enum ErrorCode {
        ExtensionNotFound,
        URLConnectionError,
        BadRequest,
    }

    public final ErrorCode errorCode;


    public CertificateValidationTechnicalException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
