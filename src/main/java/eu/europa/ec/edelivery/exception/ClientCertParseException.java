package eu.europa.ec.edelivery.exception;

public class ClientCertParseException extends RuntimeException {
    public ClientCertParseException() {
    }

    public ClientCertParseException(String message) {
        super(message);
    }

    public ClientCertParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
