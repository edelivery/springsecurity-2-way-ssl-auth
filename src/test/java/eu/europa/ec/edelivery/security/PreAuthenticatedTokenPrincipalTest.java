package eu.europa.ec.edelivery.security;

import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class PreAuthenticatedTokenPrincipalTest {

    @Test
    public void getNameTest() {

        //given
        String token = "SecurityToken";
        //when
        PreAuthenticatedTokenPrincipal principal =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token);

        //then
        assertEquals(token, principal.getName());
    }

    @Test
    public void equalsTest() {
        //given
        String token = "SecurityToken";
        //when
        PreAuthenticatedTokenPrincipal principal1 =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token);
        PreAuthenticatedTokenPrincipal principal2 =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token);

        // then
        assertEquals(principal1, principal2);
    }

    @Test
    public void equalsNotTest() {
        //given
        String token1 = "SecurityToken";
        String token2 = "SecurityTokenTwo";
        //when
        PreAuthenticatedTokenPrincipal principal1 =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token1);
        PreAuthenticatedTokenPrincipal principal2 =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token2);

        // then
        assertNotEquals(principal1, principal2);
    }

    @Test
    public void hashCodeTest() {
        String token = "SecurityToken";
        //when
        PreAuthenticatedTokenPrincipal principal1 =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token);

        assertEquals(Objects.hash(token, EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN), principal1.hashCode());
    }

    @Test
    public void toStringTest() {
        String token = "SecurityToken";
        //when
        PreAuthenticatedTokenPrincipal principal =
                new PreAuthenticatedTokenPrincipal(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, token);

        assertTrue(principal.toString().contains("PreAuthenticatedTokenPrincipal"));
        assertTrue(principal.toString().contains(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN));
    }

}
