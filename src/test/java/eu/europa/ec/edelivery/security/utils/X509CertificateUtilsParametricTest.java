/**
 * (C) Copyright 2018 - European Commission | CEF eDelivery
 * <p>
 * Licensed under the EUPL, Version 1.2 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * \BDMSL\bdmsl-parent-pom\LICENSE-EUPL-v1.2.pdf or https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package eu.europa.ec.edelivery.security.utils;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static eu.europa.ec.edelivery.security.utils.X509CertificateUtilsTest.getBytes;
import static eu.europa.ec.edelivery.security.utils.X509CertificateUtilsTest.loadCertificate;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 */
public class X509CertificateUtilsParametricTest {

    @BeforeAll
    public static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    private static Object[] crlTestListCases() {
        return new Object[][]{
                {"smp-crl-test-all.pem", "https://localhost/clr,http://localhost/clr,ldap://localhost/clr"},
                {"smp-crl-test-https.pem", "https://localhost/crl"},
                {"smp-crl-test-ldap.pem", "ldap://localhost/crl"},
                {"smp-crl-test-nolist.pem", null},
        };
    }

    private static Object[] crlExtractHTTPSTestListCases() {
        return new Object[][]{
                {"ldap://localhost/clr,https://localhost/clr,http://localhost/clr", "https://localhost/clr"},
                {"https://localhost/clr", "https://localhost/clr"},
                {"http://localhost/crl", "http://localhost/crl"},
                {"ldap://localhost/clr", null},
                {"", null},
        };
    }

    private static Object[] ocspTestListCases() {
        return new Object[][]{
                {"smp-ocsp-test.pem", "http://localhost/ocsp"},
                {"smp-ocsp-test-multiple-entries.pem", "https://localhost/ocsp,http://localhost/ocsp"},
                {"smp-ocsp-test-iai-multiple-entries.pem", "http://localhost/ocsp2"},
                {"smp-crl-test-nolist.pem", null},
        };
    }

    private static Object[] parseTestCases() {
        return new Object[][]{
                {"certificate-pem-with-header.cer"},
                {"PeppolTestSMP-DER-encoded.crt"},
                {"PeppolTestSMP-PEM-encoded.pem"},
                {"PeppolTestSMP-PEM-encoded-CRLF.txt"},
        };
    }

    @ParameterizedTest
    @MethodSource("parseTestCases")
    public void parseCertificateTest(String certificateFileName) throws CertificateException, IOException {
        //given
        byte[] buff = getBytes(certificateFileName);

        X509Certificate certificate = X509CertificateUtils.getX509Certificate(buff);
        assertNotNull(certificate);
    }

    @ParameterizedTest
    @MethodSource("crlTestListCases")
    public void getCrlDistributionPointsTest(String certificateFileName, String clrLists) throws CertificateException {
        //given
        X509Certificate certificate = loadCertificate(certificateFileName);
        List<String> lstExpected = clrLists == null ? Collections.emptyList() : Arrays.asList(clrLists.split(","));
        //when
        List<String> lstValues = X509CertificateUtils.getCrlDistributionPoints(certificate);
        // then
        assertEquals(lstExpected.size(), lstValues.size());
        lstValues.forEach(crl -> {
            lstExpected.contains(crl);
            System.out.println("Contains the url " + crl + " expected: " + clrLists);
            assertTrue(lstExpected.contains(crl));

        });
    }

    @ParameterizedTest
    @MethodSource("ocspTestListCases")
    public void getOCSPLocationsTest(String certificateFileName, String ocspLists) throws CertificateException {

        //given
        X509Certificate certificate = loadCertificate(certificateFileName);
        List<String> lstExpected = ocspLists == null ? Collections.emptyList() : Arrays.asList(ocspLists.split(","));
        //when
        List<String> lstValues = X509CertificateUtils.getOCSPLocations(certificate);
        // then
        assertEquals(lstExpected.size(), lstValues.size());

        lstValues.forEach(ocspUrl -> {
            System.out.println("Contains the url" + ocspUrl + " expected: " + ocspLists);
            assertTrue(lstExpected.contains(ocspUrl));
        });
    }

    @ParameterizedTest
    @MethodSource("crlExtractHTTPSTestListCases")
    public void extractHttpCrlDistributionPoints(String clrLists, String value) {
        //given
        List<String> urlList = clrLists == null ? Collections.emptyList() : Arrays.asList(clrLists.split(","));
        // when
        String url = X509CertificateUtils.extractHttpCrlDistributionPoint(urlList);
        // then
        assertEquals(value, url);
    }

}
