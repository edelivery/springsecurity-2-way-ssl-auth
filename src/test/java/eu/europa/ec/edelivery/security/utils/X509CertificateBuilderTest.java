package eu.europa.ec.edelivery.security.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.util.io.pem.PemObjectGenerator;
import org.bouncycastle.util.io.pem.PemWriter;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import static eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils.CERTIFICATE_POLICY_QCP_LEGAL;
import static eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils.CERTIFICATE_POLICY_QCP_NATURAL;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class X509CertificateBuilderTest {


    @BeforeEach
    public void beforeTest() {
        // default value
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }


    public static PrivateKey getPrivate(String filename, String algorithm)
            throws Exception {

        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance(algorithm);
        return kf.generatePrivate(spec);
    }

    public static PublicKey getPublicKeyFromResource(String filename, String algorithm)
            throws Exception {
        try (InputStream keyIS = X509CertificateBuilderTest.class.getResourceAsStream(filename)) {
            byte[] keyBytes = keyIS.readAllBytes();
            X509EncodedKeySpec spec =
                    new X509EncodedKeySpec(keyBytes);
            KeyFactory kf = KeyFactory.getInstance(algorithm);
            return kf.generatePublic(spec);
        }
    }


    @Test
    public void createCertificatesForKey() throws Exception {
        PublicKey publicEd25519Key = getKeyEd25519FromSsh2Value("AAAAC3NzaC1lZDI1NTE5AAAAIDZO6rcLZHhtdgQeXoKC3CK5zzmPuAx6TfaP/ya4xNN8");
        PublicKey publicX25519Key = getPublicKeyFromResource("/keys/x25519key.pub.der", "X25519");
        assertNotNull(publicEd25519Key);
        assertNotNull(publicX25519Key);
        // create a signing keys for the certificates

       /* KeyPairGenerator keyPairSignGenerator = KeyPairGenerator.getInstance("ED25519");
        KeyPair keyPairSign = keyPairSignGenerator.generateKeyPair();

        */
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPairSign = kpg.generateKeyPair();

        // create subjects and issuer DN


        String subjectEd25519 = "C=EU,O=edelivery,OU=TEST, OU=InterOpEvent (Generated from provided key), CN=TEST-ED25519";
        String subjectX25519 = "C=EU,O=edelivery,OU=TEST, OU=InterOpEvent (Generated from provided key), CN=TEST-X25519";
        String issuerDN = "C=EU,O=edelivery,OU=TEST, OU=InterOpEvent, CN=Issuer-CA";

        BigInteger serialED25519 = new BigInteger("1000000000000001");
        BigInteger serialX25519 = new BigInteger("1000000000000002");

        X509Certificate issuer = X509CertificateBuilder.create()
                .subjectDn(issuerDN)
                .issuerDn(issuerDN)
                .caFlag(true)
                .pathLength(2)
                .subjectPublicKey(keyPairSign.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .serialNumber(serialX25519)
                .notBefore(OffsetDateTime.now().minusDays(1))
                .notAfter(OffsetDateTime.now().plusYears(1))
                .build();

        X509Certificate certificateX25519 = X509CertificateBuilder.create()
                .subjectDn(subjectX25519)
                .issuerDn(issuerDN)
                .subjectPublicKey(publicX25519Key)
                .certSignerKey(keyPairSign.getPrivate())
                .serialNumber(serialX25519)
                .notBefore(OffsetDateTime.now().minusDays(1))
                .notAfter(OffsetDateTime.now().plusYears(1))
                .build();

        X509Certificate certificateED25519 = X509CertificateBuilder.create()
                .subjectDn(subjectEd25519)
                .issuerDn(issuerDN)
                .serialNumber(serialED25519)
                .subjectPublicKey(publicEd25519Key)
                .certSignerKey(keyPairSign.getPrivate())
                .notBefore(OffsetDateTime.now().minusDays(1))
                .notAfter(OffsetDateTime.now().plusYears(1))
                .build();

        assertNotNull(issuer);
        assertNotNull(certificateX25519);
        assertNotNull(certificateED25519);
        Files.write(Paths.get("target", "issuer-ca.pem"), convertToBase64PEMString(issuer).getBytes());
        Files.write(Paths.get("target", "TEST.x25519Cert.pem"), convertToBase64PEMString(certificateX25519).getBytes());
        Files.write(Paths.get("target", "TEST.ed25519Cert.pem"), convertToBase64PEMString(certificateED25519).getBytes());
    }

    /**
     * Converts a {@link X509Certificate} instance into a Base-64 encoded string (PEM format).

     */
    public String convertToBase64PEMString(X509Certificate x509Cert) throws IOException {
        StringWriter sw = new StringWriter();
        try (PemWriter pw = new PemWriter(sw)) {
            PemObjectGenerator gen = new JcaMiscPEMGenerator(x509Cert);
            pw.writeObject(gen);
        }
        return sw.toString();
    }

    public static PublicKey getKeyEd25519FromSsh2Value(String base64Value) throws Exception {

        byte[] rawpub = Base64.getDecoder().decode(base64Value);
        // the headers add asni.1 data  sauch as 1.3.101.112, key size etc..
        byte[] prefix = {0x30, 0x2a, 0x30, 0x05, 0x06, 0x03, 0x2b, 0x65, 0x70, 0x03, 0x21, 0x00};

        byte[] spki = Arrays.copyOf(prefix, prefix.length + 32);
        System.arraycopy(rawpub, 19, spki, prefix.length, 32);

        return  KeyFactory.getInstance("ed25519").generatePublic(new X509EncodedKeySpec(spki));
    }

    @Test
    public void createCertFromKeys() throws Exception {

        //PrivateKey privateKey =  getPrivate("/cef/scripts/key-generation/PartyA.x25519key.der", "X25519");
        PublicKey publicKey = getPublicKeyFromResource("/keys/x25519key.pub.der", "X25519");
        //assertNotNull(privateKey);
        assertNotNull(publicKey);
        String keyAlgorithm = "ED25519";
        String subjectX25519 = "CN=-TEST-X25519,OU=TEST, OU=GeneratedFromProvidedKey For InterOpEvent,O=edelivery, C=eu";
        String issuerDN = "CN=-SigningCert-CA,OU=TEST, OU=InterOpEvent,O=edelivery, C=eu";
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyAlgorithm);
        KeyPair certKeyPair = keyPairGenerator.generateKeyPair();



        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subjectX25519)
                .issuerDn(issuerDN)
                .subjectPublicKey(publicKey)
                .certSignerKey(certKeyPair.getPrivate())
                .build();

        assertNotNull(certificate);
    }

    @ParameterizedTest
    @CsvSource({
            "RSA,2048",
            "RSA,4096",
            "EC,secp256r1",
            "EC,secp384r1",
            "EC,secp521r1",
            "ED25519,",
            "ED448,"
    })
    void create(String keyAlgorithm, String parameter) throws Exception{
        String subject = "CN=" + keyAlgorithm;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyAlgorithm);
        KeyPair certKeyPair = keyPairGenerator.generateKeyPair();

        KeyPairGenerator keyPairSignGenerator = KeyPairGenerator.getInstance(keyAlgorithm);
        if (parameter != null && !parameter.isEmpty()) {
            if (keyAlgorithm.equals("EC")) {
                keyPairSignGenerator.initialize(new ECGenParameterSpec(parameter));
            } else if (keyAlgorithm.equals("RSA")) {
                keyPairSignGenerator.initialize(Integer.parseInt(parameter));
            } else {
                throw new IllegalArgumentException("Unsupported key parameter for algorithm: " + keyAlgorithm);
            }

        }
        KeyPair keyPairSign = keyPairSignGenerator.generateKeyPair();

        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subject)
                .subjectPublicKey(certKeyPair.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .build();

        assertNotNull(certificate);
        assertEquals(subject, certificate.getSubjectDN().getName());
        assertDoesNotThrow(() -> certificate.verify(keyPairSign.getPublic()));
        assertDoesNotThrow(() -> certificate.checkValidity());
    }

    @Test
    void testCreateNonAsciiCharacters() throws Exception {

        String subject = "C=BE,O=European_ż_ẞ_Ẅ_Commission,CN=EHEALTH_ż_ẞ_Ẅ_";
        String serialNumber = "17806925824057915271";

        KeyPair keyPairSign = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subject)
                .subjectPublicKey(keyPairSign.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .serialNumber(new BigInteger(serialNumber))
                .build();

        assertNotNull(certificate);
        MatcherAssert.assertThat(certificate.getSubjectDN().getName(), org.hamcrest.Matchers.containsString("European_ż_ẞ_Ẅ_Commission"));
        assertDoesNotThrow(() -> certificate.verify(keyPairSign.getPublic()));
        assertDoesNotThrow(() -> certificate.checkValidity());
    }

    @Test
    void testCreateWithCertificatePolicy() throws Exception {
        String subject = "C=BE,O=European_Commission,CN=PolicyTest";
        List<String> policyOids = asList(CERTIFICATE_POLICY_QCP_NATURAL, CERTIFICATE_POLICY_QCP_LEGAL);
        KeyPair keyPairSign = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subject)
                .certificatePolicyOids(policyOids)
                .subjectPublicKey(keyPairSign.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .build();

        assertNotNull(certificate);
        MatcherAssert.assertThat(certificate.getSubjectDN().getName(), org.hamcrest.Matchers.containsString("CN=PolicyTest"));
        assertDoesNotThrow(() -> certificate.verify(keyPairSign.getPublic()));
        assertDoesNotThrow(() -> certificate.checkValidity());
        assertTrue(X509CertificateUtils.getCertificatePolicyIdentifiers(certificate).containsAll(policyOids));
    }

    @Test
    void testCreateWithCertificateWithCLR() throws Exception {
        String subject = "C=BE,O=European_Commission,CN=PolicyTest";
        List<String> crlURls = asList("http://test/crl","https://test/crl");
        KeyPair keyPairSign = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subject)
                .crlURls(crlURls)
                .subjectPublicKey(keyPairSign.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .build();

        assertNotNull(certificate);
        MatcherAssert.assertThat(certificate.getSubjectDN().getName(), org.hamcrest.Matchers.containsString("CN=PolicyTest"));
        assertDoesNotThrow(() -> certificate.verify(keyPairSign.getPublic()));
        assertDoesNotThrow(() -> certificate.checkValidity());
        assertTrue(X509CertificateUtils.getCrlDistributionPoints(certificate).containsAll(crlURls));
    }

    @Test
    void testCreateWithCertificateWithOCSP() throws Exception {
        String subject = "C=BE,O=European_Commission,CN=PolicyTest";
        List<String> ocspURls = asList("http://test/ocsp","https://test/ocsp");
        KeyPair keyPairSign = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        X509Certificate certificate = X509CertificateBuilder.create()
                .subjectDn(subject)
                .ocspUrls(ocspURls)
                .subjectPublicKey(keyPairSign.getPublic())
                .certSignerKey(keyPairSign.getPrivate())
                .build();

        assertNotNull(certificate);
        MatcherAssert.assertThat(certificate.getSubjectDN().getName(), org.hamcrest.Matchers.containsString("CN=PolicyTest"));
        assertDoesNotThrow(() -> certificate.verify(keyPairSign.getPublic()));
        assertDoesNotThrow(() -> certificate.checkValidity());
        assertTrue(X509CertificateUtils.getOCSPLocations(certificate).containsAll(ocspURls));
    }
}
