package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static eu.europa.ec.edelivery.security.utils.X509CertificateUtilsTest.loadCertificate;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 */
public class KeystoreUtilsTest {

    Path testKeystorePath = Paths.get("src", "test", "resources", "keystores");

    //  test data
    KeyStore testKeystore;
    private static X509Certificate certificate01;
    private static X509Certificate certificate02;
    private static X509Certificate certificate03;
    private static X509Certificate certificate04;
    private static X509Certificate certificate05EDELIVERY7663;


    @BeforeAll
    public static void initClassData() throws Exception {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
        certificate01 = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 01,O=Digit,C=BE");
        certificate02 = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 02,O=Digit,C=BE");
        certificate03 = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 03,O=Digit,C=BE");
        certificate04 = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 04,O=Digit,C=BE");
        certificate05EDELIVERY7663 = loadCertificate("smp_edelivery_smp_foces.cer");
    }

    @BeforeEach
    void beforeEach() throws Exception {
        System.out.println("init keystore *********************************************");
        testKeystore = KeyStore.getInstance("PKCS12");
        testKeystore.load(null, "test123".toCharArray()); // just to initialize keystore
        testKeystore.setCertificateEntry("alias 1",
                certificate01);
        testKeystore.setCertificateEntry("alias 2",
                certificate02);
        testKeystore.setCertificateEntry("alias 3",
                certificate03);
    }

    @Test
    public void testLoadKeystoreJKS() throws Exception {

        File keystoreJKS = new File(testKeystorePath.toFile(), "test-keystore.jks");
        KeyStore keyStore = KeystoreUtils.loadKeystore(keystoreJKS, "test123", "JKS");

        assertNotNull(keyStore);
        assertTrue(keyStore.containsAlias("testcert"));
    }

    @Test
    public void testLoadKeystorePKCS12() throws Exception {

        File keystorePKCS12 = new File(testKeystorePath.toFile(), "test-keystore.p12");
        KeyStore keyStore = KeystoreUtils.loadKeystore(keystorePKCS12, "test123", "PKCS12");

        assertNotNull(keyStore);
        assertTrue(keyStore.containsAlias("testcert"));
    }

    @Test
    public void testLoadKeystoreWithoutTypeJKS() throws Exception {

        File keystorePKCS12 = new File(testKeystorePath.toFile(), "test-keystore.jks");
        KeyStore keyStore = KeystoreUtils.loadKeystore(keystorePKCS12, "test123");

        assertNotNull(keyStore);
        assertTrue(keyStore.containsAlias("testcert"));
    }

    @Test
    public void testLoadKeystoreWithoutTypePKCS12() throws Exception {

        File keystorePKCS12 = new File(testKeystorePath.toFile(), "test-keystore.p12");
        KeyStore keyStore = KeystoreUtils.loadKeystore(keystorePKCS12, "test123");

        assertNotNull(keyStore);
        assertTrue(keyStore.containsAlias("testcert"));
    }

    @Test
    public void testLoadKeystoreWithoutTypeNotExistFailure() {

        File keystorePKCS12 = new File(testKeystorePath.toFile(), "test-keystore-notExists.p12");
        IOException exception = assertThrows(IOException.class,
                () -> KeystoreUtils.loadKeystore(keystorePKCS12, "test123", "PKCS12"));

        assertNotNull(exception);
        MatcherAssert.assertThat(exception.getMessage(), CoreMatchers.containsString("test-keystore-notExists.p12"));
    }

    @Test
    public void testLoadKeystoreNullWhenTypeIsNotGiven() {

        File keystorePKCS12 = new File(testKeystorePath.toFile(), "test-keystore-notExists.p12");
        String password = "pass";
        // when
        KeyStore result = KeystoreUtils.loadKeystore(keystorePKCS12, password);
        assertNull(result);

    }

    @Test
    public void formatNewAliasLowerCase() {
        String alias = KeystoreUtils.formatNewAlias("TestAliasWITHUPPERCASE");
        assertEquals("testaliaswithuppercase", alias);
    }

    @Test
    public void formatNewAliasTrim() {
        String alias = KeystoreUtils.formatNewAlias(" trimspaces  ");
        assertEquals("trimspaces", alias);
    }

    @Test
    public void formatNewAliasReplaceSpaces() {
        String alias = KeystoreUtils.formatNewAlias("replace spaces in alias");
        assertEquals("replace_spaces_in_alias", alias);
    }

    @Test
    public void formatNewAliasReplaceSpacesEDELIVERY7663() {
        String alias = KeystoreUtils.formatNewAlias("SMP_eDelivery SMP-FOCES (funktionscertifikat)");
        assertEquals("smp_edelivery_smp-foces_(funktio", alias);
    }

    @Test
    public void formatNewAliasSetUUID() {
        String alias = KeystoreUtils.formatNewAlias("");
        assertTrue(StringUtils.isNotBlank(alias));
    }

    @Test
    public void formatNewAliasSubstring32() {
        String alias = KeystoreUtils.formatNewAlias("123456789012345678901234567890123");
        assertEquals("12345678901234567890123456789012", alias);
    }

    @Test
    public void testNormalizeAliasLowerCase() {
        String alias = KeystoreUtils.normalizeAlias("TestAliasWITHUPPERCASE");
        assertEquals("testaliaswithuppercase", alias);
    }

    @Test
    public void testtNormalizeAliasTrim() {
        String alias = KeystoreUtils.normalizeAlias(" trimspaces  ");
        assertEquals("trimspaces", alias);
    }

    @Test
    public void testNormalizeAliasBlankBlank() {
        String alias = KeystoreUtils.normalizeAlias("  ");
        assertNull(alias);
    }

    @Test
    public void testGetAllAliases() throws KeyStoreException {
        List<String> aliases = KeystoreUtils.getAllAliases(testKeystore);
        assertEquals(3, aliases.size());
        assertTrue(aliases.contains("alias 1"));
        assertTrue(aliases.contains("alias 2"));
        assertTrue(aliases.contains("alias 3"));
    }

    @Test
    public void testAddCertificate() throws KeyStoreException {
        // given
        List<String> aliases = KeystoreUtils.getAllAliases(testKeystore);
        // when
        KeystoreUtils.addCertificate(testKeystore, certificate04, "new alias");
        // then
        List<String> newAliases = KeystoreUtils.getAllAliases(testKeystore);
        assertEquals(aliases.size() + 1, newAliases.size());
        assertTrue(newAliases.contains("new alias"));
    }

    @Test
    public void testAddCertificateWithNullAlias() throws KeyStoreException {
        // given
        List<String> aliases = KeystoreUtils.getAllAliases(testKeystore);

        // when
        KeystoreUtils.addCertificate(testKeystore, certificate04, null);
        // then
        List<String> newAliases = KeystoreUtils.getAllAliases(testKeystore);
        assertEquals(aliases.size() + 1, newAliases.size());
        assertTrue(newAliases.contains("keystore_test_04"));
    }

    @Test
    public void testAddCertificateWithNullAlias2() throws KeyStoreException {
        // given
        List<String> aliases = KeystoreUtils.getAllAliases(testKeystore);

        // when
        KeystoreUtils.addCertificate(testKeystore, certificate05EDELIVERY7663, null);
        // then
        List<String> newAliases = KeystoreUtils.getAllAliases(testKeystore);
        assertEquals(aliases.size() + 1, newAliases.size());
        assertTrue(newAliases.contains("smp_edelivery_smp-foces_(funktio"));
    }

    @Test
    public void testAddCertificateWithTheSameAlias() throws KeyStoreException {
        // when
        String alias = KeystoreUtils.addCertificate(testKeystore, certificate04, "alias 1");
        // then
        assertEquals("alias 1_001", alias);
    }

    @Test
    public void testDeleteCertificate() throws KeyStoreException {
        // given
        List<String> aliases = KeystoreUtils.getAllAliases(testKeystore);

        // when
        KeystoreUtils.deleteCertificate(testKeystore, "alias 1");
        // then
        List<String> newAliases = KeystoreUtils.getAllAliases(testKeystore);
        assertEquals(aliases.size() - 1, newAliases.size());
        assertFalse(newAliases.contains("keystore_test_04"));
    }

    @Test
    public void testGetAliasForCertificate() throws KeyStoreException {
        // given
        // when
        String alias = KeystoreUtils.getAliasForCertificate(testKeystore, certificate02);
        // then
        assertEquals("alias 2", alias);
    }

    @Test
    public void testGetCertificate() throws KeyStoreException {
        // given
        // when
        X509Certificate certificate = KeystoreUtils.getCertificate(testKeystore, "alias 2");
        // then
        assertEquals(certificate02, certificate);
    }

    @Test
    public void testCreateAliasFromCert() throws KeyStoreException {
        // given
        X509Certificate certificate = KeystoreUtils.getCertificate(testKeystore, "alias 2");
        assertNotNull(certificate);
        // when
        String alias = KeystoreUtils.createAliasFromCert(certificate);
        // then
        assertEquals("keystore_test_02", alias);
    }

    @Test
    public void testFindDuplicateCertificate() throws Exception {
        // when
        Set<String> duplicateCertificates = KeystoreUtils.findDuplicateCertificates(testKeystore, testKeystore);

        // then
        assertEquals(new HashSet<>(Arrays.asList("alias 1", "alias 2", "alias 3")), duplicateCertificates);
    }

    @Test
    public void testFindDuplicateCertificate_noMatches() throws Exception {
        // when
        KeyStore emptyKeystore = KeyStore.getInstance("jks");
        emptyKeystore.load(null);
        Set<String> duplicateCertificates = KeystoreUtils.findDuplicateCertificates(emptyKeystore, testKeystore);

        // then
        assertEquals(new HashSet<>(), duplicateCertificates);
    }

    @Test
    public void testFindDuplicateCertificate_nullFirstKeystore() throws Exception {
        // when
        Set<String> duplicateCertificates = KeystoreUtils.findDuplicateCertificates(null, testKeystore);

        // then
        assertEquals(new HashSet<>(), duplicateCertificates);
    }


    @Test
    public void testFindDuplicateCertificate_nullSecondKeystore() throws Exception {
        // when
        Set<String> duplicateCertificates = KeystoreUtils.findDuplicateCertificates(testKeystore, null);

        // then
        assertEquals(new HashSet<>(), duplicateCertificates);
    }

    @Test
    public void testContainsCertificate_nullTargetKeystore() {
        // when
        boolean found = KeystoreUtils.containsCertificate(null, testKeystore, "alias 1");

        // then
        assertFalse(found);
    }

    @Test
    public void testContainsCertificate_nullSourceKeystore() {
        // when
        boolean found = KeystoreUtils.containsCertificate(testKeystore, null, "alias 1");

        // then
        assertFalse(found);
    }

    @Test
    public void testContainsCertificate_nullAlias() {
        // when
        boolean found = KeystoreUtils.containsCertificate(testKeystore, testKeystore,  null);

        // then
        assertFalse(found);
    }

    @Test
    public void testContainsCertificate_emptyAlias() {
        // when
        boolean found = KeystoreUtils.containsCertificate(testKeystore, testKeystore,  "  ");

        // then
        assertFalse(found);
    }

    @Test
    public void testContainsCertificate() {
        // when
        boolean found = KeystoreUtils.containsCertificate(testKeystore, testKeystore,  "alias 2");

        // then
        assertTrue(found);
    }

}
