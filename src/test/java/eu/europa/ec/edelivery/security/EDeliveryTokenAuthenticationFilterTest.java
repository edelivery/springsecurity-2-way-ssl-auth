package eu.europa.ec.edelivery.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import jakarta.servlet.ServletException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EDeliveryTokenAuthenticationFilterTest {

    @BeforeEach
    public void clearContext() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void buildDetailsTest() {
        EDeliveryTokenAuthenticationFilter filter = new EDeliveryTokenAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();

        request.addHeader(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, "securityToken");

        // then
        PreAuthenticatedTokenPrincipal principal = filter.buildDetails(request);

        assertNotNull(principal);
        assertEquals("securityToken", principal.getName());

    }

    @Test
    public void buildDetailsNullTest() {
        EDeliveryTokenAuthenticationFilter filter = new EDeliveryTokenAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        // then
        PreAuthenticatedTokenPrincipal principal = filter.buildDetails(request);

        assertNull(principal);

    }

    @Test
    public void missingHeaderMonitorIsIgnoredTest() throws IOException, ServletException {
        //given-when
        runAuthWithHeader(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, null);

        //then
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void missingHeaderAdminIsIgnoredTest() throws IOException, ServletException {
        //given-when

        runAuthWithHeader(EDeliveryTokenAuthenticationFilter.S_HEADER_ADMIN_TOKEN, null);

        //then
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void adminPasswordAuthenticationTest() throws IOException, ServletException {
        //given-when
        runAuthWithHeader(EDeliveryTokenAuthenticationFilter.S_HEADER_ADMIN_TOKEN, "test");

        //then
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertNotNull(authentication.getPrincipal());
        assertEquals(PreAuthenticatedTokenPrincipal.class, authentication.getPrincipal().getClass());
        assertEquals("test", ((PreAuthenticatedTokenPrincipal) authentication.getPrincipal()).getName());

    }

    @Test
    public void monitorPasswordAuthenticationTest() throws IOException, ServletException {
        //given-when
        runAuthWithHeader(EDeliveryTokenAuthenticationFilter.S_HEADER_MONITOR_TOKEN, "test");

        //then
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(authentication);
        assertNotNull(authentication.getPrincipal());
        assertEquals(PreAuthenticatedTokenPrincipal.class, authentication.getPrincipal().getClass());
        assertEquals("test", ((PreAuthenticatedTokenPrincipal) authentication.getPrincipal()).getName());

    }

    private void runAuthWithHeader(String header, String headerValue) throws IOException, ServletException {
        //given
        EDeliveryTokenAuthenticationFilter filter = new EDeliveryTokenAuthenticationFilter();
        MockHttpServletRequest request = new MockHttpServletRequest();
        if (headerValue != null) {
            request.addHeader(header, headerValue);
        }
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain chain = new MockFilterChain();

        filter.setAuthenticationManager(createAuthenticationManager());

        //when
        filter.doFilter(request, response, chain);
    }

    /**
     * Create an authentication manager which returns the passed in object.
     */
    private AuthenticationManager createAuthenticationManager() {
        AuthenticationManager am = mock(AuthenticationManager.class);
        when(am.authenticate(any(Authentication.class))).thenAnswer(
                (Answer<Authentication>) invocation -> (Authentication) invocation.getArguments()[0]);
        return am;
    }
}
