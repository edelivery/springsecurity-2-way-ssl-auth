/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package eu.europa.ec.edelivery.security.eu.europa.ec.edelivery.text;

import eu.europa.ec.edelivery.text.DistinguishedNamesCodingUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Method tests {@link DistinguishedNamesCodingUtil} class.
 *
 * @author Pawel GUTOWSKI
 * @author Joze RIHTARSIC
 * @since 1.0
 */

public class DistinguishedNamesCodingUtilTest {

    public static String[][] allUtfHexDecoderTestCases() {
        return new String[][]{{
                "Regular string stays unchanged!",
                "Regular string stays unchanged!"
        }, {
                "Polish: Za\\xC5\\xBC\\xC3\\xB3\\xC5\\x82\\xC4\\x87 g\\xC4\\x99\\xC5\\x9Bl\\xC4\\x85 ja\\xC5\\xBA\\xC5\\x84",
                "Polish: Zażółć gęślą jaźń"
        }, {
                "Polish HEX lowercase: Za\\xc5\\xbc\\xc3\\xb3\\xc5\\x82\\xc4\\x87 g\\xc4\\x99\\xc5\\x9bl\\xc4\\x85 ja\\xc5\\xba\\xc5\\x84",
                "Polish HEX lowercase: Zażółć gęślą jaźń"
        }, {
                "Norwegian/Swedish: \\xC3\\x86 \\xC3\\xA6,\\xC3\\x98 \\xC3\\xB8,\\xC3\\x85 \\xC3\\xA5",
                "Norwegian/Swedish: Æ æ,Ø ø,Å å",
        }, {
                "German and umlauts:\\xE1\\xBA\\x9E \\xC3\\x9F,\\xC3\\x84 \\xC3\\xA4,\\xC3\\x8B \\xC3\\xAB,\\xC3\\x8F \\xC3\\xAF,\\xC3\\x96 \\xC3\\xB6,\\xC3\\x9C \\xC3\\xBC,\\xE1\\xBA\\x84 \\xE1\\xBA\\x85,\\xC5\\xB8 \\xC3\\xBF,\\xD0\\x81 \\xD1\\x91,\\xD0\\x87 \\xD1\\x97,\\xD3\\x9C \\xD3\\x9D",
                "German and umlauts:ẞ ß,Ä ä,Ë ë,Ï ï,Ö ö,Ü ü,Ẅ ẅ,Ÿ ÿ,Ё ё,Ї ї,Ӝ ӝ"
        }, {
                "Wrongly escaped chars are untouched: \\xGH \\xa",
                "Wrongly escaped chars are untouched: \\xGH \\xa"
        }};
    }

    public static String[][] allSmartEscapingTestCases() {
        return new String[][]{{
                "CN=names that do not need escaping stay untouched,C=PL",
                "CN=names that do not need escaping stay untouched,C=PL"
        }, {
                "C=PL,O=subject org,CN=Not needed to be escaped, emailAddress=test.mail@why.mail.com",
                "C=PL,O=subject org,CN=Not needed to be escaped, emailAddress=test.mail@why.mail.com"
        }, {
                "CN=comma needs to be, ESC,C=PL",
                "CN=comma needs to be\\, ESC,C=PL"
        }, {
                "CN=comma needs , to be escaped,C=PL",
                "CN=comma needs \\, to be escaped,C=PL"
        }, {
                "CN=already escaped\\, are not escaped again,C=PL",
                "CN=already escaped\\, are not escaped again,C=PL",
        }, {
                "CN=comma needz to be escaped even at the end but not the last one,,C=PL",
                "CN=comma needz to be escaped even at the end but not the last one\\,,C=PL",
        }, {
                "CN=allRDNs,,OU=,,O=,,L=,,ST=,,C=,,E=,",
                "CN=allRDNs\\,,OU=\\,,O=\\,,L=\\,,ST=\\,,C=\\,,E=\\,"
        }, {
                " CN= trailing and leading spaces of RDN values are ignored by RFC2253 , C= PL ",
                " CN=trailing and leading spaces of RDN values are ignored by RFC2253, C=PL",
        }, {
                "CN=slash/backslash\\quote\"colon:_rfc2253special_ampersand&equals=comma,plus+lessthan<greaterthan>hash#semicolon;end,C=PL",
                "CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&equals\\=comma\\,plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,C=PL"
        }, {
                "CN=slash/backslash\\quote\"colon:_rfc2253special_ampersand&comma,equals=plus+lessthan<greaterthan>hash#semicolon;end,C=PL",
                "CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,C=PL"
        }};
    }

    public static String[][] allRemoveUnwantedRdnPostfixCases() {
        return new String[][]{{
                "CN=Common name/emailAddress\\=a@b.pl,C=PL",
                "CN=Common name,C=PL"
        }, {
                "CN=Common name/emailAddress=a@b.pl,C=PL",
                "CN=Common name,C=PL"
        }, {
                "CN=Common name/title=Mr,O=Gutek corp.,C=PL",
                "CN=Common name,O=Gutek corp.,C=PL"
        }};
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("allUtfHexDecoderTestCases")
    public void utfBinaryHexDecodeTest(String codedInput, String expectedOutput) {
        //when
        String decoded = DistinguishedNamesCodingUtil.decodeUtfHexLiterals(codedInput);

        //then
        assertEquals(expectedOutput, decoded);
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("allSmartEscapingTestCases")
    public void smartDistinguishedNameEscapeTest(String unescapedInput, String expectedEscapedOutput) {
        //when
        String escaped = DistinguishedNamesCodingUtil.smartEscapeDistinguishedName(unescapedInput);

        //then
        assertEquals(expectedEscapedOutput, escaped);
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("allRemoveUnwantedRdnPostfixCases")
    public void removeUnwantedRdnPostfixAttrTest(String dirtyInput, String expectedOutput) {
        //when
        String clearDn = DistinguishedNamesCodingUtil.removeUnwantedRdnPostfixAttr(dirtyInput);

        //then
        assertEquals(expectedOutput, clearDn);
    }
}
