package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class HttpURLFetcherTest {
    ProxyDataCallback proxyDataMock = Mockito.mock(ProxyDataCallback.class);
    HttpURLFetcher testInstance = Mockito.spy(new HttpURLFetcher(proxyDataMock));

    @Test
    public void testDoesTargetMatchNonProxyLocalhostTrue() throws MalformedURLException {
        String crlURL = "http://localhost/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1");
        assertTrue(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyDomainWithPortTrue() throws MalformedURLException {
        String crlURL = "https://test.ec.europa.eu:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|test.ec.europa.eu");
        assertTrue(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyDomainWithPortAndAsterixTrue() throws MalformedURLException {
        String crlURL = "https://test.ec.europa.eu:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|*.ec.europa.eu");
        assertTrue(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyDomainWithPortFalse() throws MalformedURLException {
        String crlURL = "https://test.ec.europa.eu:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|ec.test.eu");
        assertFalse(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyIPWithPortFalse() throws MalformedURLException {
        String crlURL = "https://192.168.5.4:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|ec.test.eu");
        assertFalse(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyIPWithPortTrue() throws MalformedURLException {
        String crlURL = "https://192.168.5.4:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|192.168.5.4|ec.test.eu");
        assertTrue(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyIPMaskWithPortTrue() throws MalformedURLException {
        String crlURL = "https://192.168.5.4:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|192.168.*|ec.test.eu");
        assertTrue(val);
    }

    @Test
    public void testDoesTargetMatchNonProxyIPMaskWithPortFalse() throws MalformedURLException {
        String crlURL = "https://192.168.5.4:8443/url";
        URL targetUrl = new URL(crlURL);
        boolean val = testInstance.doesTargetMatchNonProxy(targetUrl.getHost(), "localhost|127.0.0.1|192.168.4.*|ec.test.eu");
        assertFalse(val);
    }

    @Test
    public void testFetchURLNullUrl() throws IOException {
        // given when
        InputStream inputStream = testInstance.fetchURL(null);
        // then
        assertNull(inputStream);
    }

    @Test
    public void testFetchURLProtocol() throws IOException {
        // given
        String testUrl = "http://test-url.log.eu";
        InputStream testIS = new ByteArrayInputStream(testUrl.getBytes());
        Mockito.doReturn(false).when(proxyDataMock).isProxyEnabled();
        Mockito.doReturn(Arrays.asList("http", "https")).when(proxyDataMock).getAllowedURLProtocols();
        Mockito.doReturn(testIS).when(testInstance).downloadURLDirect(testUrl, null, null);

        InputStream inputStream = testInstance.fetchURL(testUrl);
        // then
        assertNotNull(inputStream);
        assertEquals(testIS, inputStream);
    }

    @Test
    public void testFetchURLProtocolNotDefined() throws IOException {
        // given
        String testUrl = "http://test-url.log.eu";
        InputStream testIS = new ByteArrayInputStream(testUrl.getBytes());
        Mockito.doReturn(false).when(proxyDataMock).isProxyEnabled();
        Mockito.doReturn(null).when(proxyDataMock).getAllowedURLProtocols();
        Mockito.doReturn(testIS).when(testInstance).downloadURLDirect(testUrl, null, null);

        InputStream inputStream = testInstance.fetchURL(testUrl);
        // then
        assertNotNull(inputStream);
        assertEquals(testIS, inputStream);
    }

    @Test
    public void testFetchURLProtocolUseProxy() throws IOException {
        // given
        String testUrl = "http://test-url.log.eu";
        InputStream testIS = new ByteArrayInputStream(testUrl.getBytes());
        Mockito.doReturn(true).when(proxyDataMock).isProxyEnabled();
        Mockito.doReturn("localhost").when(proxyDataMock).getHttpNoProxyHosts();
        Mockito.doReturn("localhost").when(proxyDataMock).getHttpProxyHost();
        Mockito.doReturn(1234).when(proxyDataMock).getHttpProxyPort();
        Mockito.doReturn("UserName").when(proxyDataMock).getUsername();
        Mockito.doReturn("Pass").when(proxyDataMock).getSecurityToken();
        Mockito.doReturn(Arrays.asList("http", "https")).when(proxyDataMock).getAllowedURLProtocols();
        Mockito.doReturn(testIS).when(testInstance).downloadURLViaProxy(testUrl, null, null,
                "localhost",
                1234, "UserName", "Pass");

        InputStream inputStream = testInstance.fetchURL(testUrl);
        // then
        assertNotNull(inputStream);
        assertEquals(testIS, inputStream);
    }

    @Test
    @Disabled
    public void downloadURLViaProxy() throws IOException {
        String testUrl = "http://test-url.log.eu";
        InputStream responseIS = Mockito.mock(InputStream.class);
        InputStream requestIS = Mockito.mock(InputStream.class);
        Mockito.doReturn(responseIS).when(testInstance).execute(ArgumentMatchers.any(), ArgumentMatchers.any());

        InputStream result = testInstance.downloadURLViaProxy(testUrl, null, requestIS, "localhost", 8888, "user", "password");
        assertEquals(responseIS, result);
    }


    @Test
    public void testFetchURLInvalidProtocol() throws IOException {
        // given
        String testUrl = "wrong://test-url.log.eu";
        Mockito.doReturn(false).when(proxyDataMock).isProxyEnabled();
        Mockito.doReturn(Arrays.asList("http", "https")).when(proxyDataMock).getAllowedURLProtocols();

        InputStream inputStream = testInstance.fetchURL(testUrl);
        // then
        assertNull(inputStream);
    }

    @Test
    public void testIsValidParameterNull() {
        assertFalse(testInstance.isValidParameter(null));
    }

    @Test
    public void testIsValidParameterEmpty() {
        assertFalse(testInstance.isValidParameter(""));
    }

    @Test
    public void testIsValidParameterOneIsEmpty() {
        assertFalse(testInstance.isValidParameter("Something", ""));
    }

    @Test
    public void testIsValidParameterOK() {
        assertTrue(testInstance.isValidParameter("Something"));
    }

    @Test
    public void testIsValidParameterMultipleOK() {
        assertTrue(testInstance.isValidParameter("Something", "SomethingElse"));
    }

}
