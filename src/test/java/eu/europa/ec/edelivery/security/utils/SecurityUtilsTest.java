package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.exception.SecRuntimeException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SecurityUtilsTest {

    @BeforeAll
    public static void setup() {
        Security.insertProviderAt(new org.bouncycastle.jce.provider.BouncyCastleProvider(), 1);
    }


    @Test
    public void testReadSecret() {

        File exampleKey = getExampleKey();
        // when
        SecurityUtils.Secret result = SecurityUtils.readSecret(exampleKey);
        //then
        assertNotNull(result);
    }

    @Test
    public void testReadSecretError() {
        File exampleKey = Paths.get("target","notExists.test").toFile();
        // when
        SecRuntimeException result =  assertThrows(SecRuntimeException.class,()->SecurityUtils.readSecret(exampleKey));
        //then
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("notExists.test"));
    }

    @Test
    public void testGenerateAuthenticationToken() {
        File exampleKey = getExampleKey();
        // when
        SecurityUtils.Secret token = SecurityUtils.readSecret(exampleKey);
        //then
        assertNotNull(token);
    }


    @Test
    public void testGenerateAuthenticationTokenIdentifier() {

        // when
        String encPassword = SecurityUtils.generateAuthenticationTokenIdentifier(true);
        //then
        assertNotNull(encPassword);
        assertEquals(8, encPassword.length());
    }

    @Test
    public void testEncryptNotWrappedToken() {
        File exampleKey = getExampleKey();
        String tokenExample = "NotWrapped!";

        // when
        String encPassword = SecurityUtils.encryptWrappedToken(exampleKey, tokenExample);
        //then
        assertNotNull(encPassword);
        assertEquals(tokenExample, encPassword);
    }

    @Test
    public void testEncryptWrappedToken() {
        File exampleKey = getExampleKey();
        String tokenExample = "{DEC}{Wrapped!}";

        // when
        String encPassword = SecurityUtils.encryptWrappedToken(exampleKey, tokenExample);
        //then
        assertNotNull(encPassword);
        assertNotEquals(tokenExample, encPassword);
    }

    @Test
    public void testGenerateMasterKeyWitIV() throws Exception {
        String tempPrivateKey = System.currentTimeMillis() + ".private";
        Path resourcePath = Paths.get("target", tempPrivateKey);

        SecurityUtils.generatePrivateSymmetricKey(resourcePath.toFile(), true);

        byte[] buff = Files.readAllBytes(resourcePath);
        assertTrue(buff.length > SecurityUtils.IV_GCM_SIZE);
        // start tag
        assertEquals('#', buff[0]);
        // end IV tag
        assertEquals('#', buff[SecurityUtils.IV_GCM_SIZE + 1]);
        byte[] keyBytes = Arrays.copyOfRange(buff, SecurityUtils.IV_GCM_SIZE + 2, buff.length);


        SecretKey privateKey = new SecretKeySpec(keyBytes, "AES");
        assertNotNull(privateKey);
    }

    @Test
    public void encryptDefault() throws IOException {
        // given
        File f = generateRandomPrivateKey();
        String password = "TEST11002password1@!." + System.currentTimeMillis();

        // when
        String encPassword = SecurityUtils.encrypt(f, password);
        //then
        assertNotNull(encPassword);
        assertNotEquals(password, encPassword);
    }

    @Test
    public void encryptWithSetupKeyWithoutIV() {
        // given
        Path resourceFile = Paths.get("src", "test", "resources", "keystores", "encryptionKey.key");
        String password = "test123";

        // when
        String encPassword = SecurityUtils.encrypt(resourceFile.toFile(), password);
        String decPassword = SecurityUtils.decrypt(resourceFile.toFile(), encPassword);
        //then
        assertNotNull(encPassword);
        assertNotEquals(password, encPassword);
        assertEquals(password, decPassword);
    }

    @Test
    public void encryptWithSetupKeyWitIV() {
        // given
        Path resourceFile = Paths.get("src", "test", "resources", "keystores", "masterKeyWithIV.key");
        String password = "test123";

        // when
        String encPassword = SecurityUtils.encrypt(resourceFile.toFile(), password);
        String decPassword = SecurityUtils.decrypt(resourceFile.toFile(), encPassword);
        //then
        assertNotNull(encPassword);
        assertNotEquals(password, encPassword);
        assertEquals(password, decPassword);
    }


    @Test
    public void decryptDefault() throws IOException {
        // given
        File f = generateRandomPrivateKey();
        String password = "TEST11002password1@!." + System.currentTimeMillis();
        String encPassword = SecurityUtils.encrypt(f, password);

        // when
        String decPassword = SecurityUtils.decrypt(f, encPassword);
        //then
        assertNotNull(decPassword);
        assertEquals(password, decPassword);
    }

    public static File generateRandomPrivateKey() throws IOException {
        File resource = File.createTempFile("test-key", ".key");
        resource.deleteOnExit();
        SecurityUtils.generatePrivateSymmetricKey(resource, true);
        return resource;
    }

    public static File getExampleKey() {
        return Paths.get("src", "test", "resources", "keystores", "masterKeyWithIV.key").toFile();
    }
}
