/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import eu.europa.ec.edelivery.exception.ClientCertParseException;
import eu.europa.ec.edelivery.security.utils.CertificateKeyType;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.operator.OperatorCreationException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import jakarta.servlet.ServletException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Method tests {@link ClientCertAuthenticationFilter} class.
 *
 * @author Pawel GUTOWSKI
 * @author Joze RIHTARSIC
 * @since 1.0
 */
public class ClientCertAuthenticationFilterTest {

    @SuppressWarnings("unused")
    private static Object[] positiveTestCases() {
        return new Object[][]{
                {
                        "regular positive scenario",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "regular positive scenario with changed values",
                        "sno=e6:77&subject=C=PL,O=subject org,CN=subject common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=FR,O=org,CN=common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=PL,O=subject org,CN=subject common name",
                        "CN=subject common name,O=subject org,C=PL",
                        "CN=common name,O=org,C=FR",
                        "000000000000e677",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },

                {
                        "regular positive scenario with email",
                        "sno=e6:77&subject=C=PL,O=subject org,CN=subject common name, emailAddress=test.mail@why.mail.com&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=FR,O=org,CN=common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=PL,O=subject org,CN=subject common name, emailAddress=test.mail@why.mail.com",
                        "CN=subject common name,O=subject org,C=PL",
                        "CN=common name,O=org,C=FR",
                        "000000000000e677",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "regular positive scenario with serial number",
                        "sno=e6:77&subject=C=PL,O=subject org,CN=subject common name, serialNumber=12345&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=FR,O=org,CN=common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=PL,O=subject org,CN=subject common name, serialNumber=12345",
                        "CN=subject common name,O=subject org,C=PL",
                        "CN=common name,O=org,C=FR",
                        "000000000000e677",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },

                {
                        "regular positive scenario with email and serial number",
                        "sno=e6:77&subject=C=PL,O=subject org,CN=subject common name, serialNumber=12345,emailAddress=test.mail@why.mail.com&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=FR,O=org,CN=common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=PL,O=subject org,CN=subject common name, serialNumber=12345,emailAddress=test.mail@why.mail.com",
                        "CN=subject common name,O=subject org,C=PL",
                        "CN=common name,O=org,C=FR",
                        "000000000000e677",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "HTML ampersands are unescaped correctly",
                        "sno=e6:66&subject=C=BE,O=ampersand%26org,CN=Gutek+%26+Gutek&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,CN=amp+%26+issuer&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=ampersand&org,CN=Gutek & Gutek",
                        "CN=Gutek & Gutek,O=ampersand&org,C=BE",
                        "CN=amp & issuer,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "unescaping ampersand - real sample, covers also second serial number format...",
                        "sno=4109+%280x100d%29&subject=C%3DBE%2C+ST%3DBelgium%2C+L%3DBrussels%2C+O%3DAC+GOLDMAN+SOLUTIONS+%26+SERVICES+LTD%2C+OU%3DAC+GOLDMAN+SOLUTIONS+%26+SERVICES+LTD%2C+CN%3DSMP5Connectivity_test%2FemailAddress%3Damar.deep%40ext.ec.europa.eu&validfrom=Oct++2+14%3A53%3A58+2017+GMT&validto=Jan++8+14%3A53%3A58+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu",
                        "C=BE, ST=Belgium, L=Brussels, O=AC GOLDMAN SOLUTIONS & SERVICES LTD, OU=AC GOLDMAN SOLUTIONS & SERVICES LTD, CN=SMP5Connectivity_test",
                        "CN=SMP5Connectivity_test,O=AC GOLDMAN SOLUTIONS & SERVICES LTD,C=BE",
                        "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                        "000000000000100d",
                        Collections.emptyList()
                },
                {
                        "comma character is escaped correctly",
                        "sno=e6:66&subject=C=BE%2C+O=org%2C+CN=beforecomma%2Caftercomma&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE%2C+O=issuer org%2C+CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE, O=org, CN=beforecomma\\,aftercomma",
                        "CN=beforecomma\\,aftercomma,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "rfc2253 special characters and few others are escaped correctly - real sample",
                        "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CxE1%5CxBA%5Cx9E%5CxC3%5Cx9F%5CxC3%5Cx84%5CxC3%5CxA4PL%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87NO%5CxC3%5Cx86%5CxC3%5CxA6%5CxC3%5Cx98%5CxC3%5CxB8%5CxC3%5Cx85%5CxC3%5CxA5%2C+CN%3Dslash%2Fbackslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu",
                        "C=PL, O=DEẞßÄäPLżółćNOÆæØøÅå, CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end",
                        "CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL",
                        "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                        "0000000000001010",
                        Collections.emptyList()
                },
                {
                        "rfc2253 special characters and few others are escaped correctly - real sample without x",
                        "sno=4112+%280x1010%29&subject=C%3DPL%2C+O%3DDE%5CE1%5CBA%5C9E%5CC3%5C9F%5CC3%5C84%5CC3%5CA4PL%5CC5%5CBC%5CC3%5CB3%5CC5%5C82%5CC4%5C87NO%5CC3%5C86%5CC3%5CA6%5CC3%5C98%5CC3%5CB8%5CC3%5C85%5CC3%5CA5%2C+CN%3Dslash%2Fbackslash%5Cquote%22colon%3A_rfc2253special_ampersand%26comma%2Cequals%3Dplus%2Blessthan%3Cgreaterthan%3Ehash%23semicolon%3Bend&validfrom=Oct+12+12%3A57%3A42+2017+GMT&validto=Jan+18+12%3A57%3A42+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu",
                        "C=PL, O=DEẞßÄäPLżółćNOÆæØøÅå, CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end",
                        "CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL",
                        "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                        "0000000000001010",
                        Collections.emptyList()
                },
                {
                        "UTF-8 simple characters are unescaped correctly",
                        "sno=100f&subject=CN%3Dtest%2C+C%3DPL%2C+O%3DZa%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87&validfrom=Oct+10+09%3A14%3A32+2017+GMT&validto=Jan+16+09%3A14%3A32+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu",
                        "CN=test, C=PL, O=Zażółć",
                        "CN=test,O=Zazołc,C=PL",
                        "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                        "000000000000100f",
                        Collections.emptyList()
                },
                {
                        "UTF-8 characters are unescaped correctly - real sample",
                        "sno=4111+%280x100f%29&subject=C%3DPL%2C+O%3DPL%3AZa%5CxC5%5CxBC%5CxC3%5CxB3%5CxC5%5Cx82%5CxC4%5Cx87+g%5CxC4%5Cx99%5CxC5%5Cx9Bl%5CxC4%5Cx85+ja%5CxC5%5CxBA%5CxC5%5Cx84%2FNO%2FSE%3A%5CxC3%5Cx86+%5CxC3%5CxA6%2C%5CxC3%5Cx98+%5CxC3%5CxB8%2C%5CxC3%5Cx85+%5CxC3%5CxA5%2C+CN%3DDE%7Cumlauts%3A%5CxE1%5CxBA%5Cx9E+%5CxC3%5Cx9F%2C%5CxC3%5Cx84+%5CxC3%5CxA4%2C%5CxC3%5Cx8B+%5CxC3%5CxAB%2C%5CxC3%5Cx8F+%5CxC3%5CxAF%2C%5CxC3%5Cx96+%5CxC3%5CxB6%2C%5CxC3%5Cx9C+%5CxC3%5CxBC%2C%5CxE1%5CxBA%5Cx84+%5CxE1%5CxBA%5Cx85%2C%5CxC5%5CxB8+%5CxC3%5CxBF%2C%5CxD0%5Cx81+%5CxD1%5Cx91%2C%5CxD0%5Cx87+%5CxD1%5Cx97%2C%5CxD3%5Cx9C+%5CxD3%5Cx9D%2FemailAddress%3Dpawel.gutowski%40gmail.com&validfrom=Oct+10+09%3A14%3A32+2017+GMT&validto=Jan+16+09%3A14%3A32+2028+GMT&issuer=C%3DBE%2C+ST%3DBelgium%2C+O%3DConnectivity+Test%2C+OU%3DConnecting+Europe+Facility%2C+CN%3DConnectivity+Test+Component+CA%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu",
                        "C=PL, O=PL:Zażółć gęślą jaźń/NO/SE:Æ æ\\,Ø ø\\,Å å, CN=DE|umlauts:ẞ ß\\,Ä ä\\,Ë ë\\,Ï ï\\,Ö ö\\,Ü ü\\,Ẅ ẅ\\,Ÿ ÿ\\,Ё ё\\,Ї ї\\,Ӝ ӝ",
                        "CN=DE|umlauts:ẞ ß\\,A a\\,E e\\,I i\\,O o\\,U u\\,W w\\,Y y\\,Е е\\,І і\\,Ж ж,O=PL:Zazołc gesla jazn/NO/SE:Æ æ\\,Ø ø\\,A a,C=PL",
                        "CN=Connectivity Test Component CA,OU=Connecting Europe Facility,O=Connectivity Test,ST=Belgium,C=BE",
                        "000000000000100f",
                        Collections.emptyList()
                },
                {
                        "fields order does not matter and non-used fields are skipped",
                        "validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,CN=issuer common name&subject=C=BE,O=org,CN=common name&sno=e6:66",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.emptyList()
                },
                {
                        "serial number can be represented by different key ('serial' instead of 'sno')",
                        "serial=e6:66&subject=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "keys are case insensitive",
                        "Sno=e6:66&suBJEct=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&ISSuer=C=DE,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "garbage is skipped - emailAddress added to the end of CN",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name%2FemailAddress%3DCEF-EDELIVERY-SUPPORT@ec.europa.eu&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE%2FemailAddress%3Dissuer@ec.europa.eu,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "garbage is skipped - emailAddress added to the end of CN",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name%2FemailAddress%3Damar.deep%40ext.ec.europa.eu&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE%2FemailAddress%3Dissuer%40ec.europa.eu,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "garbage is skipped - emailAddress added to the end of CN, but not at the end of subject",
                        "sno=f7:1e:e8:b1:1c:b3:b7:87&subject=C=BE%2FemailAddress%3Dpawel.gutowski%40gmail.com, O=European Commission, OU=CEF_eDelivery.europa.eu,+OU=eHealth,+OU=SMP_TEST,+CN=EHEALTH_SMP_EC%2FemailAddress%3Dpawel.gutowski%40gmail.com&validfrom=Dec++6+17:41:42+2016+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,+O=T-Systems+International+GmbH,+OU=T-Systems+Trust+Center,+ST=Nordrhein+Westfalen/postalCode=57250,+L=Netphen/street=Untere+Industriestr.+20,+CN=Shared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE, O=European Commission, OU=CEF_eDelivery.europa.eu, OU=eHealth, OU=SMP_TEST, CN=EHEALTH_SMP_EC",
                        "CN=EHEALTH_SMP_EC,O=European Commission,C=BE",
                        "CN=Shared Business CA 4,OU=T-Systems Trust Center,O=T-Systems International GmbH,L=Netphen,ST=Nordrhein Westfalen,C=DE",
                        "f71ee8b11cb3b787",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "garbage is skipped - title added to the end of CN",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name%2Ftitle%3DMr&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE/emailAddress\\=issuer%3Dec.europa.eu,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "rare RDNs are handled correctly",
                        "sno=e6:66&subject=C=BE, O=Gutek Corp, CN=Gutek, GN=repondant, SN=ncp&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,STREET=3go Maja,CN=issuer common name",
                        "C=BE, O=Gutek Corp, CN=Gutek, SN=ncp", //  can not create  certificate with , GN=repondant
                        "CN=Gutek,O=Gutek Corp,C=BE",
                        "CN=issuer common name,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.emptyList()
                },
                {
                        "RDNs with email- upper case",
                        "subject=L=Brussels,O=DIGIT,EMAILADDRESS=receiver@test.be, CN=SMP_receiverCN, OU=B4, ST=BE, C=BE&sno=0001&validto=Jun 1 10:37:53 2035 CEST&issuer=O=DIGIT,CN=rootCN,EMAILADDRESS=root@test.be,OU=B4,ST=BE,C=BE,L=Brussels&validfrom=Jun 1 10:37:53 2015 CEST",
                        "L=Brussels,O=DIGIT,EMAILADDRESS=receiver@test.be, CN=SMP_receiverCN, OU=B4, ST=BE, C=BE",
                        "CN=SMP_receiverCN,O=DIGIT,C=BE",
                        "CN=rootCN,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE",
                        "0000000000000001",
                        Collections.emptyList()
                },
                {
                        "positive scenario with double OU in issuer",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,OU=CEF_eDelivery.testa.eu, OU=eHealth,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,OU=CEF_eDelivery.testa.eu,OU=eHealth,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "positive scenario with double OU in issuer - revers order",
                        "sno=e6:66&subject=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org, OU=eHealth,OU=CEF_eDelivery.testa.eu,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "C=BE,O=org,CN=common name",
                        "CN=common name,O=org,C=BE",
                        "CN=issuer common name,OU=CEF_eDelivery.testa.eu,OU=eHealth,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "positive scenario with email and serial number escaped by RP",
                        "sno=e6:66&subject=CN=GENERALERDStest_SMP_TEST_setcce/emailAddress\\=CEF-EDELIVERY-SUPPORT@ec.europa.eu/serialNumber\\=1,O=European Commission,C=SI&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org, OU=eHealth,OU=CEF_eDelivery.testa.eu,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu, serialNumber=1, CN=GENERALERDStest_SMP_TEST_setcce,O=European Commission,C=SI",
                        "CN=GENERALERDStest_SMP_TEST_setcce,O=European Commission,C=SI",
                        "CN=issuer common name,OU=CEF_eDelivery.testa.eu,OU=eHealth,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "positive scenario with email and serial number without escaped =",
                        "sno=e6:66&subject=CN=GENERALERDStest_SMP_TEST_setcce/emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu/serialNumber=1,O=European Commission,C=SI&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org, OU=eHealth,OU=CEF_eDelivery.testa.eu,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu, serialNumber=1, CN=GENERALERDStest_SMP_TEST_setcce,O=European Commission,C=SI",
                        "CN=GENERALERDStest_SMP_TEST_setcce,O=European Commission,C=SI",
                        "CN=issuer common name,OU=CEF_eDelivery.testa.eu,OU=eHealth,O=issuer org,C=DE",
                        "000000000000e666",
                        Collections.singletonList("1.3.6.1.4.1.7879.13.25")
                },
                {
                        "positive scenario with multi value RDN",
                        //        "sno=5a:7a:8e:92&subject=C=DK, O=Digitaliseringsstyrelsen // CVR:34051178, serialNumber=CVR:34051178-FID:28818903 + CN=eDelivery SMP-FOCES (funktionscertifikat)&validfrom=Nov 22 08:47:09 2019 GMT&validto=Nov 22 08:44:01 2022 GMT&issuer=C=DK, O=TRUST2408, CN=TRUST2408 OCES CA III",
                        "sno=5a%3a7a%3a8e%3a92&subject=C%3dDK%2c%20O%3dDigitaliseringsstyrelsen%20%2f%2f%20CVR%3a34051178%2c%20serialNumber%3dCVR%3a34051178-FID%3a28818903%20%2b%20CN%3deDelivery%20SMP-FOCES%20(funktionscertifikat)&validfrom=Nov%2022%2008%3a47%3a09%202019%20GMT&validto=Nov%2022%2008%3a44%3a01%202022%20GMT&issuer=C%3dDK%2c%20O%3dTRUST2408%2c%20CN%3dTRUST2408%20OCES%20CA%20III",
                        "C=DK, O=Digitaliseringsstyrelsen // CVR:34051178, serialNumber=CVR:34051178-FID:28818903+ CN=eDelivery SMP-FOCES (funktionscertifikat)",
                        "CN=eDelivery SMP-FOCES (funktionscertifikat),O=Digitaliseringsstyrelsen // CVR:34051178,C=DK",
                        "CN=TRUST2408 OCES CA III,O=TRUST2408,C=DK",
                        "000000005a7a8e92",
                        Collections.emptyList()
                },
                {
                        "Large serial number with odd number formatted",
                        "sno=0a%3a33%3ae3%3a0c%3ad2%3a50%3ab1%3a72%3a67%3ab1%3a3b%3aec&subject=C%3dES%2c%20ST%3dMadrid%2c%20L%3dMadrid%2c%20O%3dMinisterio%20de%20Something%2c%20CN%3dncp-ppt.test.ehealth&validfrom=May%2018%2010%3a46%3a02%202032%20GMT&validto=May%2010%2015%3a46%3a02%202035%20GMT&issuer=C%3dBE%2c%20O%3dGlobalSign%20nv-sa%2c%20CN%3dGlobalSign%20Non-Public%20SHA256%20CA%20-%20G3",
                        "C=ES, ST=Madrid, L=Madrid, O=Ministerio de Something, CN=ncp-ppt.test.ehealth",
                        "CN=ncp-ppt.test.ehealth,O=Ministerio de Something,C=ES",
                        "CN=GlobalSign Non-Public SHA256 CA - G3,O=GlobalSign nv-sa,C=BE",
                        "a33e30cd250b17267b13bec",
                        Collections.emptyList()
                },

                {
                        "Large serial number with odd number not formatted and padded",
                        "sno=a33e30cd250b17267b13bec&subject=C%3dES%2c%20ST%3dMadrid%2c%20L%3dMadrid%2c%20O%3dMinisterio%20de%20Something%2c%20CN%3dncp-ppt.test.ehealth&validfrom=May%2018%2010%3a46%3a02%202032%20GMT&validto=May%2010%2015%3a46%3a02%202035%20GMT&issuer=C%3dBE%2c%20O%3dGlobalSign%20nv-sa%2c%20CN%3dGlobalSign%20Non-Public%20SHA256%20CA%20-%20G3",
                        "C=ES, ST=Madrid, L=Madrid, O=Ministerio de Something, CN=ncp-ppt.test.ehealth",
                        "CN=ncp-ppt.test.ehealth,O=Ministerio de Something,C=ES",
                        "CN=GlobalSign Non-Public SHA256 CA - G3,O=GlobalSign nv-sa,C=BE",
                        "a33e30cd250b17267b13bec",
                        Collections.emptyList()
                },
                {
                        "Test with multiple certificate policies",
                        "sno=2712&subject=C=EU,O=digit,CN=Leaf&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2025+GMT&issuer=C=EU,O=digit,CN=Issuer&policy_oids=0.4.0.194112.1.0,0.4.0.194112.4.0",
                        "C=EU,O=digit,CN=Leaf",
                        "CN=Leaf,O=digit,C=EU",
                        "CN=Issuer,O=digit,C=EU",
                        "0000000000002712",
                        Arrays.asList("0.4.0.194112.1.0", "0.4.0.194112.4.0")
                },

        };
    }

    @SuppressWarnings("unused")
    private static Object[] newAndOldReverseProxyTestCases() {
        return new Object[][]{
                {
                        "EHEALTH with email and serialNumber",
                        "sno=93%3ae4%3a2c%3aa6%3a45%3a05%3abb%3ac9&subject=C%3dBE%2c%20O%3dEuropean%20Commission%2c%20OU%3dCEF_eDelivery.europa.eu%2c%20OU%3dtestabc%2c%20OU%3dSML%2c%20CN%3dGRP%3atest_proxy_01%2c%20emailAddress%3dCEF-EDELIVERY-SUPPORT%40ec.europa.eu%2c%20serialNumber%3d1&validfrom=Jan%2018%2015%3a03%3a02%202019%20GMT&validto=Jan%2018%2023%3a59%3a59%202021%20GMT&issuer=C%3dDE%2c%20O%3dT-Systems%20International%20GmbH%2c%20OU%3dT-Systems%20Trust%20Center%2c%20CN%3dTeleSec%20Business%20CA%201",
                        "sno=93%3Ae4%3A2c%3Aa6%3A45%3A05%3Abb%3Ac9&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3Dtestabc%2C+OU%3DSML%2C+CN%3DGRP%3Atest_proxy_01%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu%2FserialNumber%3D1&validfrom=Jan+18+15%3A03%3A02+2019+GMT&validto=Jan+18+23%3A59%3A59+2021+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+CN%3DTeleSec+Business+CA+1&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "CN=GRP:test_proxy_01,OU=CEF_eDelivery.europa.eu,OU=SML,OU=testabc,O=European Commission,C=BE",
                        "CN=GRP:test_proxy_01,O=European Commission,C=BE",
                        "CN=TeleSec Business CA 1,OU=T-Systems Trust Center,O=T-Systems International GmbH,C=DE",
                        "93e42ca64505bbc9",

                },
                {
                        "GPR NEW F5",
                        "sno=3f%3af5%3a03%3ac4%3af3%3a6d%3ad7%3a6a&subject=C%3dBE%2c%20O%3dEuropean%20Commission%2c%20OU%3dCEF_eDelivery.europa.eu%2c%20OU%3dtestabc%2c%20OU%3dSMP%2c%20CN%3d%22GRP%3aTEST_%2b%2c%26%20%3d%5cC3%5cA9%5cC3%5cA1%5cC5%5cB1!%22%2c%20emailAddress%3dCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Jan%2010%2016%3a24%3a27%202019%20GMT&validto=Jan%2010%2023%3a59%3a59%202021%20GMT&issuer=C%3dDE%2c%20O%3dT-Systems%20International%20GmbH%2c%20OU%3dT-Systems%20Trust%20Center%2c%20CN%3dTeleSec%20Business%20CA%201",
                        "sno=3f%3Af5%3A03%3Ac4%3Af3%3A6d%3Ad7%3A6a&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3Dtestabc%2C+OU%3DSMP%2C+CN%3DGRP%3ATEST_%2B%2C%26+%3D%5CxC3%5CxA9%5CxC3%5CxA1%5CxC5%5CxB1%21%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Jan+10+16%3A24%3A27+2019+GMT&validto=Jan+10+23%3A59%3A59+2021+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+CN%3DTeleSec+Business+CA+1&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "CN=GRP:TEST_\\+\\,& \\=eau!,OU=CEF_eDelivery.europa.eu,OU=SMP,OU=testabc,O=European Commission,C=BE",
                        "CN=GRP:TEST_\\+\\,& \\=eau!,O=European Commission,C=BE",
                        "CN=TeleSec Business CA 1,OU=T-Systems Trust Center,O=T-Systems International GmbH,C=DE",
                        "3ff503c4f36dd76a",
                },
                {
                        "ehealth - NEW F5 ",
                        "sno=f7%3a1e%3ae8%3ab1%3a1c%3ab3%3ab7%3a87&subject=C%3dBE%2c%20O%3dEuropean%20Commission%2c%20OU%3dCEF_eDelivery.europa.eu%2c%20OU%3deHealth%2c%20OU%3dSMP_TEST%2c%20CN%3dEHEALTH_SMP_EC%2c%20emailAddress%3dCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Dec%20%206%2017%3a41%3a42%202016%20GMT&validto=Jul%20%209%2023%3a59%3a00%202019%20GMT&issuer=C%3dDE%2c%20O%3dT-Systems%20International%20GmbH%2c%20OU%3dT-Systems%20Trust%20Center%2c%20ST%3dNordrhein%20Westfalen%2c%20postalCode%3d57250%2c%20L%3dNetphen%2c%20street%3dUntere%20Industriestr.%2020%2c%20CN%3dShared%20Business%20CA%204",
                        "sno=f7%3A1e%3Ae8%3Ab1%3A1c%3Ab3%3Ab7%3A87&subject=C%3DBE%2C+O%3DEuropean+Commission%2C+OU%3DCEF_eDelivery.europa.eu%2C+OU%3DeHealth%2C+OU%3DSMP_TEST%2C+CN%3DEHEALTH_SMP_EC%2FemailAddress%3DCEF-EDELIVERY-SUPPORT%40ec.europa.eu&validfrom=Dec++6+17%3A41%3A42+2016+GMT&validto=Jul++9+23%3A59%3A00+2019+GMT&issuer=C%3DDE%2C+O%3DT-Systems+International+GmbH%2C+OU%3DT-Systems+Trust+Center%2C+ST%3DNordrhein+Westfalen%2FpostalCode%3D57250%2C+L%3DNetphen%2Fstreet%3DUntere+Industriestr.+20%2C+CN%3DShared+Business+CA+4&policy_oids=1.3.6.1.4.1.7879.13.25",
                        "CN=EHEALTH_SMP_EC,OU=CEF_eDelivery.europa.eu,OU=eHealth,OU=SMP_TEST,O=European Commission,C=BE",
                        "CN=EHEALTH_SMP_EC,O=European Commission,C=BE",
                        "CN=Shared Business CA 4,OU=T-Systems Trust Center,O=T-Systems International GmbH,L=Netphen,ST=Nordrhein Westfalen,C=DE",
                        "f71ee8b11cb3b787",
                },
                {
                        "PEPPOL",
                        "sno=0d%3ad0%3ad2%3af9%3a8c%3ac2%3a52%3a05%3abc%3a6c%3a85%3a4d%3a1c%3ad8%3a84%3a11&subject=CN%3dPOP000004%2c%20OU%3dPEPPOL%20TEST%20SMP%2c%20O%3dEuropean%20Commission%2c%20C%3dBE&validfrom=Apr%2017%2000%3a00%3a00%202018%20GMT&validto=Apr%20%206%2023%3a59%3a59%202020%20GMT&issuer=C%3dBE%2c%20O%3dOpenPEPPOL%20AISBL%2c%20OU%3dFOR%20TEST%20ONLY%2c%20CN%3dPEPPOL%20SERVICE%20METADATA%20PUBLISHER%20TEST%20CA%20-%20G2",
                        "sno=0d%3Ad0%3Ad2%3Af9%3A8c%3Ac2%3A52%3A05%3Abc%3A6c%3A85%3A4d%3A1c%3Ad8%3A84%3A11&subject=CN%3DPOP000004%2C+OU%3DPEPPOL+TEST+SMP%2C+O%3DEuropean+Commission%2C+C%3DBE&validfrom=Apr+17+00%3A00%3A00+2018+GMT&validto=Apr++6+23%3A59%3A59+2020+GMT&issuer=C%3DBE%2C+O%3DOpenPEPPOL+AISBL%2C+OU%3DFOR+TEST+ONLY%2C+CN%3DPEPPOL+SERVICE+METADATA+PUBLISHER+TEST+CA+-+G2",
                        "CN=POP000004,OU=PEPPOL TEST SMP,O=European Commission,C=BE",
                        "CN=POP000004,O=European Commission,C=BE",
                        "CN=PEPPOL SERVICE METADATA PUBLISHER TEST CA - G2,OU=FOR TEST ONLY,O=OpenPEPPOL AISBL,C=BE",
                        "0dd0d2f98cc25205bc6c854d1cd88411",
                },

        };
    }

    @SuppressWarnings("unused")
    private static Object[] negativeTestCases() {
        return new Object[][]{
                {
                        "malformed header value",
                        "this is malformed header value that results in authentication error"
                },
                {
                        "missing mandatory field serial number",
                        "subject%3DC%3DBE,O%3Dorg,CN%3Dcommon%20name%26validfrom%3DFeb++1+14%3A20%3A18+2017+GMT%26validto%3DJul++9+23%3A59%3A00+2019+GMT%26issuer%3DC%3DDE,O%3Dissuer%20org,CN%3Dissuer%20common%20name%26policy_oids%3D1.3.6.1.4.1.7879.13.25",
                },
                {
                        "missing mandatory field subject",
                        "sno%3De6%3A66%26validfrom%3DFeb++1+14%3A20%3A18+2017+GMT%26validto%3DJul++9+23%3A59%3A00+2019+GMT%26issuer%3DC%3DDE,O%3Dissuer%20org,CN%3Dissuer%20common%20name%26policy_oids%3D1.3.6.1.4.1.7879.13.25",
                },
                {
                        "missing mandatory field issuer",
                        "sno%3De6%3A66%26subject%3DC%3DBE,O%3Dorg,CN%3Dcommon%20name%26validfrom%3DFeb++1+14%3A20%3A18+2017+GMT%26validto%3DJul++9+23%3A59%3A00+2019+GMT%26policy_oids%3D1.3.6.1.4.1.7879.13.25",
                },
                {
                        "certificate not yet valid",
                        "sno%3De6%3A66%26subject%3DC%3DBE,O%3Dorg,CN%3Dcommon%20name%26validfrom%3DFeb++1+14%3A20%3A18+2050+GMT%26validto%3DJul++9+23%3A59%3A00+2019+GMT%26issuer%3DC%3DDE,O%3Dissuer%20org,CN%3Dissuer%20common%20name%26policy_oids%3D1.3.6.1.4.1.7879.13.25"
                },
                {
                        "certificate expired",
                        "sno%3De6%3A66%26subject%3DC%3DBE,O%3Dorg,CN%3Dcommon%20name%26validfrom%3DFeb++1+14%3A20%3A18+2050+GMT%26validto%3DJul++9+23%3A59%3A00+2016+GMT%26issuer%3DC%3DDE,O%3Dissuer%20org,CN%3Dissuer%20common%20name%26policy_oids%3D1.3.6.1.4.1.7879.13.25"
                }
        };
    }

    @AfterEach
    public void clearContext() {
        SecurityContextHolder.clearContext();
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("newAndOldReverseProxyTestCases")
    public void newAndOldReverseProxyExamples(String caseName, String newRPString, String oldRPString, String commonRDNSubject, String shortRDNSubject, String expIssuerDN, String expSerial)  {
        //given
        ClientCertAuthenticationFilter filter = new ClientCertAuthenticationFilter();
        filter.setClientCertAuthenticationEnabled(true);

        //when
        PreAuthenticatedCertificatePrincipal newRPPrincipal = filter.buildPrincipalFromHeader(newRPString);
        PreAuthenticatedCertificatePrincipal oldRPPrincipal = filter.buildPrincipalFromHeader(oldRPString);

        // then
        assertEquals(oldRPPrincipal.getSubjectCommonDN(), newRPPrincipal.getSubjectCommonDN());
        assertEquals(oldRPPrincipal.getSubjectShortDN(), newRPPrincipal.getSubjectShortDN());
        assertEquals(oldRPPrincipal.getCertSerial(), newRPPrincipal.getCertSerial());
        assertEquals(oldRPPrincipal.getIssuerDN(), newRPPrincipal.getIssuerDN());
        assertEquals(oldRPPrincipal.getName(), newRPPrincipal.getName());
        assertEquals(oldRPPrincipal.getNotAfter(), newRPPrincipal.getNotAfter());
        assertEquals(oldRPPrincipal.getNotBefore(), newRPPrincipal.getNotBefore());

        assertEquals(commonRDNSubject, newRPPrincipal.getSubjectCommonDN());
        assertEquals(shortRDNSubject, newRPPrincipal.getSubjectShortDN());
        assertEquals(expIssuerDN, newRPPrincipal.getIssuerDN());
        assertEquals(expSerial, newRPPrincipal.getCertSerial());

    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("negativeTestCases")
    public void invalidHeaderValueThrowsAuthException(String caseName, String header) {

        //given-when
        ClientCertParseException result = assertThrows(ClientCertParseException.class,
                () -> runAuth(true, header));

        //then
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Malformed 'Client-Cert' authentication header."));
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @ParameterizedTest(name = "{index} {0}")
    @MethodSource("positiveTestCases")
    public void correctHeaderResultsInPositiveAuthentication(String caseName, String header, String originalDN, String expSubjectDN, String expIssuerDN, String expSerial, List<String> policyList) throws Exception {
        //given-when
        String expName = expSubjectDN + ":" + (expSerial.length() % 2 == 0 ? "" : "0") + expSerial;
        runAuth(true, header);

        //then
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        assertNotNull(auth);
        assertEquals(expName, auth.getName());
        assertEquals("N/A", auth.getCredentials());

        Object details = auth.getDetails();
        assertInstanceOf(PreAuthenticatedCertificatePrincipal.class, auth.getPrincipal());
        PreAuthenticatedCertificatePrincipal principal = (PreAuthenticatedCertificatePrincipal) auth.getPrincipal();

        assertEquals(details, principal);
        assertEquals(expSubjectDN, principal.getSubjectShortDN());
        assertEquals(expIssuerDN, principal.getIssuerDN());
        assertEquals(expName, principal.getName());
        assertEquals(policyList.size(), principal.getPolicyOids().size());
        assertEquals(policyList.toString(), principal.getPolicyOids().toString());
        // check it also with certificate authentication
        System.out.println(principal.getSubjectOriginalDN());
        X509Certificate cert = createTestCertificate(expSerial, originalDN, principal.getIssuerDN());

        PreAuthenticatedCertificatePrincipal certPrincipal = X509CertificateUtils.extractPrincipalFromCertificate(cert);
        assertEquals(certPrincipal.getSubjectShortDN(), principal.getSubjectShortDN());
        assertEquals(certPrincipal.getIssuerDN(), principal.getIssuerDN());
        assertEquals(certPrincipal.getName(), principal.getName());
    }

    @Test
    public void clientCertTurnedOffByDefault() throws IOException, ServletException {
        //given-when
        String correctHeader = "sno=e6:66&subject=C=BE,O=org,CN=common name&validfrom=Feb++1+14:20:18+2017+GMT&validto=Jul++9+23:59:00+2019+GMT&issuer=C=DE,O=issuer org,CN=issuer common name&policy_oids=1.3.6.1.4.1.7879.13.25";
        runAuth(false, correctHeader);

        //then
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void missingHeaderIsIgnored() throws IOException, ServletException {
        //given-when
        runAuth(true, null);

        //then
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void testLDAP() {
        String domainName = "C=FI, O=Kansanelakelaitos, OU=CEF_eDelivery.testa.eu, OU=eHealth, OU=Kanta, CN=ncp.fi.ehealth.testa.eu, emailAddress=tekninentuki@kanta.fi";
        LdapName ldapName;
        try {
            ldapName = new LdapName(domainName);
        } catch (InvalidNameException e) {
            throw new BadCredentialsException("Received invalid domain name for certificate pre-authentication: " + domainName, e);
        }

        // Make a map from type to name
        final Map<String, Rdn> parts = new HashMap<>();
        for (final Rdn rdn : ldapName.getRdns()) {
            parts.put(rdn.getType(), rdn);
        }
        String dn = parts.get("CN") +
                "," +
                parts.get("O") +
                "," +
                parts.get("C");
        assertEquals("CN=ncp.fi.ehealth.testa.eu,O=Kansanelakelaitos,C=FI", dn);
    }

    private void runAuth(boolean clientCertActive, String headerValue) throws IOException, ServletException {
        //given
        MockHttpServletRequest request = new MockHttpServletRequest();
        if (headerValue != null) {
            request.addHeader("Client-Cert", headerValue);
        }
        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain chain = new MockFilterChain();
        ClientCertAuthenticationFilter filter = new ClientCertAuthenticationFilter();
        if (clientCertActive) {
            filter.setClientCertAuthenticationEnabled(true);
        }
        filter.setAuthenticationManager(createAuthenticationManager());

        //when
        filter.doFilter(request, response, chain);
    }

    /**
     * Create an authentication manager which returns the passed in object.
     */
    private AuthenticationManager createAuthenticationManager() {
        AuthenticationManager am = mock(AuthenticationManager.class);
        when(am.authenticate(any(Authentication.class))).thenAnswer(
                (Answer<Authentication>) invocation -> (Authentication) invocation.getArguments()[0]);
        return am;
    }

    /**
     * Create test certificate. Note this certificate is not valid because it is self-signed with different issuer! And it
     * MUST be used jus for testing parsing serial number, subject and issuer!!!
     *
     * @param serialNumber hex string
     * @param subject subject DN string in format "C=FI, O=Kansanelakelaitos, OU=CEF_eDelivery.testa.eu, OU=eHealth, OU=Kanta, CN=ncp.fi.ehealth.testa.eu,
     * @param issuer  issuer DN string in format "C=FI, O=Kansanelakelaitos, OU=CEF_eDelivery.testa.eu, OU=eHealth, OU=Kanta, CN=ncp.fi.ehealth.testa.eu,
     * @return test certificate
     */
    private X509Certificate createTestCertificate(String serialNumber, String subject, String issuer) throws NoSuchAlgorithmException, CertificateException, OperatorCreationException, IOException, NoSuchProviderException, InvalidAlgorithmParameterException {

        // generate keys

        KeyPair key = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        KeyUsage usage = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.nonRepudiation);

        BigInteger serial = new BigInteger(serialNumber, 16);

        return X509CertificateUtils.generateCertificate(serial, key.getPublic(), subject,
                OffsetDateTime.now().minusDays(1),
                OffsetDateTime.now().plusYears(5),
                issuer,
                key.getPrivate(),
                false, -1,
                usage, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList());
    }

}
