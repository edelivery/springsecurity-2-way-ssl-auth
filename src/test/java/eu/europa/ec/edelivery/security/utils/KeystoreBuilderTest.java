package eu.europa.ec.edelivery.security.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class KeystoreBuilderTest {
    private static final String TEST_CERT_ISSUER_DN = "CN=rootCNTest,OU=B4,O=DIGIT,L=Brussels,ST=BE,C=BE";
    private static final String TEST_CERT_SUBJECT_DN = "CN=SMP_TEST-PRE-SET-EXAMPLE, OU=eDelivery, O=DIGITAL, C=BE";

    @Test
    public void reset() {
        String secretToken = "testToken";
        String keystoreType = "PKCS12";
        String fileName = "test.p12";
        File parentFolder = Paths.get("target").toFile();

        KeystoreBuilder testInstance = KeystoreBuilder.create()
                .testMode(true)
                .secretToken(secretToken)
                .folder(parentFolder)
                .filename(fileName)
                .keystoreType(keystoreType);

        assertEquals(secretToken, testInstance.keystoreData.getSecretToken());
        assertEquals(keystoreType, testInstance.keystoreData.getKeystoreType());
        assertEquals(fileName, testInstance.keystoreData.getFilename());
        assertEquals(parentFolder, testInstance.keystoreData.getFolder());
        assertTrue(testInstance.keystoreData.isTestMode());

        testInstance.reset();
        assertNull(testInstance.keystoreData.getSecretToken());
        assertNull(testInstance.keystoreData.getKeystoreType());
        assertNull(testInstance.keystoreData.getFilename());
        assertNull(testInstance.keystoreData.getFolder());
        assertFalse(testInstance.keystoreData.isTestMode());
    }

    @Test
    public void build() {
        String secretToken = "testToken";
        String keystoreType = "PKCS12";
        String fileName = UUID.randomUUID() + ".p12";
        File parentFolder = Paths.get("target").toFile();

        KeystoreBuilder.KeystoreData result = KeystoreBuilder.create()
                .testMode(true)
                .secretToken(secretToken)
                .folder(parentFolder)
                .filename(fileName)
                .keystoreType(keystoreType).build();

        assertNotNull(result.resultKeystore);
    }

    @Test
    public void buildWithCertificate() throws KeyStoreException {
        String issuerAlias = "issuer";
        String certAlias = "cert";
        String secretToken = "testToken";
        String keystoreType = "PKCS12";
        String fileName = UUID.randomUUID() + ".p12";
        File parentFolder = Paths.get("target").toFile();

        KeystoreBuilder.KeystoreData result = KeystoreBuilder.create()
                .testMode(true)
                .secretToken(secretToken)
                .folder(parentFolder)
                .filename(fileName)
                .subjectChain(TEST_CERT_ISSUER_DN, TEST_CERT_SUBJECT_DN)
                .aliasList(issuerAlias, certAlias)
                .keystoreType(keystoreType).build();

        assertNotNull(result.resultKeystore);
        assertEquals(2, result.resultKeystore.size());
        assertNotNull(result.resultKeystore.getCertificate(issuerAlias));
        assertNotNull(result.resultKeystore.getCertificate(certAlias));
    }

    @Test
    public void buildExistsWithBackup() throws IOException {
        String secretToken = "testToken";
        String keystoreType = "PKCS12";
        String fileName = UUID.randomUUID() + ".p12";
        File parentFolder = Paths.get("target").toFile();
        File exists = new File(parentFolder, fileName);
        File backup = new File(parentFolder, fileName + ".001");
        Files.write(exists.toPath(), "badKeystore".getBytes());
        assertTrue(exists.exists());
        assertFalse(backup.exists());

        KeystoreBuilder.KeystoreData result = KeystoreBuilder.create()
                .testMode(true)
                .secretToken(secretToken)
                .folder(parentFolder)
                .filename(fileName)
                .keystoreType(keystoreType).build();

        assertNotNull(result.resultKeystore);
        assertTrue(exists.exists());
        assertTrue(backup.exists());
    }

    @Test
    public void buildExistsOK() throws IOException, CertificateException, KeyStoreException, NoSuchAlgorithmException {

        String secretToken = "testToken";
        String keystoreType = "PKCS12";
        String fileName = UUID.randomUUID() + ".p12";
        File parentFolder = Paths.get("target").toFile();
        File exists = new File(parentFolder, fileName);
        File backup = new File(parentFolder, fileName + ".001");
        KeystoreUtils.createNewKeystore(exists, secretToken, keystoreType);

        assertTrue(exists.exists());
        assertFalse(backup.exists());

        KeystoreBuilder.KeystoreData result = KeystoreBuilder.create()
                .testMode(true)
                .secretToken(secretToken)
                .folder(parentFolder)
                .filename(fileName)
                .keystoreType(keystoreType).build();

        assertNotNull(result.resultKeystore);
        assertTrue(exists.exists());
        assertFalse(backup.exists());
    }
}
