/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.BadCredentialsException;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Class responsible for testing {@link PreAuthenticatedCertificatePrincipal} class.
 *
 * @author Pawel GUTOWSKI
 * @since 1.0
 */
public class PreAuthenticatedCertificatePrincipalTest {

    private static final String ANY_VALID_DN = "C=PL,O=org,CN=common name";

    @Test
    public void resultFieldsAreNormalized() {
        //given
        String subjectDN = "C=BE, CN=common name,   OU=org unit 1,OU=org unit 2,O=org";
        String issuerDN = "C=PL,O=issuer org,CN=issuer common name";
        String serial = "c400";

        //when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(subjectDN, issuerDN, serial);

        //then
        assertEquals("CN=common name,O=org,C=BE:000000000000c400", principal.getName());
        assertEquals("CN=common name,O=org,C=BE", principal.getSubjectShortDN());
        assertEquals("CN=issuer common name,O=issuer org,C=PL", principal.getIssuerDN());
    }

    @Test
    public void resultFieldsAreNormalizedWithEmailAsTag() {
        //given
        String subjectDN = "C=BE, O=European Commission, OU=CEF_eDelivery.europa.eu, OU=eHealth, CN=DUMMY_SMP_TEST,emailAddress=CEF-EDELIVERY-SUPPORT@ec.europa.eu";
        String issuerDN = "C=PL,O=issuer org,CN=issuer common name";
        String serial = "c400";

        //when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(subjectDN, issuerDN, serial);

        //then
        assertEquals("CN=DUMMY_SMP_TEST,O=European Commission,C=BE:000000000000c400", principal.getName());
        assertEquals("CN=DUMMY_SMP_TEST,O=European Commission,C=BE", principal.getSubjectShortDN());
        assertEquals("CN=issuer common name,O=issuer org,C=PL", principal.getIssuerDN());
    }

    @Test
    public void serialNumberDoesNotContainHexPrefix() {
        //given-when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, "0x00f7");

        //then
        assertEquals("CN=common name,O=org,C=PL:00000000000000f7", principal.getName());
    }

    @Test
    public void serialNumberIsNormalizedEvenForClientCertActiveCombinedFormatOfDecAndHex() {
        //given-when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, "4109 (0x100d)");

        //then
        assertEquals("CN=common name,O=org,C=PL:000000000000100d", principal.getName());
    }

    @Test
    public void serialNumberIsNormalizedToLowercase() {
        //given-when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, "0x00F7");

        //then
        assertEquals("CN=common name,O=org,C=PL:00000000000000f7", principal.getName());
    }

    @Test
    public void serialNumberDoesNotContainColonHexSeparators() {
        //given-when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, "ab:cd:ef:01:23:45");

        //then
        assertEquals("CN=common name,O=org,C=PL:0000abcdef012345", principal.getName());
    }

    @Test
    public void rejectInvalidDomainNamePatternForIssuer() {
        BadCredentialsException result = assertThrows(BadCredentialsException.class, () -> new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, "C=BE,+O=org,CN=common name", "00"));
        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("Received invalid domain name for certificate pre-authentication:"));
    }

    @Test
    public void rejectInvalidDomainNamePatternForSubject() {
        BadCredentialsException result = assertThrows(BadCredentialsException.class, () -> new PreAuthenticatedCertificatePrincipal("+C=BE,CN=common name", ANY_VALID_DN, "00"));
        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("Received invalid domain name for certificate pre-authentication:"));
    }

    @Test
    public void supportSerialAsBigInteger() {
        //given
        BigInteger serial = BigInteger.valueOf(256);

        //when
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, serial);

        //then
        assertEquals("CN=common name,O=org,C=PL:0000000000000100", principal.getName());
    }

    @Test
    public void testSettersGetters() {
        //given
        BigInteger serial = BigInteger.valueOf(256);
        PreAuthenticatedCertificatePrincipal principal = new PreAuthenticatedCertificatePrincipal(ANY_VALID_DN, ANY_VALID_DN, serial);
        String serialValue = UUID.randomUUID().toString();
        String subjectShortDN = UUID.randomUUID().toString();
        String subjectCommonDN = UUID.randomUUID().toString();
        String subjectOriginDN = UUID.randomUUID().toString();
        String issuerDN = UUID.randomUUID().toString();
        String distroPoint = UUID.randomUUID().toString();
        Date dtFrom = Calendar.getInstance().getTime();
        Date dtTo = Calendar.getInstance().getTime();

        //when
        principal.setCertSerial(serialValue);
        principal.setSubjectShortDN(subjectShortDN);
        principal.setSubjectOriginalDN(subjectOriginDN);
        principal.setSubjectCommonDN(subjectCommonDN);
        principal.setIssuerDN(issuerDN);
        principal.setNotAfter(dtTo);
        principal.setNotBefore(dtFrom);

        principal.getClrDistributionPoints().add(distroPoint);
        //then
        assertEquals(serialValue, principal.getCertSerial());
        assertEquals(subjectShortDN, principal.getSubjectShortDN());
        assertEquals(subjectCommonDN, principal.getSubjectCommonDN());
        assertEquals(subjectOriginDN, principal.getSubjectOriginalDN());
        assertEquals(issuerDN, principal.getIssuerDN());
        assertEquals(distroPoint, principal.getClrDistributionPoints().get(0));
        assertEquals(dtFrom, principal.getNotBefore());
        assertEquals(dtTo, principal.getNotAfter());

    }

}
