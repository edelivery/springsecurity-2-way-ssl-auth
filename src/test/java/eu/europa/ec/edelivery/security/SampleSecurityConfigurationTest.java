/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;

import eu.europa.ec.edelivery.exception.ClientCertParseException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests suite verifying Sample Configuration
 * <p>
 *
 * @author Pawel GUTOWSKI
 * @since 1.0
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("/spring-security-sample-configuration.xml")
@WebAppConfiguration
public class SampleSecurityConfigurationTest {

    private static final String RETURN_LOGGED_USER_PATH = "/getLoggedUsername";

    private static final String TEST_USERNAME_CLEAR_PASS = "test_user_clear_pass";
    private static final String TEST_USERNAME_HASHED_PASS = "test_user_hashed_pass";
    private static final String PASSWORD = "gutek123";

    private static final String BLUE_COAT_VALID_HEADER = "sno=66&subject=C=BE,O=org,CN=comon name&validfrom=Dec 6 17:41:42 2016 GMT&validto=Jul 9 23:59:00 2050 GMT&issuer=C=x,O=y,CN=z";
    private static final String TEST_USERNAME_BLUE_COAT = "CN=comon name,O=org,C=BE:0000000000000066";

    private static final String BLUE_COAT_VALID_HEADER_NON_EXISTING_USER = "sno=1117&subject=C=BE,O=org,CN=not existing user&validfrom=Dec 6 17:41:42 2016 GMT&validto=Jul 9 23:59:00 2050 GMT&issuer=C=x,O=y,CN=z";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeEach
    public void setup() {

        mvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();


    }

    @Test
    public void validClientCertHeaderAuthorizedForPutTest() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Client-Cert", BLUE_COAT_VALID_HEADER);
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .headers(headers))
                .andExpect(status().isOk())
                .andExpect(content().string(TEST_USERNAME_BLUE_COAT))
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    public void validClientCertHeaderOfNonExistingUserIsNotAuthorizedForPostTest() throws Exception {
        System.out.println("validClientCertHeaderOfNonExistingUserIsNotAuthorizedForPostTest");

        HttpHeaders headers = new HttpHeaders();
        headers.clear();
        headers.add("Client-Cert", BLUE_COAT_VALID_HEADER_NON_EXISTING_USER);

        System.out.println("validClientCertHeaderOfNonExistingUserIsNotAuthorizedForPostTest - before");
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .headers(headers))
                .andExpect(status().isUnauthorized());

        System.out.println("validClientCertHeaderOfNonExistingUserIsNotAuthorizedForPostTest - END");
    }

    @Test
    @Disabled("TODO: Error since new Spring version 6.2, the basicAuthentication even if blueCoat authenticated: ")
    public void validClientCertHeaderAuthorizedBeforeValidBasicAuthTest() throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Client-Cert", BLUE_COAT_VALID_HEADER);
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .headers(headers)
                        .with(httpBasic(TEST_USERNAME_HASHED_PASS, PASSWORD))
                 )
                .andExpect(status().isOk())
                .andExpect(content().string(TEST_USERNAME_BLUE_COAT));
    }

    @Test
    public void malformedClientCertHeaderNotAuthorizedTest(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Client-Cert", "malformed-header-value");

        ClientCertParseException result = Assertions.assertThrows(ClientCertParseException.class,
                () -> mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .headers(headers))
                .andExpect(status().isUnauthorized()));

        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.is("Malformed 'Client-Cert' authentication header. This might be a bug or environment configuration: malformed-header-value"));
    }

    @Test
    public void getMethodAccessiblePubliclyTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get(RETURN_LOGGED_USER_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("anonymousUser"));
    }

    @Test
    public void postMethodNotAllowedForAnonymousUserTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void postMethodAllowedForUserStoredWithHashedPassTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .with(httpBasic(TEST_USERNAME_HASHED_PASS, PASSWORD)))
                .andExpect(status().isOk())
                .andExpect(content().string(TEST_USERNAME_HASHED_PASS));
    }

    @Test
    public void postMethodNotAllowedForUserStoredWithClearPassTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post(RETURN_LOGGED_USER_PATH)
                        .with(httpBasic(TEST_USERNAME_CLEAR_PASS, PASSWORD)))
                .andExpect(status().isUnauthorized());
    }
}
