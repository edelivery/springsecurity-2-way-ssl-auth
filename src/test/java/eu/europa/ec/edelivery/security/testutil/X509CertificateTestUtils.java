package eu.europa.ec.edelivery.security.testutil;

import eu.europa.ec.edelivery.security.utils.CertificateKeyType;
import eu.europa.ec.edelivery.security.utils.KeystoreUtils;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.bouncycastle.asn1.x500.X500Name;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class X509CertificateTestUtils {

    public static final String TEST_SECURITY_TOKEN = UUID.randomUUID().toString();

    public static final String KEYSTORE_TYPE_DEFAULT = "PKCS12";
    public static final String CERTIFICATE_POLICY_ANY = "2.5.29.32.0";
    public static final String CERTIFICATE_POLICY_QCP_NATURAL = "0.4.0.194112.1.0";
    public static final String CERTIFICATE_POLICY_QCP_LEGAL = "0.4.0.194112.1.1";
    public static final String CERTIFICATE_POLICY_QCP_NATURAL_QSCD = "0.4.0.194112.1.2";
    public static final String CERTIFICATE_POLICY_QCP_LEGAL_QSCD = "0.4.0.194112.1.3";

    public static X509Certificate createSelfSignedCertificate(String subject) throws Exception {
        return createCertificateChain(new String[]{subject})[0];
    }

    public static X509Certificate[] createCertificateChain(String[] subjects) throws Exception {
        return createCertificateChain(subjects, Collections.emptyList(), null, null);
    }

    /**
     * Method generates certificate chain
     *
     * @param subjects                in order from CA to leaf
     * @param certificatePoliciesOids Policy oids fromCA to leaf
     * @param startDate               valid certificates from
     * @param expiryDate              valid certificates to
     * @return
     * @throws Exception
     */
    public static X509Certificate[] createCertificateChain(String[] subjects, List<List<String>> certificatePoliciesOids, Date startDate, Date expiryDate) throws Exception {
        return createCertificateChain(subjects, null, null, certificatePoliciesOids, startDate, expiryDate);
    }

    /**
     * Method generates certificate chain
     *
     * @param subjects                in order from CA to leaf
     * @param certificatePoliciesOids Policy oids fromCA to leaf
     * @param startDate               valid certificates from
     * @param expiryDate              valid certificates to
     * @return
     * @throws Exception
     */
    public static X509Certificate[] createCertificateChain(String[] subjects, BigInteger[] serialNumbers, CertificateKeyType[] certTypes, List<List<String>> certificatePoliciesOids, Date startDate, Date expiryDate) throws Exception {

        KeyStore keyStore = createCertificateChainKeystore(TEST_SECURITY_TOKEN, subjects, serialNumbers, certTypes, startDate, expiryDate, certificatePoliciesOids, null, null);

        X500Name leafCertificateSubject = new X500Name(subjects[subjects.length - 1]);

        Enumeration<String> aliases = keyStore.aliases();
        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            X509Certificate cert = (X509Certificate) keyStore.getCertificate(alias);
            X500Name x500name = new X500Name(cert.getSubjectX500Principal().getName(X500Principal.RFC1779));

            if (x500name.equals(leafCertificateSubject)) {
                Certificate[] chain = keyStore.getCertificateChain(alias);
                return Stream.of(chain).map(certificate -> (X509Certificate) certificate).collect(Collectors.toList()).toArray(new X509Certificate[]{});
            }

        }
        return new X509Certificate[]{};
    }

    /**
     * Method generates certificate chain
     *
     * @param subjects                in order from CA to leaf
     * @param certificatePoliciesOids Policy oids fromCA to leaf
     * @param startDate               valid certificates from
     * @param expiryDate              valid certificates to
     * @return
     * @throws Exception
     */
    public static KeyStore createCertificateChainKeystore(String pass, String[] subjects, Date startDate, Date expiryDate, List<List<String>> certificatePoliciesOids, List<String> leafCRLs, List<String> leafOcspUrls) throws Exception {
        return createCertificateChainKeystore(pass, subjects, null, null, startDate, expiryDate, certificatePoliciesOids, leafCRLs, leafOcspUrls);
    }

    /**
     * Method generates certificate chain
     *
     * @param subjects                in order from leaf
     * @param certificatePoliciesOids Policy oids fromCA to leaf
     * @param startDate               valid certificates from
     * @param expiryDate              valid certificates to
     * @return
     * @throws Exception
     */
    public static KeyStore createCertificateChainKeystore(String pass, String[] subjects, BigInteger[] serialNumbers,
                                                          CertificateKeyType[] certificateKeyTypes, Date startDate, Date expiryDate,
                                                          List<List<String>> certificatePoliciesOids, List<String> leafCRLs, List<String> leafOcspUrls) throws Exception {

        KeyStore keystore = KeyStore.getInstance(KEYSTORE_TYPE_DEFAULT);
        char[] password = pass.toCharArray();
        keystore.load(null, password);

        X509CertificateUtils.createAndStoreCertificateWithChain2(serialNumbers, subjects, null,
                startDate == null ? OffsetDateTime.now().minusDays(1) : startDate.toInstant().atOffset(ZoneOffset.UTC),
                expiryDate == null ? OffsetDateTime.now().plusYears(1) : expiryDate.toInstant().atOffset(ZoneOffset.UTC),
                certificateKeyTypes, certificatePoliciesOids, leafCRLs, leafOcspUrls, keystore, pass);

        return keystore;
    }

    public static KeyStore createTruststore(X509Certificate... certificates) throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        KeyStore truststore = KeyStore.getInstance(KeyStore.getDefaultType());
        char[] password = "123456".toCharArray();
        truststore.load(null, password);
        for (X509Certificate certificate : certificates) {
            truststore.setCertificateEntry(KeystoreUtils.createAliasFromCert(certificate), certificate);
        }
        return truststore;
    }
}
