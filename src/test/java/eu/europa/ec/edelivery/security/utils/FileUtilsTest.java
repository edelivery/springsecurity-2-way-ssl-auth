package eu.europa.ec.edelivery.security.utils;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class FileUtilsTest {
    public static final String BUILD_TEMP_FOLDER = "target";

    @Test
    public void backupFileIfExistsNotExists() {
        String fileName = generateRandomFileName();
        Path testFilePath = Paths.get(BUILD_TEMP_FOLDER, fileName);
        Path testFilePathBackup = Paths.get(BUILD_TEMP_FOLDER, fileName + ".001");
        assertFalse(Files.exists(testFilePath));
        assertFalse(Files.exists(testFilePathBackup));
        // when
        FileUtils.backupFileIfExists(testFilePath.toFile());

        assertFalse(Files.exists(testFilePath));
        assertFalse(Files.exists(testFilePathBackup));
    }

    @Test
    public void backupFileIfExistsCreateABackup() throws IOException {
        String fileName = generateRandomFileName();
        Path testFilePath = Paths.get(BUILD_TEMP_FOLDER, fileName);
        Path testFilePathBackup = Paths.get(BUILD_TEMP_FOLDER, fileName + ".001");
        Files.write(testFilePath, fileName.getBytes());


        assertTrue(Files.exists(testFilePath));
        assertFalse(Files.exists(testFilePathBackup));
        // when
        FileUtils.backupFileIfExists(testFilePath.toFile());
        // the backup is generated and the file does not exist anymore
        assertFalse(Files.exists(testFilePath));
        assertTrue(Files.exists(testFilePathBackup));
    }

    @Test
    public void backupFileIfExistsCreateABackup2() throws IOException {
        String fileName = generateRandomFileName();
        Path testFilePath = Paths.get(BUILD_TEMP_FOLDER, fileName);
        Path testFilePathBackup1 = Paths.get(BUILD_TEMP_FOLDER, fileName + ".001");
        Path testFilePathBackup2 = Paths.get(BUILD_TEMP_FOLDER, fileName + ".002");
        Files.write(testFilePath, "testValue" .getBytes());
        Files.write(testFilePathBackup1, "testValue" .getBytes());

        assertTrue(Files.exists(testFilePath));
        assertTrue(Files.exists(testFilePathBackup1));
        assertFalse(Files.exists(testFilePathBackup2));
        // when
        FileUtils.backupFileIfExists(testFilePath.toFile());
        // the backup is generated and the file does not exist anymore
        assertFalse(Files.exists(testFilePath));
        assertTrue(Files.exists(testFilePathBackup1));
        assertTrue(Files.exists(testFilePathBackup2));
    }

    private String generateRandomFileName() {
        return UUID.randomUUID() + ".jks";
    }
}
