package eu.europa.ec.edelivery.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PreAuthenticatedAnonymousPrincipalTest {

    @Test
    public void getNameTest() {
        // given
        PreAuthenticatedAnonymousPrincipal principal = new PreAuthenticatedAnonymousPrincipal();

        assertEquals("Anonymous", principal.getName());
    }

    @Test
    public void toStringTest() {

        // given
        PreAuthenticatedAnonymousPrincipal principal = new PreAuthenticatedAnonymousPrincipal();

        assertEquals("PreAuthenticatedAnonymousPrincipal", principal.toString());
    }
}
