package eu.europa.ec.edelivery.security.utils;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.Extension;
import org.junit.jupiter.api.Test;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Basic tests for the X509CertificateUtils
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class X509CertificateUtilsTest {

    @Test
    public void extractExtensionDistributionPoints() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-crl-test-all.pem");
        ASN1Primitive result = X509CertificateUtils.extractExtension(certificate, Extension.cRLDistributionPoints.getId());

        assertNotNull(result);
    }

    @Test
    public void extractExtensionNotExists() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-crl-test-nolist.pem");
        ASN1Primitive result = X509CertificateUtils.extractExtension(certificate, Extension.cRLDistributionPoints.getId());

        assertNull(result);
    }

    @Test
    public void extractExtensionAuthorityInfoAccess() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-ocsp-test-multiple-entries.pem");
        ASN1Primitive result = X509CertificateUtils.extractExtension(certificate, Extension.authorityInfoAccess.getId());

        assertNotNull(result);
    }

    @Test
    public void getCrlDistributionUrlHttps() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-crl-test-nolist.pem");
        String result = X509CertificateUtils.getCrlDistributionUrl(certificate);

        assertNull(result);
    }

    @Test
    public void getCrlDistributionUrl() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-crl-test-all.pem");
        String result = X509CertificateUtils.getCrlDistributionUrl(certificate);

        assertNotNull(result);
        // return https if exists
        assertEquals("https://localhost/clr", result);
    }

    @Test
    public void getCrlDistributionPoints() throws CertificateException {
        X509Certificate certificate = loadCertificate("smp-crl-test-all.pem");
        List<String> result = X509CertificateUtils.getCrlDistributionPoints(certificate);

        assertNotNull(result);
        // return https if exists
        assertEquals(3, result.size());
    }

    @Test
    public void hasDistinguishedNameAllRequiredValuesCaseInsensitiveTrue() {
        String name = "CN=Test name, O=MyOrganization, C=EU";
        boolean result =  X509CertificateUtils.hasDistinguishedNameAllRequiredValues(name,
                new String[]{"CN","o","C"});
        assertTrue(result);
    }

    @Test
    public void hasDistinguishedNameAllRequiredValuesFalse()  {
        String name = "CN=Test name,C=EU";
        boolean result =  X509CertificateUtils.hasDistinguishedNameAllRequiredValues(name,
                new String[]{"CN","o","C"});
        assertFalse(result);
    }

    @Test
    public void getX509CertificatePem() throws IOException, CertificateException {
        String certValue = StreamUtils.copyToString(X509CertificateUtilsTest.class.getResourceAsStream("/certificates/PeppolTestSMP-PEM-encoded.pem"), Charset.defaultCharset());
        X509Certificate result = X509CertificateUtils.getX509Certificate(certValue);
        assertNotNull(result);
    }

    @Test
    public void getX509CertificatePemHeader() throws IOException, CertificateException {
        String certValue = StreamUtils.copyToString(X509CertificateUtilsTest.class.getResourceAsStream("/certificates/certificate-pem-with-header.cer"), Charset.defaultCharset());
        X509Certificate result = X509CertificateUtils.getX509Certificate(certValue);
        assertNotNull(result);
    }

    @Test
    public void getX509CertificateBase64Der() throws IOException, CertificateException {
        String certValue = Base64.getEncoder().encodeToString(
                StreamUtils.copyToByteArray(
                        X509CertificateUtilsTest.class.getResourceAsStream("/certificates/certificate-pem-with-header.cer")));
        X509Certificate result = X509CertificateUtils.getX509Certificate(certValue);
        assertNotNull(result);
    }

    public static X509Certificate loadCertificate(String filename) throws CertificateException {

        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        return (X509Certificate) fact.generateCertificate(X509CertificateUtilsTest.class
                .getResourceAsStream("/certificates/" + filename));
    }

    public static byte[] getBytes(String filename) throws IOException {
        return StreamUtils.copyToByteArray(X509CertificateUtilsTest.class.getResourceAsStream("/certificates/" + filename));
    }
}
