package eu.europa.ec.edelivery.security.utils;

import eu.europa.ec.edelivery.security.cert.CertificateValidator;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class X509CertificateUtilsCreateCertificatesTest {

    File testKeystoreFile = null;
    public static final String CERTIFICATE_POLICY_ANY = "2.5.29.32.0";
    public static final String CERTIFICATE_POLICY_QCP_NATURAL = "0.4.0.10000.1.0";
    public static final String CERTIFICATE_POLICY_QCP_LEGAL = "0.4.0.10000.1.1";

    @BeforeEach
    public void beforeTest() {
        testKeystoreFile = Paths.get("target", UUID.randomUUID() + ".jks").toFile();
    }

    @AfterEach
    public void afterTest() {
        if (testKeystoreFile != null) {
            testKeystoreFile.delete();
        }
    }

    @Test
    public void generateCertificate() throws Exception {
        // given
        String subject = "CN=test,OU=eDelivery,O=DIGIT,C=EU";
        KeyPair key = X509CertificateUtils.generateKeyPair(CertificateKeyType.RSA_2048);
        OffsetDateTime dateFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime dateTo = OffsetDateTime.now().plusYears(5);
        KeyUsage usage = new KeyUsage(KeyUsage.keyCertSign | KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage.dataEncipherment | KeyUsage.nonRepudiation);

        X509Certificate cert = X509CertificateUtils.generateCertificate(null, key.getPublic(), subject,
                dateFrom,
                dateTo,
                null,
                key.getPrivate(),
                false, -1,
                usage, Collections.emptyList(), Collections.emptyList(),
                Arrays.asList(CERTIFICATE_POLICY_ANY, CERTIFICATE_POLICY_QCP_NATURAL,
                        CERTIFICATE_POLICY_QCP_LEGAL));


        assertNotNull(cert);
        assertEquals(new X500Name(RFC4519Style.INSTANCE, subject),
                new X500Name(RFC4519Style.INSTANCE, cert.getSubjectX500Principal().getName()));
        assertEquals(new X500Name(RFC4519Style.INSTANCE, subject),
                new X500Name(RFC4519Style.INSTANCE, cert.getIssuerX500Principal().getName()));
        cert.checkValidity();
    }

    @Test
    public void createAndAddSelfSignedCertificate() throws Exception {
        // given
        String subject = "CN=test,OU=eDelivery,O=DIGIT,C=EU";
        String passwd = UUID.randomUUID().toString();
        String alias = UUID.randomUUID().toString();
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        // when
        X509Certificate certificate = X509CertificateUtils.createAndStoreSelfSignedCertificate(subject, alias, keystore, passwd);
        // then
        assertNotNull(certificate);
        assertEquals(certificate, keystore.getCertificate(alias));
        // also key is stored to keystore
        assertNotNull(keystore.getKey(alias, passwd.toCharArray()));
        // test certificate is valid
        certificate.checkValidity();
    }

    @Test
    public void createAndAddCertificateWithChain() throws Exception {

        // given
        String[] subjects = new String[]{"CN=RootCa,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Issuer,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Leaf,OU=eDelivery,O=DIGIT,C=EU"};

        String passwd = UUID.randomUUID().toString();
        String[] aliases = new String[]{"root-" + UUID.randomUUID(),
                "issuer-" + UUID.randomUUID(),
                "leaf-" + UUID.randomUUID()};
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        // when
        List<String> newAliases = X509CertificateUtils.createAndStoreCertificateWithChain(
                subjects, aliases, keystore, passwd);
        // then
        assertEquals(3, newAliases.size());
        for (String alias : newAliases) {
            assertNotNull(keystore.getCertificate(alias));
            assertNotNull(keystore.getKey(alias, passwd.toCharArray()));
            assertNotNull(keystore.getCertificateChain(alias));
        }
    }

    @Test
    public void createAndAddCertificateWithChainWithSerial() throws Exception {

        // given
        String[] subjects = new String[]{"CN=RootCa,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Issuer,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Leaf,OU=eDelivery,O=DIGIT,C=EU"};

        String passwd = UUID.randomUUID().toString();
        String leafAlias = UUID.randomUUID().toString();
        String[] aliases = new String[]{"root-" + leafAlias,
                "issuer-" + leafAlias,
                leafAlias};
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        BigInteger serialNumber = BigInteger.valueOf(12345L);
        OffsetDateTime validFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime validTo = OffsetDateTime.now().plusYears(5);
        // when
        List<String> newAliases = X509CertificateUtils.createAndStoreCertificateWithChain(
                serialNumber, subjects, aliases, validFrom, validTo, keystore, passwd);
        // then
        assertEquals(3, newAliases.size());
        for (String alias : newAliases) {
            X509Certificate certificate = (X509Certificate) keystore.getCertificate(alias);
            assertNotNull(certificate);
            assertNotNull(keystore.getKey(alias, passwd.toCharArray()));
            assertNotNull(keystore.getCertificateChain(alias));
            if (alias.equalsIgnoreCase(leafAlias)) {
                assertEquals(serialNumber, certificate.getSerialNumber());
            } else {
                assertNotEquals(serialNumber, certificate.getSerialNumber());
            }
        }
    }

    @Test
    public void trustedCertificate() throws Exception {
        // given
        String[] subjects = new String[]{"CN=RootCa,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Issuer,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Leaf,OU=eDelivery,O=DIGIT,C=EU"};

        String passwd = UUID.randomUUID().toString();
        String leafAlias = UUID.randomUUID().toString();
        String[] aliases = new String[]{"root-" + leafAlias,
                "issuer-" + leafAlias,
                leafAlias};
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        BigInteger serialNumber = BigInteger.valueOf(12345L);
        OffsetDateTime validFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime validTo = OffsetDateTime.now().plusYears(5);
        // when
        X509CertificateUtils.createAndStoreCertificateWithChain(
                serialNumber, subjects, aliases, validFrom, validTo, keystore, passwd);

        CertificateValidator certificateValidator = new CertificateValidator(null, keystore, null);
        X509Certificate certificate = (X509Certificate) keystore.getCertificate(leafAlias);

        certificateValidator.validateCertificate(certificate);
    }

    @Test
    public void trustedCertificateByIssuer() throws Exception {
        // given
        String[] subjects = new String[]{"CN=RootCa,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Issuer,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Leaf,OU=eDelivery,O=DIGIT,C=EU"};

        String passwd = UUID.randomUUID().toString();
        String leafAlias = UUID.randomUUID().toString();
        String[] aliases = new String[]{"root-" + leafAlias,
                "issuer-" + leafAlias,
                leafAlias};
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        BigInteger serialNumber = BigInteger.valueOf(12345L);
        OffsetDateTime validFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime validTo = OffsetDateTime.now().plusYears(5);
        // when
        X509CertificateUtils.createAndStoreCertificateWithChain(
                serialNumber, subjects, aliases, validFrom, validTo, keystore, passwd);

        // get certificate and remove it from truststore
        X509Certificate certificate = (X509Certificate) keystore.getCertificate(leafAlias);
        keystore.deleteEntry(leafAlias);

        CertificateValidator certificateValidator = new CertificateValidator(null, keystore, null);
        // validate certificate
        certificateValidator.validateCertificate(certificate);
    }

    @Test
    public void trustedCertificateNotTrusted() throws Exception {
        // given
        String[] subjects = new String[]{"CN=RootCa,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Issuer,OU=eDelivery,O=DIGIT,C=EU",
                "CN=Leaf,OU=eDelivery,O=DIGIT,C=EU"};

        String passwd = UUID.randomUUID().toString();
        String leafAlias = UUID.randomUUID().toString();
        String issuerAlias = "issuer-" + leafAlias;
        String[] aliases = new String[]{"root-" + leafAlias,
                issuerAlias,
                leafAlias};
        KeyStore keystore = KeystoreUtils.createNewKeystore(testKeystoreFile, passwd);
        BigInteger serialNumber = BigInteger.valueOf(12345L);
        OffsetDateTime validFrom = OffsetDateTime.now().minusDays(1);
        OffsetDateTime validTo = OffsetDateTime.now().plusYears(5);
        // when
        X509CertificateUtils.createAndStoreCertificateWithChain(
                serialNumber, subjects, aliases, validFrom, validTo, keystore, passwd);

        // get certificate and remove it from truststore
        X509Certificate certificate = (X509Certificate) keystore.getCertificate(leafAlias);
        keystore.deleteEntry(leafAlias);
        keystore.deleteEntry(issuerAlias);

        CertificateValidator certificateValidator = new CertificateValidator(null, keystore, null);
        // validate certificate
        CertificateException result = assertThrows(CertificateException.class, () -> certificateValidator.validateCertificate(certificate));
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("is not trusted"));
    }
}
