package eu.europa.ec.edelivery.security.cert;

import eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils;
import eu.europa.ec.edelivery.security.utils.CertificateKeyType;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.*;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import static eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.StringUtils.lowerCase;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CertificateValidatorTest {

    static {
        if (!org.apache.xml.security.Init.isInitialized()) {
            org.apache.xml.security.Init.init();
        }
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    static final String CERT_FILENAME_SMP = "SMP_test_certificate.crt";
    static final String CERT_FILENAME_INTERMEDIATE = "Test_intermediate_Issuer_01.crt";
    static final String CERT_FILENAME_ROOT_CA = "TEST_Root_CA_V01.crt";
    static final String CERT_FILENAME_SMP_INVALID = "SMP_invalid.pem";

    static final String RESOURCE_PATH = "/certificates/cert-validator/";
    Pattern regExpGeneral = Pattern.compile(".*");
    Pattern regExpNotExists = Pattern.compile("CN=NotExits.*");

    IRevocationValidator revocationValidator = mock(IRevocationValidator.class);

    CertificateValidator testInstance = spy(new CertificateValidator(null, null, null));

    @BeforeEach
    public void init() {

        // reset configuration
        testInstance.clearRevocationVerifierServices();
        testInstance.setSubjectRegularExpressionPattern(regExpGeneral);
    }

    @Test
    public void testValidateOKWholeCertificateChain() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, WSSecurityException {
        // given
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_SMP, CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        //when
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
    }

    @Test
    public void testValidateOKIssuerCertificateChain() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, WSSecurityException {
        // given
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        //when
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
    }

    @Test
    public void testValidateOKWholeCertificateChainWithPolicy() throws Exception {
        // given
        Date date = Calendar.getInstance().getTime();
        // create certificate change
        X509Certificate[] certificates = createCertificateChain(
                new String[]{"CN=CA,O=digit,C=EU", "CN=Issuer,O=digit,C=EU", "CN=Leaf,O=digit,C=EU"},
                asList(singletonList(CERTIFICATE_POLICY_ANY),
                        singletonList(CERTIFICATE_POLICY_QCP_NATURAL),
                        singletonList(CERTIFICATE_POLICY_QCP_NATURAL)),
                DateUtils.addDays(date, -1), DateUtils.addDays(date, 1));

        // create truststore with issues and CA
        KeyStore truststore = X509CertificateTestUtils.createTruststore(certificates[1], certificates[2]);

        testInstance.setTrustStore(truststore);
        // set allowed
        testInstance.setAllowedCertificatePolicyOIDs(singletonList(CERTIFICATE_POLICY_QCP_NATURAL));
        // leaf certificate
        X509Certificate certificate = certificates[0];

        //when
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
    }

    /**
     * This is test of "test method" if jdk with bouncy castle can generate  and serialize the certificates with target key types.
     *
     * @throws Exception  if any error occurs
     */
    @Test
    public void generateCertificates() throws Exception {
        String password = "test123";
        CertificateKeyType type = CertificateKeyType.Ed25519;
        KeyStore keystore = createCertificateChainKeystore(password,
                new String[]{"CN=Test-" + type.name() + ",OU=EDELIVERY,O=DIGIT,C=EU"},
                null, new CertificateKeyType[]{type}, null, null,
                null, null, null);

        try (FileOutputStream fos = new FileOutputStream("target/" + type.name() + ".p12")) {
            keystore.store(fos, password.toCharArray());
        }

        KeyStore loaded = KeyStore.getInstance(KEYSTORE_TYPE_DEFAULT);
        try (FileInputStream fis = new FileInputStream("target/" + type.name() + ".p12")) {
            loaded.load(fis, password.toCharArray());
        }
    }

    @Test
    public void testValidateOKWholeCertificateChainWithMultiplePolicies() throws Exception {
        // given
        Date date = Calendar.getInstance().getTime();
        // create certificates
        X509Certificate[] certificates = createCertificateChain(
                new String[]{"CN=CA,O=digit,C=EU", "CN=Issuer,O=digit,C=EU", "CN=Leaf,O=digit,C=EU"},
                asList(singletonList(CERTIFICATE_POLICY_ANY),
                        asList(CERTIFICATE_POLICY_QCP_NATURAL, CERTIFICATE_POLICY_QCP_LEGAL, CERTIFICATE_POLICY_QCP_NATURAL_QSCD, CERTIFICATE_POLICY_QCP_LEGAL_QSCD),
                        singletonList(CERTIFICATE_POLICY_QCP_NATURAL)),
                DateUtils.addDays(date, -1), DateUtils.addDays(date, 3650));
        KeyStore truststore = X509CertificateTestUtils.createTruststore(certificates[1], certificates[2]);
        testInstance.setTrustStore(truststore);
        // set allowed
        testInstance.setAllowedCertificatePolicyOIDs(asList(CERTIFICATE_POLICY_QCP_NATURAL, CERTIFICATE_POLICY_QCP_LEGAL));

        // leaf certificate
        X509Certificate certificate = certificates[0];

        //when
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
    }

    @Test
    public void testValidateWholeCertificateChainFailedWithWrongPolicy() throws Exception {
        // given
        Date date = Calendar.getInstance().getTime();
        // crate certificate change
        X509Certificate[] certificates = createCertificateChain(
                new String[]{"CN=CA,O=digit,C=EU", "CN=Issuer,O=digit,C=EU", "CN=Leaf,O=digit,C=EU"},
                asList(singletonList(CERTIFICATE_POLICY_ANY),
                        asList(CERTIFICATE_POLICY_QCP_NATURAL, CERTIFICATE_POLICY_QCP_LEGAL, CERTIFICATE_POLICY_QCP_NATURAL_QSCD, CERTIFICATE_POLICY_QCP_LEGAL_QSCD),
                        singletonList(CERTIFICATE_POLICY_QCP_NATURAL)),
                DateUtils.addDays(date, -1), DateUtils.addDays(date, 1));
        // create truststore with issues and CA
        KeyStore truststore = X509CertificateTestUtils.createTruststore(certificates[1], certificates[2]);
        testInstance.setTrustStore(truststore);
        // set Not allowed policy
        testInstance.setAllowedCertificatePolicyOIDs(singletonList(CERTIFICATE_POLICY_QCP_LEGAL));
        // leaf certificate
        X509Certificate certificate = certificates[0];

        //when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));
        // then
        MatcherAssert.assertThat(exception.getMessage(), CoreMatchers.containsString("is not trusted"));
    }

    @Test
    public void testValidateWholeCertificateChainFailedWithInvalidPolicyPath() throws Exception {
        // given
        Date date = Calendar.getInstance().getTime();
        // crate certificate change
        X509Certificate[] certificates = createCertificateChain(
                new String[]{"CN=CA,O=digit,C=EU", "CN=Issuer,O=digit,C=EU", "CN=Leaf,O=digit,C=EU"},
                asList(singletonList(CERTIFICATE_POLICY_ANY),
                        asList(CERTIFICATE_POLICY_QCP_LEGAL, CERTIFICATE_POLICY_QCP_NATURAL_QSCD, CERTIFICATE_POLICY_QCP_LEGAL_QSCD), // missing CERTIFICATE_POLICY_QCP_NATURAL,in issuer
                        singletonList(CERTIFICATE_POLICY_QCP_NATURAL)),
                DateUtils.addDays(date, -1), DateUtils.addDays(date, 1));
        // create truststore with issues and CA
        KeyStore truststore = X509CertificateTestUtils.createTruststore(certificates[1], certificates[2]);
        testInstance.setTrustStore(truststore);
        // set Not allowed policy
        testInstance.setAllowedCertificatePolicyOIDs(singletonList(CERTIFICATE_POLICY_QCP_LEGAL));
        // leaf certificate
        X509Certificate certificate = certificates[0];

        //when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));
        // then
        MatcherAssert.assertThat(exception.getMessage(), CoreMatchers.containsString("is not trusted"));
    }

    @Test
    public void testValidateIsPassingValidationNotTrustedMissingAnchor() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        // given On root ca is in truststore without intermediate
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_ROOT_CA));
        //when
        assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));

    }

    @Test
    public void testValidateIsPassingValidationInvalid() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        // given On root ca is in truststore without intermediate
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP_INVALID);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_SMP_INVALID));
        //when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));
        // then
        assertEquals("Certificate [CN=SMP CRL NoList, OU=CEF, O=DIGIT, C=EU] is not valid!", exception.getMessage());
    }

    @Test
    public void testValidateIsPassingValidationNotTrustedIncompleteChain() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        // given On root ca is in truststore without intermediate
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_INTERMEDIATE));
        //when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));
        // then
        assertEquals("Certificate [CN=SMP test certificate, OU=eDelivery, O=Digit, C=BE] is not trusted!", exception.getMessage());
    }

    @Test
    public void testValidateIsPassingValidationNotTrustedRegExp() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        // given On root ca is in truststore without intermediate
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        testInstance.setSubjectRegularExpressionPattern(regExpNotExists);
        //when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.validateCertificate(certificate));
        // then
        assertEquals("Certificate [CN=SMP test certificate, OU=eDelivery, O=Digit, C=BE] is not trusted!", exception.getMessage());
    }


    @Test
    public void testValidateOKWholeCertificateChainWithSerial() throws Exception {
        // given
        // create certificate change
        X509Certificate[] certificates = createCertificateChain(
                new String[]{"CN=CA,O=digit,C=EU", "CN=Issuer,O=digit,C=EU", "SERIALNUMBER=123456,CN=E359391@Leaf.SMP.ACC,O=digit,C=EU"});

        X509Certificate certificate = certificates[0];
        // create truststore with issues and CA
        KeyStore truststore = X509CertificateTestUtils.createTruststore(certificates[1], certificates[2]);

        testInstance.setTrustStore(truststore);
        //C=EU,O=digit,CN=E359391@Leaf.SMP.ACC,2.5.4.5=#1306313233343536
        Pattern testRegExp = Pattern.compile("^(?=.*CN=[A-Z]\\d{0,7}@Leaf\\.SMP\\.ACC\\b)(?i)(?=.*\\b(2.5.4.5=#1306313233343536)|(SERIALNUMBER=123456)\\b).*$");
        testInstance.setSubjectRegularExpressionPattern(testRegExp);
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
    }

    @Test
    public void testValidateOKCRL() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, WSSecurityException {
        // given
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        X509Certificate intermediate = getCertificate(CERT_FILENAME_INTERMEDIATE);
        X509Certificate root = getCertificate(CERT_FILENAME_ROOT_CA);

        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        testInstance.addRevocationVerifierService(revocationValidator);

        //when
        testInstance.validateCertificate(certificate);
        // then
        verify(testInstance, atLeast(1)).isCertificateValid(certificate);
        verify(testInstance).verifyTrust(certificate);
        verify(testInstance).verifyCertificateChain(certificate);
        verify(testInstance).isCertificateValid(certificate);
        verify(testInstance).isCertificateValid(intermediate);
        verify(revocationValidator).isCertificateRevoked(certificate, intermediate);
    }

    @Test
    public void testValidateCRLCertificateIsRevoked() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException{
        // given
        X509Certificate certificate = getCertificate(CERT_FILENAME_SMP);
        X509Certificate issuer = getCertificate(CERT_FILENAME_INTERMEDIATE);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        testInstance.addRevocationVerifierService(revocationValidator);
        CertificateRevokedException exception = new CertificateRevokedException(Calendar.getInstance().getTime(), CRLReason.UNSPECIFIED,
                certificate.getIssuerX500Principal(), new HashMap<>());
        doThrow(new CertificateRevokedException(Calendar.getInstance().getTime(), CRLReason.UNSPECIFIED,
                certificate.getIssuerX500Principal(), new HashMap<>())).when(revocationValidator).isCertificateRevoked(certificate, issuer);
        //CertificateRevokedException
        CertificateRevokedException result = assertThrows(CertificateRevokedException.class, () -> testInstance.validateCertificate(certificate));
        // then
        assertEquals(exception.getMessage(), result.getMessage());
    }

    @Test
    public void testIsSignedByTrue() throws CertificateException {
        X509Certificate certificateSMP = getCertificate(CERT_FILENAME_SMP);
        X509Certificate certificateIntermediate = getCertificate(CERT_FILENAME_INTERMEDIATE);

        assertTrue(testInstance.isSignedBy(certificateSMP, certificateIntermediate));
    }

    @Test
    public void testIsSignedByFalse() throws CertificateException {
        X509Certificate certificateSMP = getCertificate(CERT_FILENAME_SMP);
        X509Certificate certificateIntermediate = getCertificate(CERT_FILENAME_ROOT_CA);

        assertFalse(testInstance.isSignedBy(certificateSMP, certificateIntermediate));
    }

    @Test
    public void testGetCertificateAlias() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        // given
        X509Certificate certificateSMP = getCertificate(CERT_FILENAME_SMP);
        testInstance.setTrustStore(buildTruststore(CERT_FILENAME_SMP, CERT_FILENAME_INTERMEDIATE, CERT_FILENAME_ROOT_CA));
        // when
        String alias = testInstance.getCertificateAlias(certificateSMP);
        // then
        assertEquals(lowerCase(CERT_FILENAME_SMP), lowerCase(alias));
    }

    private X509Certificate getCertificate(String filename) throws CertificateException {

        CertificateFactory fact = CertificateFactory.getInstance("X.509");
        return (X509Certificate) fact.generateCertificate(
                getClass().getResourceAsStream(RESOURCE_PATH + filename));

    }

    private KeyStore buildTruststore(String... filenames) throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {

        KeyStore truststore = KeyStore.getInstance(KeyStore.getDefaultType());
        truststore.load(null, null);
        for (String filename : filenames) {
            X509Certificate certificate = getCertificate(filename);
            truststore.setCertificateEntry(filename, certificate);
        }
        return truststore;
    }
}
