package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.exception.CertificateValidationTechnicalException;
import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Test class for {@link CertificateRevocationValidatorService} class methods.
 *
 * @author Joze RIHTARSIC
 * @since 1.14
 */
class CertificateRevocationValidatorServiceTest {

    private final ProxyDataCallback proxyDataMock = mock(ProxyDataCallback.class);
    private final HttpURLFetcher httpURLFetcher = spy(new HttpURLFetcher(proxyDataMock));
    private final CertificateRevocationValidatorService testInstance = spy(new CertificateRevocationValidatorService(httpURLFetcher));

    @ParameterizedTest
    @CsvSource({"OCSP_ONLY, true, http://",
            "CRL_OCSP, false, ldap://",
            "NO_VALIDATION, false, file://"})
    void testSetValidationParameters(CertificateRVStrategyEnum strategy, boolean gracefulValidation, String allowedScheme) {
        // given when
        testInstance.setValidationParameters(strategy, gracefulValidation, Collections.singletonList(allowedScheme));
        // then
        assertEquals(strategy, testInstance.getValidationStrategy());
        assertEquals(gracefulValidation, testInstance.isGracefulValidation());
        assertEquals(1, testInstance.getAllowedURLSchemas().size());
        assertEquals(allowedScheme, testInstance.getAllowedURLSchemas().get(0));
    }

    @Test
    void testSetHttpConnection() {
        // given when
        testInstance.setHttpConnection(httpURLFetcher);
        // then
        assertEquals(httpURLFetcher, testInstance.getHttpConnection());
    }

    @ParameterizedTest
    @EnumSource(CertificateRVStrategyEnum.class)
    void isCertificateRevokedNoGracefulAlwaysFalse(CertificateRVStrategyEnum strategy) throws Exception {
        X509Certificate certificate = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 01,O=Digit,C=BE");
        testInstance.setValidationParameters(strategy, true, Collections.singletonList("http://"));

        boolean result = testInstance.isCertificateRevoked(certificate, null);
        // always false (not validated) because certificate does not have verification endpoint extensions
        assertFalse(result);
    }

    @ParameterizedTest
    @EnumSource(CertificateRVStrategyEnum.class)
    void testIsSerialNumberRevoked(CertificateRVStrategyEnum strategy) throws Exception {

        testInstance.setValidationParameters(strategy, true, Collections.singletonList("http://"));

        boolean result = testInstance.isSerialNumberRevoked(BigInteger.TEN, null, Collections.emptyList());
        // always false (not validated) because no verification endpoints are given
        assertFalse(result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"OCSP_ONLY", "CRL_OCSP"})
    void testIsSerialNumberRevokedFails(CertificateRVStrategyEnum strategy) {

        testInstance.setValidationParameters(strategy, true, Collections.singletonList("http://"));
        // throws exception because graceful false and issuer cert is mandatory for OCSP validation
        CertificateException result = assertThrows(CertificateException.class, () ->
                testInstance.isSerialNumberRevoked(BigInteger.TEN, null, Collections.singletonList("http://localhost:8080/crl")));
        // always false (not validated) because no verification endpoints are given
        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("Missing issuer certificate for serial number"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"OCSP_CRL", "CRL_OCSP", "NO_VALIDATION"})
    void getAllValidationEndpointsThrowError(CertificateRVStrategyEnum strategy) throws Exception {
        // given when
        X509Certificate certificate = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 01,O=Digit,C=BE");
        testInstance.setValidationStrategy(strategy);

        CertificateValidationTechnicalException result = assertThrows(CertificateValidationTechnicalException.class, () -> testInstance.getAllValidationEndpoints(certificate));
        MatcherAssert.assertThat(result.getMessage(),
                CoreMatchers.containsString("Unsupported method for validation strategy"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"OCSP_ONLY", "CRL_ONLY"})
    void getAllValidationEndpointsNoError(CertificateRVStrategyEnum strategy) throws Exception {
        // given when
        X509Certificate certificate = X509CertificateTestUtils.createSelfSignedCertificate("CN=Keystore test 01,O=Digit,C=BE");
        testInstance.setValidationStrategy(strategy);

        List<String> result = testInstance.getAllValidationEndpoints(certificate);
        assertNotNull(result);
    }
}
