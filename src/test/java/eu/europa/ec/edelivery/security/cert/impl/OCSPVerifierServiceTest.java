package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import eu.europa.ec.edelivery.security.testutil.X509CertificateTestUtils;
import eu.europa.ec.edelivery.security.testutil.X509OCSPTestUtils;
import eu.europa.ec.edelivery.security.utils.X509CertificateUtilsTest;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.CertificateRevokedException;
import java.security.cert.X509Certificate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class OCSPVerifierServiceTest {

    ProxyDataCallback proxyDataMock = mock(ProxyDataCallback.class);
    HttpURLFetcher httpURLFetcher = spy(new HttpURLFetcher(proxyDataMock));
    OCSPVerifierService testInstance = spy(new OCSPVerifierService(httpURLFetcher));

    @BeforeEach
    public void beforeTest() {
        // default value
        testInstance.setGracefulValidation(true);
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
    }

    @Test
    public void isSerialNumberRevokedTestNotRevoked() throws Exception {
        OCSPTestCaseData data = generateTestCaseData(OCSPResp.SUCCESSFUL, false, Collections.singletonList("http://localhost/ocsp"));
        testInstance.setGracefulValidation(false);
        doReturn(data.getResponse()).when(testInstance).getOCSPResponse(anyString(), any());

        testInstance.isCertificateRevoked(data.getCertificate(), data.getIssuer());
        // no error must be thrown
    }

    @Test
    public void isSerialNumberRevokedTestFailToTest() throws Exception {
        OCSPTestCaseData data = generateTestCaseData(OCSPResp.INTERNAL_ERROR, true, Collections.singletonList("http://localhost/ocsp"));
        doReturn(data.getResponse()).when(testInstance).getOCSPResponse(anyString(), any());
        testInstance.setGracefulValidation(false);

        CertificateException result = assertThrows(CertificateException.class,
                () -> testInstance.isCertificateRevoked(data.getCertificate(), data.getIssuer()));

        assertNotNull(result);
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Can't get Revocation Status from OCSP"));
    }

    @Test
    public void isSerialNumberRevokedTestFailToTestGraceful() throws Exception {
        OCSPTestCaseData data = generateTestCaseData(OCSPResp.INTERNAL_ERROR, true, Collections.singletonList("http://localhost/ocsp"));

        testInstance.setGracefulValidation(true);
        doReturn(data.getResponse()).when(testInstance).getOCSPResponse(anyString(), any());

        testInstance.isCertificateRevoked(data.getCertificate(), data.getIssuer());
        // no error must be thrown
    }


    /**
     * For this test to work startup DomiSML docker image  web-utils and set host  he OCSP server on localhost:8080
     *
     * @throws  CertificateException if the certificate OK is revoked
     */
    @Test
    @Disabled
    public void testCertificate() throws CertificateException {
        X509Certificate issuer = X509CertificateUtilsTest.loadCertificate("ocsp-tmp/rootCA.crt");
        X509Certificate valid = X509CertificateUtilsTest.loadCertificate("ocsp-tmp/certificateValid.crt");
        X509Certificate revoked = X509CertificateUtilsTest.loadCertificate("ocsp-tmp/certificateRevoked.crt");

        // this one is OK
        assertTrue(testInstance.isCertificateRevoked(valid, issuer));
        // this one is revoked
        CertificateException result = assertThrows(CertificateException.class, () -> testInstance.isCertificateRevoked(revoked, issuer));
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Certificate has been revoked"));
    }


    @Test
    public void isSerialNumberRevokedTestRevoked() throws Exception {
        OCSPTestCaseData data = generateTestCaseData(OCSPResp.SUCCESSFUL, true, Collections.singletonList("http://localhost/ocsp"));

        doReturn(data.getResponse()).when(testInstance).getOCSPResponse(anyString(), any());

        CertificateRevokedException result = assertThrows(CertificateRevokedException.class,
                () -> testInstance.isCertificateRevoked(data.getCertificate(), data.getIssuer()));

        assertNotNull(result);
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Certificate has been revoked"));
    }

    public OCSPTestCaseData generateTestCaseData(int ocspResponse, boolean revoked, List<String> ocspUrls) throws Exception {
        Calendar c = Calendar.getInstance();
        Date validFrom = c.getTime();
        c.add(Calendar.DAY_OF_MONTH, 1);
        Date validTo = c.getTime();
        String pass = "test123";
        KeyStore keyStore = X509CertificateTestUtils.createCertificateChainKeystore(pass, new String[]{"CN=test,O=digit,C=eu"}, validFrom, validTo, null, null,
                ocspUrls);

        String alias = keyStore.aliases().nextElement();

        X509Certificate sefSignedCertWithOcsp = (X509Certificate) keyStore.getCertificate(alias);
        Key sefSignedKeyCertWithOcsp = keyStore.getKey(alias, pass.toCharArray());

        OCSPResp resp = X509OCSPTestUtils.generateOCSPResponse(ocspResponse, sefSignedCertWithOcsp.getSerialNumber(), sefSignedCertWithOcsp, (PrivateKey) sefSignedKeyCertWithOcsp, revoked);

        return new OCSPTestCaseData(resp, sefSignedCertWithOcsp);
    }

    static class OCSPTestCaseData {
        public OCSPTestCaseData(OCSPResp response, X509Certificate certificate) {
            this(response, certificate, certificate);
        }

        public OCSPTestCaseData(OCSPResp response, X509Certificate certificate, X509Certificate issuer) {
            this.response = response;
            this.certificate = certificate;
            this.issuer = issuer;
        }

        OCSPResp response;
        X509Certificate certificate;
        X509Certificate issuer;

        public OCSPResp getResponse() {
            return response;
        }

        public X509Certificate getCertificate() {
            return certificate;
        }

        public X509Certificate getIssuer() {
            return issuer;
        }
    }
}
