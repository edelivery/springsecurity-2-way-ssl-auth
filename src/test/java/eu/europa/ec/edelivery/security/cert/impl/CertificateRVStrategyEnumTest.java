package eu.europa.ec.edelivery.security.cert.impl;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for {@link CertificateRVStrategyEnum} class methods.
 *
 * @author Joze RIHTARSIC
 * @since 1.14
 */
class CertificateRVStrategyEnumTest {

    @ParameterizedTest
    @EnumSource(CertificateRVStrategyEnum.class)
    void testFromCodeLowerCase(CertificateRVStrategyEnum strategy) {
        //given
        String code = StringUtils.lowerCase(strategy.name());
        // when then
        assertEquals(strategy, CertificateRVStrategyEnum.fromCode(code));
    }

    @ParameterizedTest
    @CsvSource({", OCSP_CRL",
            "'', OCSP_CRL",
            "INVALID_CODE, OCSP_CRL",
            "OCSP_CRL, OCSP_CRL",
            "OCSP_ONLY, OCSP_ONLY",
            "ocsp_only, OCSP_ONLY",
            "' ocsp_only', OCSP_ONLY",
            "'ocsp_only ', OCSP_ONLY",
            "' ocsp_only ', OCSP_ONLY",
            "OCSP_ONLY, OCSP_ONLY",
            "CRL_OCSP, CRL_OCSP",
            "NO_VALIDATION, NO_VALIDATION"})
    void testFromCodeLowerCase(String testValue, CertificateRVStrategyEnum expectedResult) {
        //given
        CertificateRVStrategyEnum result = CertificateRVStrategyEnum.fromCode(testValue);
        // when then
        assertEquals(expectedResult, result);
    }
}
