package eu.europa.ec.edelivery.security.cert.impl;

import eu.europa.ec.edelivery.security.cert.ProxyDataCallback;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.Security;
import java.security.cert.*;
import java.util.Arrays;

import static eu.europa.ec.edelivery.security.utils.X509CertificateUtilsTest.loadCertificate;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * @author Joze Rihtarsic
 * @since 1.9
 */
public class CRLVerifierServiceTest {

    @BeforeAll
    public static void beforeClass() {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    private final ProxyDataCallback proxyDataMock = mock(ProxyDataCallback.class);
    private final HttpURLFetcher httpURLFetcher = spy(new HttpURLFetcher(proxyDataMock));
    private final CRLVerifierService testInstance = spy(new CRLVerifierService(httpURLFetcher));

    @BeforeEach
    public void init() {
        // set by default
        testInstance.setGracefulValidation(true);
    }

    @Test
    public void verifyCertificateCRLsTest() throws CertificateException, CRLException {
        // given
        X509Certificate certificate = loadCertificate("smp-crl-test-all.pem");
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509CRL crl = (X509CRL) cf.generateCRL(CRLVerifierServiceTest.class.getResourceAsStream("/certificates/test-revocation-list.crl"));
        doReturn(crl).when(testInstance).downloadCRL(anyString(), anyBoolean());

        // when-then
        assertDoesNotThrow(() -> testInstance.isCertificateRevoked(certificate, null));
    }

    @Test
    public void verifyCertificateCRLRevokedTest() throws CertificateException, CRLException {
        // given
        X509Certificate certificate = loadCertificate("smp-crl-revoked.pem");

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509CRL crl = (X509CRL) cf.generateCRL(CRLVerifierServiceTest.class.getResourceAsStream("/certificates/smp-crl-test.crl"));

        doReturn(crl).when(testInstance).downloadCRL(anyString(), anyBoolean());

        // when
        CertificateRevokedException result = assertThrows(CertificateRevokedException.class,
                () ->   testInstance.isCertificateRevoked(certificate, null));
        //then
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Certificate has been revoked"));

    }

    @Test
    public void verifyCertificateCRLRevokedGracefulOnFailure() throws CertificateException {
        // given
        X509Certificate certificate = loadCertificate("smp-crl-revoked.pem");

        testInstance.setGracefulValidation(true);
        doReturn(null).when(testInstance).downloadCRL(anyString(), anyBoolean());
        // when-then
        try {
            testInstance.isCertificateRevoked(certificate, null);
        }catch (Exception e){
            fail();
        }
    }

    @Test
    public void verifyCertificateCRLRevokedNotGracefulOnFailure() throws CertificateException {
        // given
        X509Certificate certificate = loadCertificate("smp-crl-revoked.pem");

        testInstance.setGracefulValidation(false);
        doReturn(null).when(testInstance).downloadCRL(anyString(), anyBoolean());
        // when
        CertificateException exception = assertThrows(CertificateException.class, () -> testInstance.isCertificateRevoked(certificate, null));
        // then
        MatcherAssert.assertThat(exception.getMessage(), CoreMatchers.startsWith("Could not get CRL from non of the CLR distribution points:"));
    }

    @Test
    public void verifyCertificateCRLsX509FailsToConnectTest() throws Exception {
        // given
        X509Certificate certificate = loadCertificate("smp-crl-test-all.pem");
        doThrow(new CertificateException("Can not download CRL [https://localhost/crl]")).when(testInstance).downloadCRL(anyString(), anyBoolean());

        // when
        CertificateException result = assertThrows(CertificateException.class,
                () ->   testInstance.isCertificateRevoked(certificate, null));
        //then
        assertEquals("Can not download CRL [https://localhost/crl]", result.getMessage());
    }

    @Test
    public void downloadCRLWrongUrlSchemeTest() throws CertificateException {
        // ignore not allowed protocols
        doReturn(Arrays.asList("http", "https")).when(proxyDataMock).getAllowedURLProtocols();

        X509CRL crl = testInstance.downloadCRL("wrong://localhost/crl", true);
        // the url is not supported and is ignored
        assertNull(crl);
    }

    @Test
    public void downloadCRLUrlSchemeLdapTest() throws CertificateException {
        // ignore not allowed protocols
        doReturn(Arrays.asList("http", "https")).when(proxyDataMock).getAllowedURLProtocols();

        X509CRL crl = testInstance.downloadCRL("ldap://localhost/crl", false);
        // the url is not supported and is ignored
        assertNull(crl);
    }

    @Test
    public void downloadCRLUrlSchemeHttpTestIgnoreIfFail() throws CertificateException {
        X509CRL crl = testInstance.downloadCRL("http://sdpoidfg-must-not-notExists.com/crl", true);
        // if url fetch fails, then crl is ignored
        assertNull(crl);
    }

    @Test
    public void downloadCRLUrlSchemeHttpTestNOTIgnoreIfFail(){
        // if url fetch fails, then crl is ignored
        CertificateException result = assertThrows(CertificateException.class,
                () -> testInstance.downloadCRL("http://sdpoidfg-must-not-notExists.com/crl", false));
        assertEquals("Can not download CRL [http://sdpoidfg-must-not-notExists.com/crl].", result.getMessage());
    }

    @Test
    public void verifyCertificateCRLsRevokedSerialTest() throws CertificateException, CRLException {

        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        X509CRL crl = (X509CRL) cf.generateCRL(CRLVerifierServiceTest.class.getResourceAsStream("/certificates/smp-crl-test.crl"));

        doReturn(crl).when(testInstance).downloadCRL(anyString(), anyBoolean());

        // when
        CertificateRevokedException result = assertThrows(CertificateRevokedException.class,
                () ->  testInstance.isSerialNumberRevoked("11", null, "https://localhost/crl"));
        //then
        MatcherAssert.assertThat(result.getMessage(), CoreMatchers.containsString("Certificate has been revoked"));
    }

    @Test
    public void verifyCertificateCRLsRevokedSerialTestThrowIOExceptionHttps() throws CertificateException {
        doThrow(new CertificateException("Can not download CRL [URL].")).when(testInstance).downloadCRL(anyString(), anyBoolean());
        testInstance.setGracefulValidation(false);

        // when
        CertificateException result = assertThrows(CertificateException.class,
                () ->  testInstance.isSerialNumberRevoked("11", null, "https://localhost/crl"));
        //then
        assertEquals("Can not download CRL [URL].", result.getMessage());
    }
}
