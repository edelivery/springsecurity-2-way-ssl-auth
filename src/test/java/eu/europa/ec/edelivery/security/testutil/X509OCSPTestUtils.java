package eu.europa.ec.edelivery.security.testutil;

import eu.europa.ec.edelivery.security.utils.X509CertificateUtils;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.cert.ocsp.jcajce.JcaBasicOCSPRespBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Calendar;

/**
 * Tools for mocking the OCSP responses
 *
 * @author Joze Rihtarsic
 * @since 1.12
 */
public class X509OCSPTestUtils {

    public static BasicOCSPResp generateBasicOCSPResponse(BigInteger serialNumber, X509Certificate issuerCert, PrivateKey privateKey, boolean revoked) throws NoSuchAlgorithmException, OperatorCreationException, CertificateEncodingException, OCSPException {

        BasicOCSPRespBuilder builder = new JcaBasicOCSPRespBuilder(issuerCert.getSubjectX500Principal());
        DigestCalculator digestCalculator = new JcaDigestCalculatorProviderBuilder().build().get(CertificateID.HASH_SHA1);
        CertificateID id = new CertificateID(digestCalculator, new JcaX509CertificateHolder(issuerCert), serialNumber);
        builder.addResponse(id,
                revoked ? new RevokedStatus(Calendar.getInstance().getTime(), CRLReason.superseded) : CertificateStatus.GOOD);

        String sigAlgName = X509CertificateUtils.getJCESigningAlgorithmForPrivateKey(privateKey);
        ContentSigner contentSigner = new JcaContentSignerBuilder(sigAlgName)
                .build(privateKey);
        return builder.build(contentSigner, new X509CertificateHolder[]{new JcaX509CertificateHolder(issuerCert)}, Calendar.getInstance().getTime());
    }

    public static OCSPResp generateOCSPResponse(int result, BigInteger serialNumber, X509Certificate issuerCert, PrivateKey privateKey, boolean revoked) throws NoSuchAlgorithmException, OperatorCreationException, CertificateEncodingException, OCSPException {

        BasicOCSPResp resp = generateBasicOCSPResponse(serialNumber, issuerCert, privateKey, revoked);
        OCSPRespBuilder builder = new OCSPRespBuilder();
        return builder.build(result, resp);
    }

    public static OCSPResp generateOCSPResponse(BigInteger serialNumber, X509Certificate issuerCert, PrivateKey privateKey, boolean revoked) throws NoSuchAlgorithmException, OperatorCreationException, CertificateEncodingException, OCSPException {
        return generateOCSPResponse(OCSPResp.SUCCESSFUL, serialNumber, issuerCert, privateKey, revoked);
    }
}
