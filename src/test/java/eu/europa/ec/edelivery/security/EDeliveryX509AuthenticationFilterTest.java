/*
 * Copyright 2017 European Commission | CEF eDelivery
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 * or file: LICENCE-EUPL-v1.1.pdf
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package eu.europa.ec.edelivery.security;


import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import jakarta.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Pawel GUTOWSKI
 * @author Joze RIHTARSIC
 * @since 1.0
 */
public class EDeliveryX509AuthenticationFilterTest {

    static final String CERT_PEM = "MIICKzCCAZSgAwIBAgIEWP9xdDANBgkqhkiG9w0BAQsFADBOMQswCQYDVQQGEwJQ\n" +
            "TDEUMBIGA1UECgwLZ3V0ZWsgQ29ycC4xDjAMBgNVBAsMBURJR0lUMRkwFwYDVQQD\n" +
            "DBBsb2NhbGhvc3QgdG9tY2F0MB4XDTE3MDQyNTE1NTUzNFoXDTE4MDQyNTE1NTUz\n" +
            "NFowZjEhMB8GCSqGSIb3DQEJARYSZ3V0ZWtAYnV6aWFjemVrLnBsMQswCQYDVQQG\n" +
            "EwJHQjELMAkGA1UECgwCRUMxEjAQBgNVBAsMCURJR0lUIEQuMzETMBEGA1UEAwwK\n" +
            "RHVrZSBHdXRlazCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApvtylARrq7wm\n" +
            "/G54zCnOB8jDcJ+dfBua8a0oX3efqfiWE2DsE+BGzJHbiXP57ynEEav2dz6I/pud\n" +
            "p0LKfgSsac13pRrEkeLK+WktjNpk0YP9QksC+dIfCJI///L/0+zlcL15Jb9yw8rd\n" +
            "kRERIf2h/44Htr/qoxnG7BBLDJNpSDECAwEAATANBgkqhkiG9w0BAQsFAAOBgQCA\n" +
            "WGul6avOGZNG6lNN3Y5B8sV8WtrZGpD1h3xW6WDKS8YdBkjfiEg9opqem5ayt4Rp\n" +
            "4BaVtEfpE8TlrkpaE5KHJ4TvBdgUNE83+FWDJ2vkgbAWnSYICLI0UGQKh2hayE2P\n" +
            "DgU4+tUu/W2vGvR5cvD6u6G9l+q2VxKhU6yOsqIqAg==";

    static final String CERT_PEM_SPECIAL_CHARS = "MIIC4zCCAkygAwIBAgIEWd48lzANBgkqhkiG9w0BAQsFADCBtTELMAkGA1UEBhMC\n" +
            "UEwxLDAqBgNVBAoMI0RF4bqew5/DhMOkUEzFvMOzxYLEh05Pw4bDpsOYw7jDhcOl\n" +
            "MXgwdgYDVQQDDG9zbGFzaC9iYWNrc2xhc2hccXVvdGUiY29sb246X3JmYzIyNTNz\n" +
            "cGVjaWFsX2FtcGVyc2FuZCZjb21tYSxlcXVhbHM9cGx1cytsZXNzdGhhbjxncmVh\n" +
            "dGVydGhhbj5oYXNoI3NlbWljb2xvbjtlbmQwHhcNMTcxMDExMTU0OTEwWhcNMTgx\n" +
            "MDExMTU0OTEwWjCBtTELMAkGA1UEBhMCUEwxLDAqBgNVBAoMI0RF4bqew5/DhMOk\n" +
            "UEzFvMOzxYLEh05Pw4bDpsOYw7jDhcOlMXgwdgYDVQQDDG9zbGFzaC9iYWNrc2xh\n" +
            "c2hccXVvdGUiY29sb246X3JmYzIyNTNzcGVjaWFsX2FtcGVyc2FuZCZjb21tYSxl\n" +
            "cXVhbHM9cGx1cytsZXNzdGhhbjxncmVhdGVydGhhbj5oYXNoI3NlbWljb2xvbjtl\n" +
            "bmQwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBALnWo2hPLB3i+6lonndjX9HR\n" +
            "K3ji7vnAyJblgXMyhe4cZkSeFSd8v0zf0opQTdYKgQibbIhobH+usOXSDyRTR4+v\n" +
            "GC+RL4uZZHy6jDUWyjdPKCzek9miBHIPv0+Di8qJzS61DLTpKQIt7MCk2pzi8Dqq\n" +
            "f3Lud1YIveBX7wswrZIJAgMBAAEwDQYJKoZIhvcNAQELBQADgYEAQlyENG7aPUFb\n" +
            "bqv6dXzvZ0RNgVxrRb16vU1cxyErEKgVqbDLik435SjS+0j5/SqZr1SK+njkYF1e\n" +
            "P4FBbFh0tEnZIXMhEXZd8E7+KpMEKdCls1Vi7yiz4XkoZIgFpZw356PvUiz+IGDo\n" +
            "tPrQxf15UoXEw7KxB58s+66CEGfuYRU=";

    static final String CERT_MULTIVALUE_RDN = "MIICajCCAdOgAwIBAgIIASNFZ4kQERIwDQYJKoZIhvcNAQEFBQAwTTEuMAgGA1UE\n" +
            "BRMBMTALBgNVBCoMBGpvaG4wFQYDVQQDDA5TTVBfcmVjZWl2ZXJDTjEOMAwGA1UE\n" +
            "CgwFRElHSVQxCzAJBgNVBAYTAkJFMB4XDTE5MTIyMTEwMzE0NFoXDTIxMDIwMTEw\n" +
            "MzE0NFowTTEuMAgGA1UEBRMBMTALBgNVBCoMBGpvaG4wFQYDVQQDDA5TTVBfcmVj\n" +
            "ZWl2ZXJDTjEOMAwGA1UECgwFRElHSVQxCzAJBgNVBAYTAkJFMIGfMA0GCSqGSIb3\n" +
            "DQEBAQUAA4GNADCBiQKBgQCK86gu9nKgeR9qbs7sq0UJPtMIt8/wWYI9ns48KEew\n" +
            "O4Hu/yjt5JQNnsT1sRXBIJd7p43xGZHbyxta6oGvxxW7gRB+fXDA2lfKBVBvvM6K\n" +
            "CKeo6NKapIo0ICJbH1se04X4w8Ho4hN+CcBPmUlITmRmomL58vh9suRL2s48kp7s\n" +
            "qQIDAQABo1MwUTBPBgNVHR8ESDBGMESgQqBAhj5maWxlOi9jZWYvY29kZS9iZG1z\n" +
            "bC9iZG1zbC13ZWJhcHAvdGFyZ2V0L3Rlc3QtY2xhc3Nlcy90ZXN0LmNybDANBgkq\n" +
            "hkiG9w0BAQUFAAOBgQAt9IptsA/Kx6lRDL2GSmgiWiISDFBHVihgmJ2qLFAWCpkA\n" +
            "kNpVShhmc/cU3p7rfatDIo/9ngdoCl6CfOeSA4ufozUmMY2tVPcjRXxXCY0Uk5GD\n" +
            "vjArXjpzPkjc7Ueo24P3cQQ1Bgt0RpfPn5WXvD1o20OGKTdZgw1Ws02RzzuSQQ==";

    static final String CERT_WITH_SPACE = "MIIB5zCCAVCgAwIBAgICEjQwDQYJKoZIhvcNAQEFBQAwOTEZMBcGA1UEAwwQVGVz\n" +
            "dCB3aXRoIHNwYWNlIDEPMA0GA1UECgwGRGlnaXQgMQswCQYDVQQGEwJCRTAeFw0y\n" +
            "MDA1MDgxMzA5NDdaFw0zMDA1MDgxMzA5NDdaMDkxGTAXBgNVBAMMEFRlc3Qgd2l0\n" +
            "aCBzcGFjZSAxDzANBgNVBAoMBkRpZ2l0IDELMAkGA1UEBhMCQkUwgZ8wDQYJKoZI\n" +
            "hvcNAQEBBQADgY0AMIGJAoGBAKJKtNA8buCSsIHW7frPMt2M82TaMSC7cnaDnXZC\n" +
            "gAp/Pnvcn8G9ONmr9duylZpFcXYoTXS/m68p/LYHoWv4io8ZbR0VCUZNIlzdjkKI\n" +
            "ePtaxrtrcaWtwFjgWVIAQdDFggOcfmI0Tskq6PKrtXoLbGNR8puHbCOQdct2Bs/r\n" +
            "s7lBAgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAOpE/s2LdPqE5eOgsxoiDotdijjla\n" +
            "Fj8gSvuyZQ0/eKIeFMq+A0J7gZ6YaYAhhtfr5jGSBiwXbsKPw2kWUmxmI0wZVCxJ\n" +
            "JXmGel6dh78aM2PfnMBBgV9+pEDR9Oz5PhfbwZbxaname3iznWclCu1HoF9EFLf6\n" +
            "0bk3Fw6KU+Fjfyk=";

    static final String SSLCERT_HEADER = "MIIF2jCCA8KgAwIBAgIQeQl1yveXevc8bYmbmj+HMjANBgkqhkiG9w0BAQsFADB5MQswCQYDVQQGEwJCRTEZMBcGA1UEChMQT3BlblBFUFBPTCBBSVNCTDEWMBQGA1UECxMNRk9SIFRFU1QgT05MWTE3MDUGA1UEAxMuUEVQUE9MIFNFUlZJQ0UgTUVUQURBVEEgUFVCTElTSEVSIFRFU1QgQ0EgLSBHMjAeFw0yMDAzMzAwMDAwMDBaFw0yMjAzMjAyMzU5NTlaMFkxEjAQBgNVBAMMCVBPUDAwMDAwNDEYMBYGA1UECwwPUEVQUE9MIFRFU1QgU01QMRwwGgYDVQQKDBNFdXJvcGVhbiBDb21taXNzaW9uMQswCQYDVQQGEwJCRTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALd3R0ScCYPRFdFO8u3C11r2VJJFT5EdqDimqebr9V2nCLpgT6e6t2WovpuzeL0obf20uzHEZG/Ct/bWpWaV3HxjNgKfgsuYDxYndtwcxQnv7/ZWJauz2pMNste4LkUxaHU+bfNObKclLfErt+pck2ZjXHt+YJuAOqRqv62IeB3qvdBuI1iNzsDAkB062VD4Nu6hAleyYSI5rC72qQB5D/ZXfRcBULitseqvcJ4yR2t9RFF4zOhokKev0aVHSxllp07culdDawXidVmq0x/mel2xo/x4eG1blNLktrH/AvnZrkceiMeiY1vK6XMwS10Ky0pRDMBFlhxWunSl8aVgAocCAwEAAaOCAXwwggF4MAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgOoMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMCMB0GA1UdDgQWBBRe5FVf5PrQbcrm9ZUZk6CQ5HzwxzBdBgNVHR8EVjBUMFKgUKBOhkxodHRwOi8vcGtpLWNybC5zeW1hdXRoLmNvbS9jYV9iNmQwZGMxZGMzMTQ3NzIzZmUzNmI3NTc1OTk3YWZjNC9MYXRlc3RDUkwuY3JsMDcGCCsGAQUFBwEBBCswKTAnBggrBgEFBQcwAYYbaHR0cDovL3BraS1vY3NwLnN5bWF1dGguY29tMB8GA1UdIwQYMBaAFHwdskjxutkKBsoWY6nwek+9I517MC0GCmCGSAGG+EUBEAMEHzAdBhNghkgBhvhFARABAgMBAYGo1YEKFgY5NTc2MDgwOQYKYIZIAYb4RQEQBQQrMCkCAQAWJGFIUjBjSE02THk5d2Eya3RjbUV1YzNsdFlYVjBhQzVqYjIwPTANBgkqhkiG9w0BAQsFAAOCAgEAjveDw+BVI54rANaA06WI5w8/pMvmcM9WXWzs2hSIM+7zJC5TBd+WJTr29M8+YBOCF0h7EfHkQIDjFewq4QJeZnaZG8O6DpK5naRg1zbHxNabYHH9PeSJejCGmB2+xrmxwjgXu8/3gZxPQBfzRgWfrjmYnyIVRsFkfoMn7imWpTteeoOiqo14M9O4DzrnMDA8kn1JPjsWoCl2owPqMrgxWefsQTWTVOm/ZQY0H4QhtcJNgL/6uVirrfQ8+onhAUffiZnJSJaHMf0iOWCDSZPSWb7gqExDM7uaaaXQfqFZcw3j2XuKAz67Ma4IKQxQRl3HTL26SbPDRmildKUM+K+qkpHHlRjXbhfHqRYIanNiy0dLt5LQxB5wBAB5zmgS19iiHFDP3LD0ME7idskKV9aKZOL4qm00dTR6xyfPYPO2R/V3BCprawdLBTB8RTVoXqunNxT6hJ4AwEsSJyUI0X9S20vuwIUKgu7p8//C8DrStxV9Izn4FAQJ6KxtSyPtW7eL6Ud/aQrJDTpPiotRb8bvt/Z5Y6haC88Y7nQeefkSVnf99YMClY/pGdfsh6C57vuWdhdIH7blVg8TdQYOcvAlMLTYXDZna2P6GN5fwXWLs6yb0fYd5mcQEfoJfU4gyLY9pomt+NIO6qtv7a8HdPVrPpGekcT5KHcf06tzmJpq/5w=";

    private static final String SUBJECT = "CN=Duke Gutek,O=EC,C=GB";
    private static final String NAME = SUBJECT + ":" + "0000000058ff7174";
    private static final String ISSUER = "CN=localhost tomcat,OU=DIGIT,O=gutek Corp.,C=PL";

    private static final String SUBJECT_SPECIAL_CHARS = "CN=slash/backslash\\\\quote\\\"colon:_rfc2253special_ampersand&comma\\,equals\\=plus\\+lessthan\\<greaterthan\\>hash\\#semicolon\\;end,O=DEẞßAaPLzołcNOÆæØøAa,C=PL";

    private final EDeliveryX509AuthenticationFilter filter = new EDeliveryX509AuthenticationFilter();

    @Test
    public void testBuildDetails() throws CertificateException {//given
        byte[] certBytes = Base64.decode(CERT_PEM.getBytes());
        Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certBytes));
        X509Certificate[] certs = Collections.singletonList(cert).toArray(new X509Certificate[0]);

        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .requestAttr("jakarta.servlet.request.X509Certificate", certs)
                .buildRequest(new MockServletContext());

        //when
        PreAuthenticatedCertificatePrincipal principal = filter.buildDetails(httpRequest);

        //then
        assertEquals(SUBJECT, principal.getSubjectShortDN());
        assertEquals(ISSUER, principal.getIssuerDN());
        assertEquals(NAME, principal.getName());
    }

    @Test
    public void testSpecialSubjectCharacters() throws CertificateException {
        //given
        byte[] certBytes = Base64.decode(CERT_PEM_SPECIAL_CHARS.getBytes());
        Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certBytes));
        X509Certificate[] certs = Collections.singletonList(cert).toArray(new X509Certificate[0]);

        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .requestAttr("jakarta.servlet.request.X509Certificate", certs)
                .buildRequest(new MockServletContext());

        //when
        PreAuthenticatedCertificatePrincipal principal = filter.buildDetails(httpRequest);

        //then
        assertEquals(SUBJECT_SPECIAL_CHARS, principal.getSubjectShortDN());
    }

    @Test
    public void testMultiValueRDN() throws CertificateException {
        //given
        byte[] certBytes = Base64.decode(CERT_MULTIVALUE_RDN.getBytes());
        Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certBytes));
        X509Certificate[] certs = Collections.singletonList(cert).toArray(new X509Certificate[0]);

        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .requestAttr("jakarta.servlet.request.X509Certificate", certs)
                .buildRequest(new MockServletContext());

        //when
        PreAuthenticatedCertificatePrincipal principal = filter.buildDetails(httpRequest);

        //then
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE", principal.getSubjectShortDN());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE", principal.getSubjectCommonDN());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE:0123456789101112", principal.getName());
        assertEquals("C=BE,O=DIGIT,2.5.4.5=#130131+2.5.4.42=#0c046a6f686e+CN=SMP_receiverCN", principal.getSubjectOriginalDN());
        assertEquals("CN=SMP_receiverCN,O=DIGIT,C=BE", principal.getIssuerDN());
    }

    @Test
    public void testWithEndSPaceInRDN() throws CertificateException {
        //given
        byte[] certBytes = Base64.decode(CERT_WITH_SPACE.getBytes());
        Certificate cert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certBytes));
        X509Certificate[] certs = Collections.singletonList(cert).toArray(new X509Certificate[0]);

        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .requestAttr("jakarta.servlet.request.X509Certificate", certs)
                .buildRequest(new MockServletContext());

        //when
        PreAuthenticatedCertificatePrincipal principal = filter.buildDetails(httpRequest);

        //then
        assertEquals("CN=Test with space,O=Digit,C=BE", principal.getSubjectShortDN());
        assertEquals("CN=Test with space,O=Digit,C=BE", principal.getSubjectCommonDN());
        assertEquals("CN=Test with space,O=Digit,C=BE:0000000000001234", principal.getName());
        assertEquals("C=BE,O=Digit\\ ,CN=Test with space\\ ", principal.getSubjectOriginalDN());
        assertEquals("CN=Test with space,O=Digit,C=BE", principal.getIssuerDN());
    }

    @Test
    public void testBuildDetailsWithoutCert() {
        //given
        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .buildRequest(new MockServletContext());

        //when
        PreAuthenticatedCertificatePrincipal principal = filter.buildDetails(httpRequest);

        //then
        assertNull(principal);
    }

    @Test
    public void testGetCertificateArray() {
        //given
        HttpServletRequest httpRequest = MockMvcRequestBuilders.put("https://localhost/")
                .header(EDeliveryX509AuthenticationFilter.HTTP_HEADER_RP_CERTIFICATE, SSLCERT_HEADER)
                .buildRequest(new MockServletContext());

        //when
        X509Certificate[] crtList = filter.getCertificateArray(httpRequest);

        //then
        assertNotNull(crtList);
        assertEquals(1, crtList.length);
        assertNotNull(crtList[0]);
    }

}
