<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2017 European Commission | CEF eDelivery
  ~
  ~ Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
  ~ You may not use this work except in compliance with the Licence.
  ~
  ~ You may obtain a copy of the Licence at:
  ~ https://joinup.ec.europa.eu/software/page/eupl
  ~ or file: LICENCE-EUPL-v1.1.pdf
  ~
  ~ Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the Licence for the specific language governing permissions and limitations under the Licence.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>eu.europa.ec.edelivery</groupId>
    <artifactId>edelivery-springsecurity-2-way-ssl-auth</artifactId>
    <version>2.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>eDelivery SpringSecurity 2-way-SSL authentication supporting library</name>

    <licenses>
        <license>
            <name>EUPL 1.2</name>
            <url>https://joinup.ec.europa.eu/software/page/eupl/licence-eupl</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <developerConnection>
            scm:git:https://ec.europa.eu/digital-building-blocks/code/scm/edelivery/springsecurity-2-way-ssl-auth.git
        </developerConnection>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <repository>
            <id>cefdigital-releases</id>
            <url>https://ec.europa.eu/digital-building-blocks/artifact/content/repositories/eDelivery/</url>
        </repository>
        <snapshotRepository>
            <id>cefdigital-snapshots</id>
            <url>https://ec.europa.eu/digital-building-blocks/artifact/content/repositories/eDelivery-snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <sonar.host.url>http://localhost:9000/sonar/</sonar.host.url>
        <sonar.language>java</sonar.language>

        <project.scm.id>edelivery-scm</project.scm.id>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <maven.test.argLine>-javaagent:${org.mockito:mockito-core:jar} -XX:+EnableDynamicAgentLoading</maven.test.argLine>

        <!-- Plugin versions -->
        <dependency-check-maven.version>11.1.0</dependency-check-maven.version>
        <plugin.jacoco.version>0.8.12</plugin.jacoco.version>
        <plugin.sonar.version>4.0.0.4121</plugin.sonar.version>
        <maven-enforcer-plugin.version>3.5.0</maven-enforcer-plugin.version>
        <maven-surefire-plugin.version>3.5.2</maven-surefire-plugin.version>
        <maven-release-plugin.version>3.1.1</maven-release-plugin.version>
        <maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
        <maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
        <maven-dependency-plugin.version>3.8.1</maven-dependency-plugin.version>
        <!--  dependecies -->
        <bouncycastle.version>1.79</bouncycastle.version>
        <commons-lang.version>3.17.0</commons-lang.version>
        <hamcrest-core.version>3.0</hamcrest-core.version>
        <httpclient.version>5.4.1</httpclient.version>
        <jakarta.servlet-api.version>6.1.0</jakarta.servlet-api.version>
        <junit-jupiter.version>5.11.4</junit-jupiter.version>
        <logback-classic.version>1.5.16</logback-classic.version>
        <lombok.version>1.16.16</lombok.version>
        <mockito.version>5.14.2</mockito.version>
        <org.slf4j.version>2.0.7</org.slf4j.version>
        <spring.security.version>6.4.2</spring.security.version>
        <spring.version>6.2.1</spring.version>
        <wss4j.version>3.0.4</wss4j.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-web</artifactId>
            <version>${spring.security.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <version>${hamcrest-core.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang.version}</version>
        </dependency>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
            <version>${jakarta.servlet-api.version}</version>
            <scope>provided</scope>
        </dependency>

        <!-- Merlin implementation -->
        <dependency>
            <groupId>org.apache.wss4j</groupId>
            <artifactId>wss4j-ws-security-common</artifactId>
            <version>${wss4j.version}</version>
            <!-- wss4j 2.4.x uses the geronimo javamail in the
                AttachmentUtils.canonizeMimeHeaders(outputStream, getAttachment().getHeaders());
                for preparing the mime headers for the signature. This is not needed in our case
                and it is causing a conflict with the javamail version used in other the project.
                NOTE: Remove this exclusion when wss4j is upgraded to 3.x+
             -->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.geronimo.javamail</groupId>
                    <artifactId>geronimo-javamail_1.4_mail</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <!-- BouncyCastle -->
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk18on</artifactId>
            <version>${bouncycastle.version}</version>
        </dependency>
        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcpkix-jdk18on</artifactId>
            <version>${bouncycastle.version}</version>
        </dependency>
        <!-- End BouncyCastle -->
        <!-- -->

        <dependency>
            <groupId>org.apache.httpcomponents.client5</groupId>
            <artifactId>httpclient5</artifactId>
            <version>${httpclient.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-config</artifactId>
            <version>${spring.security.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-test</artifactId>
            <version>${spring.security.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback-classic.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>dependency-check</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.owasp</groupId>
                        <artifactId>dependency-check-maven</artifactId>
                        <version>${dependency-check-maven.version}</version>
                        <inherited>false</inherited>
                        <configuration>
                            <nvdApiServerId>edelivery-owasp</nvdApiServerId>
                            <skipProvidedScope>true</skipProvidedScope>
                            <skipRuntimeScope>true</skipRuntimeScope>
                            <!-- Disable .Net Assembly Analyzer -->
                            <assemblyAnalyzerEnabled>false</assemblyAnalyzerEnabled>
                            <suppressionFile>owasp-false-positive-warnings.xml</suppressionFile>
                        </configuration>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>aggregate</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>${plugin.jacoco.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.sonarsource.scanner.maven</groupId>
                    <artifactId>sonar-maven-plugin</artifactId>
                    <version>${plugin.sonar.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven-release-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven-enforcer-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>${maven-dependency-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-maven</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>3.5</version>
                                </requireMavenVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>properties</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- jacoco start -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <formats>
                        <!-- Eclipse uses exec files, SonarQube uses xml reports -->
                        <format>XML</format>
                        <format>HTML</format>
                    </formats>
                </configuration>
                <executions>
                    <execution>
                        <id>default-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>default-report</id>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.sonarsource.scanner.maven</groupId>
                <artifactId>sonar-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <configuration>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <useReleaseProfile>false</useReleaseProfile>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <!-- We need to reinitialize classes based on the configuration -->
                    <reuseForks>false</reuseForks>
                    <forkNode implementation="org.apache.maven.plugin.surefire.extensions.SurefireForkNodeFactory" />
                    <!-- @{argLine} is generated by jacoco-prepare -->
                    <argLine>${maven.test.argLine} @{argLine}</argLine>
                </configuration>

            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
